import { defineConfig } from 'vite'
// import { defineConfig } from 'vitest/dist/config'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  define: {
       "import.meta.vitest": "undefined",
  },
  test: {
    globals: true,
    environment: "jsdom",
    includeSource: ["src/**/*.{js, jsx}"],
    coverage: {
      reporter: ["text", "html"]
    }
  }
})

