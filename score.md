team 07 
  * ui - 2
  * logic - 2
  * presentation - 3
team 09 - 2
  * ui - 2
  * logic - 1
  * presentation - 1

team 13 
  * ui - 2
  * logic - 2
  * presentation - 1

team 5
  * ui - 1
  * logic - 1
  * presentation - 1

team 11
  * ui - 1
  * logic - 2
  * presentation - 1

team 15
  * ui - 2
  * logic - 2
  * presentation - 1

team 1
  * ui - 3
  * logic - 2
  * presentation - 2




to steal:
  * ticket for bugs -> document in the database

























// on value ref listener for the user profile to change data dynamically when the user updates something
// the same thing for user addons
// change the addon view in the user addons





import { Tab, Tabs, Typography, Grid, Box, Button, Stack,   styled  } from "@mui/material"
import { TabContext, TabPanel } from '@mui/lab';
import { useLocation } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { db } from '../../services/database-services';
import HomeAddonSmall from './HomeAddonSmall';

const Container = styled('div')({
    display: "grid",
    gridTemplateColumns: "repeat(auto-fill, minmax(calc(20% - 20px), 1fr))",
    gap: "20px",
    width: "80%",
    margin: "50px auto",
});


export default function FilteredAddons() {

    const location = useLocation();
    const data = location.state || '';
  

    // const location = useLocation();
    // const { array } = location.state || '';
    console.log(data)
    // const array = Object.values(addons);
    const [addonData, setAddonData] = useState(array);
    const [isAddonFound, setIsAddonFound] = useState(false);
    const [tabValue, setTabValue] = useState('1');

    const handleChange = (event, newValue) => {
        setTabValue(newValue);
    };



    // const location = useLocation();
    // const searchParams = new URLSearchParams(location.search);
    // const searchInput = searchParams.get("searchInputRef").toLowerCase().trim();
    // const selectedMethod = searchParams.get("filterAddon");

    // useEffect(() => {
    //     const finalResults = async () => {
    //         const snapshot = await db.get("addons");
    //         const addonArray = Object.values(snapshot);
    //         let gatherAddonsResults = [];

    //         if (selectedMethod === "Name") {
    //             const nameInput = searchInput;
    //             gatherAddonsResults = addonArray.filter((singleAddon) => {
    //                 if(singleAddon.name === undefined) return false;
    //                 return singleAddon.name.toLowerCase().includes(nameInput) && singleAddon.state === "approved"
    //             })
    //         }
    //         else if (selectedMethod === "IDE") { 
    //             const nameInput = searchInput;
    //             gatherAddonsResults = addonArray.filter((singleAddon) => {
    //                 if(singleAddon.IDE === undefined) return false;
    //                 return singleAddon.IDE.toLowerCase().includes(nameInput) && singleAddon.state === "approved"
    //             })
    //         }
    //         else if (selectedMethod === "Creator") {
    //             const nameInput = searchInput;
    //             gatherAddonsResults = addonArray.filter((singleAddon) => {
    //                 if(singleAddon.creator === undefined) return false;
    //                 return singleAddon.creator.toLowerCase().includes(nameInput) && singleAddon.state === "approved"
    //             }) 
    //         }
    //         if (gatherAddonsResults.length > 0) {
    //             setIsAddonFound(true);
    //             setAddonData(gatherAddonsResults);
    //         } else {
    //             setIsAddonFound(false);
    //             setAddonData([]);
    //         }
    //     };

    //     finalResults();
    // }, []);

    // if (isAddonFound && addonData.length === 0) {
    //     return (
    //         <>
    //             <Typography variant="h2" sx={{ textAlign: "center", paddingTop: "50px" }}>No addon found. Please go back and try again</Typography>
    //         </>
    //     )
    // }

    const name = addonData.slice().sort((a, b) => a.name.localeCompare(b.name));
    const creator = addonData.slice().sort((a, b) => a.creator.localeCompare(b.creator));
    const recent = addonData.slice().sort((a, b) => b.created - a.created);
    const tags = addonData.slice().sort((a, b) => b.tags.length - a.tags.length);
    const downloads = addonData.slice().sort((a, b) => b.downloads - a.downloads);


    return (
        <>
            <Box sx={{mb: "58%"}}>
            <TabContext value={tabValue}>
                <Tabs onChange={handleChange} aria-label="disabled tabs example">
                <Tab label="Name" value='1'/>
                <Tab label="Creator" value='2'/> 
                <Tab label="Tags" value='3'/>
                <Tab label="Recent" value='4'/>
                <Tab label="Downloads" value='5'/>
                <Tab label="Last commit" disabled/>
                </Tabs>
                <TabPanel value="1">
                <main>
                    <Typography variant="h4">All Addons</Typography>
                    <Grid container spacing={3}>
                    <Container>
                        {name.map((item, index) =>
                            <HomeAddonSmall addon={item} key={index} />)}
                    </Container>
                    </Grid>    
                </main>
                </TabPanel>
                <TabPanel value="2">
                <main>
                    <Typography variant="h4">All Addons</Typography>
                    <Grid container spacing={3}>
                    <Container>
                        {creator.map((item, index) =>
                            <HomeAddonSmall addon={item} key={index} />)}
                    </Container>
                    </Grid>    
                </main>
                </TabPanel>
                <TabPanel value="3">
                <main>
                    <Typography variant="h4">All Addons</Typography>
                    <Grid container spacing={3}>
                    <Container>
                        {tags.map((item, index) =>
                            <HomeAddonSmall addon={item} key={index} />)}
                    </Container>
                    </Grid>    
                </main>
                </TabPanel>
                <TabPanel value="4">
                <main>
                    <Typography variant="h4">All Addons</Typography>
                    <Grid container spacing={3}>
                    <Container>
                        {recent.map((item, index) =>
                            <HomeAddonSmall addon={item} key={index} />)}
                    </Container>
                    </Grid>    
                </main>
                </TabPanel>
                <TabPanel value="5">
                <main>
                    <Typography variant="h4">All Addons</Typography>
                    <Grid container spacing={3}>
                    <Container>
                        {downloads.map((item, index) =>
                            <HomeAddonSmall addon={item} key={index} />)}
                    </Container>
                    </Grid>    
                </main>
                </TabPanel>
                </TabContext>
            </Box>
            {isAddonFound && addonData.length === 0 && (
                <Typography variant="h2" sx={{ textAlign: "center", paddingTop: "50px" }}>No addon found. Please go back and try again</Typography>
            )}
        </>
    )
}