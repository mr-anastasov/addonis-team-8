import { createTheme } from "@mui/material/styles";


export const neumorphism = (theme) => {
  return createTheme({
    borderRadius: 3,
    spacing: 7,
    palette: {

      // .color1 {color: #656c87;}
      // .color2 {color: #9aa2b5;}
      // .color3 {color: #c3c0d6;}
      // .color4 {color: #e8e5ef;}
      // .color5 {color: #f9f8f9;}

      mode: theme ? 'light' : 'dark',
      background: {
default: theme? "#f9f8f9" : "#cec9c8",
        // default: theme? "#f9f8f9" : "#c3c0d6",
      },
      primary: {
        main: theme? "#000011" : "#cec9c8"
      },
      text: {
        primary: "#6a6567",
        secondary: '#fff',
      },
      secondary: {
        main: theme? "#000011" : "#000011",
      },
      success: {
        main: theme? "#47CF73" : "#47CF73",
      },
      error: {
        main: theme? "#FF0000" : "#FF0000",
      },
      warning: {
        main: theme? "#FFC107" : "#FFC107",
      },
      info: {
        main: theme? "#cec9c8" : "#000",
      },

    },
    typography: {
      color: theme? "#000011" : "#000011",
      textDecorationColor: theme? "#000011" : "#000011",
      fontFamily: [
        'Montserrat',
        'sans-serif',
      ].join(','),
    },
    components: {
      MuiPaper: {
        styleOverrides: {
          root: {
            boxShadow: '4px 4px 8px rgba(0, 0, 0, 0.1), -4px -4px 8px rgba(255, 255, 255, 0.5)', // Customize shadow
            borderRadius: '8px', // Apply rounded corners
background: 'linear-gradient(117deg, rgba(207,215,219,4) 14%, rgba(164,128,102,1) 100%, rgba(202,170,147,1) 100%)'// Background gradient
          },
        },
      },
      MuiModal: {
        styleOverrides: {
          root: {
            color: '#55a49e',
            border: '1px solid #cec9c8',
          },
        },
      },
      MuiFormControl: {
        styleOverrides: {
          root: {
            width: '100%',
            margin: '10px 0',
            color: '#000011',
            // background: 'linear-gradient(225deg, #f3f3f3, #d1d1d1)', // Background gradient
          },
        },
      },
      MuiTypography: {
        styleOverrides: {
          root: {
            color: '#000011',
            textDecorationColor: '#000011',
          },
        },
      },
      MuiInput: {
        styleOverrides: {
          root: {
            color: '#000011',
            textDecorationColor: '#000011',
          },
        },
      },
      MuiInputLabel: {
        styleOverrides: {
          root: {
            color: '#000011',
          },
        },
      },
      MuiTextField: {
        styleOverrides: {
          root: {
            color: '#000011',
            textDecorationColor: '#000011',
          },
        },
      },
      MuiButton: {
        styleOverrides: {
          root: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            variant: 'outlined',
            gap: '10px',
            margin: 10,
            padding: 2,
          }
        },
      },
      // MuiCard: {
      //   styleOverrides: {
      //     root: {
      //       background: 'linear-gradient(225deg, #55a49e, #d1d1d1)', 
      //       width: '100%',
      //       padding: '10px',
      //       borderRadius: '10px',
      //       boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
      //       height: '400px',
      //       display: 'flex',
      //       flexDirection: 'column',
      //       justifyContent: 'space-between',
      //       transition: 'transform 0.2s',
      //       '&:hover': {
      //         transform: 'scale(1.05)',
      //       }
      //     }
      //   }
      // }
  // to override only the button color the button color 
  //   Button: {
  //     styleOverrides: { 
  //       root: { ({ownerState}) => ({
  //         ...(ownerState.color === 'red' && {color: 'white'}),
  //         })
  //       }
  //     },
  //     defaultProps: {
  //       size: 'red',
  //     }
  //   }
    }
  })
}




// export default CustomTheme;

// https://mybrandnewlogo.com/color-palette-generator
