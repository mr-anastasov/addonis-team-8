import { styled } from '@mui/material/styles';
import {  Box, Paper, Grid, Typography, Button, Stack, Rating } from '@mui/material';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import NavigationHelper from '../../common/helper';
import { addonUtils } from '../../services/addon-services';
import { getGitHubRepoInfoFromLink } from '../../services/git-hub';
import { useState, useEffect } from 'react';
import GitHubIcon from '@mui/icons-material/GitHub';
import WatchLaterIcon from '@mui/icons-material/WatchLater';
import DownloadIcon from '@mui/icons-material/Download';
import TagIcon from '@mui/icons-material/Tag';


const modalStyle = {
  position: 'absolute',
  maxWidth: '80%',
  margin: '0 auto',
  padding: '10px',
  backgroundColor: "#cec9c8",
  borderRadius: '10px',
  boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
  overflowY: 'auto',
};


const ItemImage = styled('img')({
  width: "300px",
  height: "300px",
  objectFit: "cover",
  margin: "auto",
  marginTop: '20px',
  padding: "15px",
  display: "block",
});


export default function DetailAddon(prop) {



  const { addon } = prop;
  const handleLike = () => {
    addonUtils.rate(addon.addonID,'BC6SFlnF1kcVVTpK49llcz77PL82' )
    console.log("Liked")
  }

  const [repoInfo, setRepoInfo] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await getGitHubRepoInfoFromLink(addon.originUrl);
        setRepoInfo(result);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  if (!repoInfo) {
    return <div>Loading...</div>;
  }

  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      minHeight="100vh"
      backgroundColor="rgba(0, 0, 0, 0.5)"
      position="fixed"
      top="0"
      left="0"
      right="0"
      bottom="0"
      zIndex="1000"
      sx = {{ overflowY: 'auto'}}
    >
      <Grid container spacing={2} style={{ ...modalStyle, marginTop: '20px' }} key={addon.addonID}>
        <Grid item xs={12} sm={6}>
            <Paper sx={{ m: 2, p: 2, textAlign: "center"}}>
              <Typography variant="h6" component="div">{addon.name}</Typography>
            </Paper>
            <ItemImage src={addon.addonImg} alt={addon.name} />
        </Grid>
        <Grid item xs={12} sm={6}>
            <Paper sx={{ m: 2, p: 2}}>
                <Stack spacing={1}>
                    <Rating name="size-small"  defaultValue={0} value={addon.rating} size="small"/>
                </Stack>
              <Typography variant="h7" component="div">IDE: {addon.IDE}</Typography>
              <Typography variant="h7" component="div">Creator: {addon.creator}</Typography>
              <Typography variant="h7" component="div">{<TagIcon></TagIcon>} {addon.tags}</Typography>
              <Typography variant="h7" component="div">{<DownloadIcon></DownloadIcon>} {<Link to={addon.downloadURL}>{addon.downloadURL}</Link>} </Typography>
              <Typography variant="h7" component="div">{<GitHubIcon></GitHubIcon>} {<Link to={addon.originUrl}>{addon.originUrl}</Link>}</Typography>
              <Typography variant="h7" component="div">Number of downloads: {addon.downloads}</Typography>
              <Typography variant="h7" component="div">Open issue count: {repoInfo.issueCount}</Typography>
              <Typography variant="h7" component="div">Pull requests count:{repoInfo.pullRequestCount}</Typography>
              <Typography variant="h7" component="div">{<WatchLaterIcon></WatchLaterIcon>} Last commit: {new Date(repoInfo.lastCommitDate).toDateString()}</Typography>
              <Typography variant="h7" component="div">Last commit title: {repoInfo.lastCommitTitle}</Typography>
            </Paper>
        </Grid>
        <Grid item xs={12} sm={12}>
          <Paper sx={{ m: 2, p: 2}}>
            <Typography variant="h6" component="div">{addon.description}</Typography>
            </Paper> 
        </Grid>
      </Grid>
    </Box>
  );
}


DetailAddon.propTypes = {
  addon: PropTypes.object,
};