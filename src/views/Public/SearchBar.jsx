import { useState, useRef } from 'react';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import ListItemText from '@mui/material/ListItemText';
import Select from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';
import { styled, InputBase, IconButton, Button } from "@mui/material";

import SearchIcon from "@mui/icons-material/Search";
import NavigationHelper from "../../common/helper";
import { useNavigate } from 'react-router-dom';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 150,
    },
  },
};

const filterComponents = [
  'Name',
  'IDE',
  'Creator'
];

export default function SearchBar() {
  // eslint-disable-next-line no-unused-vars
  const { goToSearchResults } = NavigationHelper();
  const [filterAddon, setFilterAddon] = useState([]);
  const [searchInput, setSearchInput] = useState('');
  const [searchVisible, setSearchVisible] = useState(false);

  const isSearchInputEmpty = searchInput.trim() === '';
  const [showErrorMessage, setShowErrorMessage] = useState(false);

  const Search = styled("div")(() => ({
    display: "flex",
    alignItems: "center",
    backgroundColor: "white",
    padding: "0 10px",
  }));

  const Spacing = styled("div")({
    marginLeft: "10px",
  });

  const toggleSearch = () => {
    setSearchVisible(!searchVisible);
  };

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;

    setFilterAddon(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  const searchInputRef = useRef(null);
  const history = useNavigate();

  const handleBlur = () => {
    const inputValue = searchInputRef.current.value;
    setSearchInput(inputValue);
    setShowErrorMessage(inputValue.trim() === '');
  };

  const handleClick = () => {
    const searchInputValue = searchInputRef.current.value;
    setSearchInput(searchInputValue)

    const filterAddonValue = filterAddon.join(',');
    if(searchInput.trim() !== '' && filterAddonValue.trim() !== ''){
    history(`/search-results?filterAddon=${filterAddonValue}&searchInputRef=${searchInput}`);
    return;
    } else {
      setShowErrorMessage(true);
    }
    goToSearchResults();
  }

  return (
    <>
      <IconButton size="small" onClick={toggleSearch}>
        <SearchIcon />
      </IconButton>
      {searchVisible && (
        <>
          <div>
            <FormControl sx={{ m: 1, width: 200 }}>
              <InputLabel id="demo-multiple-checkbox-label">Filter</InputLabel>
              <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                className='select-input-user'
                multiple={false}
                value={filterAddon}
                onChange={handleChange}
                input={<OutlinedInput label="Filter" />}
                renderValue={(selected) => selected.join(', ')}
                MenuProps={MenuProps}
              >
                {filterComponents.map((name) => (
                  <MenuItem key={name} value={name}>
                    <Checkbox checked={filterAddon.indexOf(name) > -1} />
                    <ListItemText primary={name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
          <Spacing />
          <Search>
            <InputBase
              placeholder="Search..."
              onBlur={handleBlur} 
              inputRef={searchInputRef}
              className='search-input-user'
            />
          </Search>
          <Spacing />
          {isSearchInputEmpty && showErrorMessage && (
            <p className="error-message" style={{ color: "red"}}>Please, enter data for your search!</p>
          )}
          <Button onClick={handleClick}>Search</Button>
        </>
      )}
    </>
  );
}