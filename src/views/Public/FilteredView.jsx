import { useLocation } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { db } from '../../services/database-services';
import { Typography, styled, Box } from '@mui/material';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import HomeAddonSmall from './HomeAddonSmall';

const Container = styled('div')({
    display: "grid",
    gridTemplateColumns: "repeat(auto-fill, minmax(calc(20% - 20px), 1fr))",
    gap: "20px",
    width: "80%",
    margin: "50px auto",
});


export default function FilteredAddons() {

    const [addonData, setAddonData] = useState([]);
    const [isAddonFound, setIsAddonFound] = useState(false);
    const [sortingMethod, setSortingMethod] = useState("default");

    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const searchInput = searchParams.get("searchInputRef").toLowerCase().trim();
    const selectedMethod = searchParams.get("filterAddon");

    useEffect(() => {
        const finalResults = async () => {
            const snapshot = await db.get("addons");
            const addonArray = Object.values(snapshot);
            let gatherAddonsResults = [];

            if (selectedMethod === "Name") {
                const nameInput = searchInput;
                gatherAddonsResults = addonArray.filter((singleAddon) => {
                    if(singleAddon.name === undefined) return false;
                    return singleAddon.name.toLowerCase().includes(nameInput) && singleAddon.state === "approved"
                })
            }
            else if (selectedMethod === "IDE") { 
                const nameInput = searchInput;
                gatherAddonsResults = addonArray.filter((singleAddon) => {
                    if(singleAddon.IDE === undefined) return false;
                    return singleAddon.IDE.toLowerCase().includes(nameInput) && singleAddon.state === "approved"
                })
            }
            else if (selectedMethod === "Creator") {
                const nameInput = searchInput;
                gatherAddonsResults = addonArray.filter((singleAddon) => {
                    if(singleAddon.creator === undefined) return false;
                    return singleAddon.creator.toLowerCase().includes(nameInput) && singleAddon.state === "approved"
                }) 
            }
            if (gatherAddonsResults.length > 0) {
                setIsAddonFound(true);
                setAddonData(gatherAddonsResults);
            } else {
                setIsAddonFound(false);
                setAddonData([]);
            }
        };

        finalResults();
    }, [searchInput, selectedMethod]);


    const handleSorting = (method) => {
        let sortedAddonData = [...addonData];
        if (method === "Name") {
          sortedAddonData = sortedAddonData.sort((a, b) => a.name.localeCompare(b.name));
        } else if (method === "Creator") {
           sortedAddonData = sortedAddonData.sort((a, b) => a.creator.localeCompare(b.creator));
        } else if (method === "Recent") {
            sortedAddonData = sortedAddonData.sort((a, b) => b.created - a.created);
        } else if (method === "Tags") {
            sortedAddonData = sortedAddonData.sort((a, b) => b.tags.length - a.tags.length);
        } else if (method === "Downloads"){
            sortedAddonData = sortedAddonData.sort((a, b) => b.downloads - a.downloads);
        } 
        // else if (method === "LastCommit"){
        //     sortedAddonData = sortedAddonData.sort((a, b) => b.created - a.created);
        // }
      
        setAddonData(sortedAddonData);
        setSortingMethod(method);
    };


    return (
        <Box sx={{mb: "40%"}}>
            <Typography variant="h4" sx={{ textAlign: "center", paddingBottom: "10px", paddingTop: "10px" }}> Results found</Typography>
                <Stack spacing={1} direction='row' justifyContent='center'>
                    <Button variant='contained' className="buttons" onClick={() => handleSorting("Name")}>Name</Button>
                    <Button variant='contained' className="buttons" onClick={() => handleSorting("Creator")}>Creator</Button>
                    <Button variant='contained' className="buttons" onClick={() => handleSorting("Tags")}>Tags</Button>
                    <Button variant='contained' className="buttons" onClick={() => handleSorting("Recent")}>Recent</Button>
                    <Button variant='contained' className="buttons" onClick={() => handleSorting("Downloads")}>Downloads</Button>
                    <Button variant='contained' className="buttons" disabled>Last commit</Button>
                </Stack>
            {isAddonFound && addonData.length > 0 && (
                <Container>
                    {addonData.map((item, index) => (
                            <HomeAddonSmall addon={item} key={index}/>
                    ))}
                </Container>
            )}

            {!isAddonFound && addonData.length === 0 && (
                <>
                    <Typography variant="h2" sx={{ textAlign: "center", paddingTop: "50px" }}>No addon found. Please go back and try again</Typography>
                </>
            )}
        </Box>
    )
}