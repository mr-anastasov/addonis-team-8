import Carousel from "@itseasy21/react-elastic-carousel";
import { Typography, styled } from '@mui/material';
import { UseAppData } from "../../context/AppContext";
import HomeAddonSmall from "./HomeAddonSmall";
import AI from '../../assets/AI.jpg';
import { useEffect, useState } from "react";
import { db } from "../../services/database-services";

const ItemImage = styled('img')({
    width: "1400px",
    height: "400px",
    objectFit: "cover",
    margin: "auto",
    padding: "15px",
    display:"flex",
    justifyContent: "center",
    borderRadius: "8px",
});

const Text = styled(Typography)({
    variant: "h2",
    display: "flex",
    justifyContent: "center",
    padding: "10px",
    margin: "30px auto",
    fontSize: "40px",
    textShadow: "2px 2px 4px rgba(0, 0, 0, 1)",
});

export default function Main() {


    const { addons } = UseAppData();
    const [addonData, setAddonData] = useState(addons);


    useEffect(() => {

        const fetchData = async () => {
            try {
                const response = await db.get("addons");
                setAddonData(response);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();

    }, []);

    const array = Object.values(addonData);

    const mostRecent = array.slice().sort((a, b) => b.created - a.created);
    const mostDownloaded = array.slice().sort((a, b) => b.downloads - a.downloads);
    const featuredAddons = array.filter(addon => addon.Featured === true);

    return (
        <>
            <Text>Intelligent Technologies for Everyone: AI Marketplace</Text>
            <ItemImage src={AI}></ItemImage>
            <div style={{ padding: "30px", margin: "50px auto", width: "80%" }}>
                <Typography variant="h4" sx={{ textAlign: "center", paddingBottom: "10px", paddingTop: "10px"}}>Featured</Typography>
                <Carousel itemsToShow={5} style={{ marginBottom: "50px" }}>
                    {featuredAddons.map((addon, index) => (
                        <HomeAddonSmall addon={addon} key={index}/>
                    ))}
                </Carousel>
                <Typography variant="h4" sx={{ textAlign: "center", paddingBottom: "10px", paddingTop: "10px" }}>Most Downloaded</Typography>
                <Carousel itemsToShow={5} style={{ marginBottom: "50px" }}>
                    {mostDownloaded.map((addon, index) => (
                        <HomeAddonSmall addon={addon} key={index}/>
                    ))}
                </Carousel>
                <Typography variant="h4" sx={{ textAlign: "center", paddingBottom: "10px", paddingTop: "10px" }}>Most Recent</Typography>
                <Carousel itemsToShow={5} style={{ marginBottom: "50px" }}>
                    {mostRecent.map((addon, index) => (
                        <HomeAddonSmall addon={addon} key={index}/>
                    ))}
                </Carousel>
            </div>
        </>
    )
}