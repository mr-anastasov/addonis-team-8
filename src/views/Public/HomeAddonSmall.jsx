import { Typography, Button, styled, Modal, Stack, Rating } from '@mui/material';
import PropTypes from 'prop-types';
import DownloadDoneIcon from '@mui/icons-material/DownloadDone';
import { useState } from "react";
import DetailAddon from './DetailsAddon';
import { db } from '../../services/database-services';
import { UseUserContext } from '../../context/UserContext';

const Item = styled('div')({
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    border: "1px solid #ccc",
    borderRadius: "8px",
    height: "250px",
    width: "100%",
    background: 'linear-gradient(90deg, rgba(176,160,152,1) 16%, rgba(106,101,103,1) 100%, rgba(206,201,200,1) 100%)',
    color: "black",
    margin: "0 10px",
    transition: "box-shadow 0.3s ease, border-width 0.3s ease",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
    borderWidth: "1px",
    "&:hover": {
        boxShadow: "0px 10px 14px rgba(0, 0, 0, 0.5)",
        borderWidth: "5px"
    },
});

const ItemImage = styled('img')({
    width: "100px",
    height: "100px",
    objectFit: "cover",
    margin: "auto",
    padding: "15px",
    display: "block",
});

const HomeAddonSmall = (props) => {

    const { userData, role } = UseUserContext();
    const { addon } = props;
    const [selectedAddon, setSelectedAddon] = useState(null);
    let value = addon?.rating;
    if ( typeof value === "object" ) {
        value = 0;
    }
    const [rating, setRating] = useState(value);

    const openDetailsModal = (addon) => {
        setSelectedAddon(addon);
    };

    const [downLoadCount, setDownLoadCount] = useState(addon?.downloads);
    const handleDownload = async () => {
        const downloadURL = addon.downloadURL;
        const link = document.createElement('a');
        link.download = downloadURL;
        link.href = downloadURL;

        alert("download started");
        link.click();

        const updateDownLoadCount = downLoadCount + 1;
        setDownLoadCount(updateDownLoadCount);

        try {
            await db.set(`/addons/${addon.addonID}/downloads`, updateDownLoadCount);
        } catch (error) {
            console.error("Error with download", error);
        }
    }

    const handleRating = (event) => {

        if ( role === null) {
            alert("Please login to rate this addon");
            return;
        }

        if ( role === "blocked") {
            alert("You are blocked you can't rate addons");
        }
        
        if ( userData.userID === addon.creatorID ) {
            alert("You can't rate your own addon");
            return;
        }

        let newScore = 0;
        if (addon.rating === 0) {
            newScore = + event.target.value;
        } else {
            newScore = + (+rating + +event.target.value) / 2;
        }
        setRating(newScore);
        const path = `/addons/${addon.addonID}/rating`;
        db.set(path, newScore.toFixed(0));
    }

    
    return (
        <>
            <Item key={addon?.addonID} sx={{ m: 1, p: 1 }}>
                <ItemImage src={addon?.addonImg} alt={addon?.name} loading="lazy" />
                <Typography sx={{ fontSize: "16px", textAlign: "center", margin: "5px", fontWeight: "600" }}>{addon?.name}</Typography>
                <Typography sx={{ fontSize: "12px", display: "flex", alignItems: "center", justifyContent: "space-between", margin: "5px" }}>
                    <span>{addon?.creator}</span>
                    <span style={{ display: "flex", alignItems: "center" }}>
                        <DownloadDoneIcon />
                        {downLoadCount}
                    </span>
                </Typography>
                <Stack spacing={1}>
                    <Rating name="size-small"  defaultValue={0} value={rating} size="small" onChange={handleRating}/>
                </Stack>
                <div style={{ display: "flex", alignItems: "center" }} >
                    <Button variant="contained" sx={{ margin: "10px", width: "100px" }} onClick={() => openDetailsModal(addon)}>Details</Button>
                    <span>&nbsp;</span>
                    <Button variant="contained" sx={{ margin: "10px", width: "100px" }} onClick={handleDownload}>Download</Button>
                </div>
            </Item>
            <Modal
                open={selectedAddon !== null}
                onClose={() => setSelectedAddon(null)}
                aria-labelledby="detail-modal-title"
                aria-describedby="detail-modal-description"
            >
                <div>
                    {selectedAddon && <DetailAddon addon={selectedAddon} />}
                </div>
            </Modal>
        </>
    )
}

HomeAddonSmall.propTypes = {
    addon: PropTypes.object,
}

export default HomeAddonSmall;