import { Typography, Button, styled, Modal, Stack, Rating } from '@mui/material';
import PropTypes from 'prop-types';
import DownloadDoneIcon from '@mui/icons-material/DownloadDone';
import { useState } from "react";
import { db } from '../../services/database-services';
import { UseUserContext } from '../../context/UserContext';
import NavigationHelper from '../../common/helper';
import { addonUtils } from "../../services/addon-services";
import { DOCS } from '../../common/constants';

const Item = styled('div')({
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    border: "1px solid #ccc",
    borderRadius: "8px",
    height: "250px",
    width: "100%",
    background: 'linear-gradient(90deg, rgba(176,160,152,1) 16%, rgba(106,101,103,1) 100%, rgba(206,201,200,1) 100%)',
    color: "black",
    margin: "0 10px",
    transition: "box-shadow 0.3s ease, border-width 0.3s ease",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
    borderWidth: "1px",
    "&:hover": {
        boxShadow: "0px 10px 14px rgba(0, 0, 0, 0.5)",
        borderWidth: "5px"
    },
});

const ItemImage = styled('img')({
    width: "100px",
    height: "100px",
    objectFit: "cover",
    margin: "auto",
    padding: "15px",
    display: "block",
});

export default function SingleAddonView (props) {

  const { role } = UseUserContext();
  const { addon } = props;
  const { addonID } = addon;
  const { goToEditAddon } = NavigationHelper(); 

  if (role === "blocked") {
    alert("You are blocked you can't delete or edit addons");
    return;
  }

  const handleDelete = () => {
    const path = `/${DOCS.ADDONS}/${addonID}/state/`;
    const state = 'deleted';
    db.set(path, state)
    alert("Addon deleted successfully");
  }
  
  const handleEdit = () => {
    addonUtils.edit(addonID ,goToEditAddon);
  }


   

    
    return (
        <>
            <Item key={addon?.addonID} sx={{ m: 1, p: 1 }}>
                <ItemImage src={addon?.addonImg} alt={addon?.name} loading="lazy" />
                <Typography sx={{ fontSize: "16px", textAlign: "center", margin: "5px", fontWeight: "600" }}>{addon?.name}</Typography>
                <Typography sx={{ fontSize: "12px", display: "flex", alignItems: "center", justifyContent: "space-between", margin: "5px" }}>
                    <span>{addon?.creator}</span>
                    <span style={{ display: "flex", alignItems: "center" }}>
                        <DownloadDoneIcon />
                        {addon?.downloads}
                    </span>
                </Typography>
                <div style={{ display: "flex", alignItems: "center" }} >
                    <Button variant="contained" sx={{ margin: "10px", width: "100px" }} onClick={handleEdit}>Edit</Button>
                    <span>&nbsp;</span>
                    <Button variant="contained" sx={{ margin: "10px", width: "100px" }} onClick={handleDelete}>Delete</Button>
                </div>
            </Item>
        </>
    )
}

SingleAddonView.propTypes = {
    addon: PropTypes.object,
}

