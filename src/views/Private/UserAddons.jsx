import { Tab, Tabs, Typography, Grid, Box, styled } from "@mui/material"
import { TabContext, TabPanel } from '@mui/lab';
import { useEffect, useState } from "react";
import { UseAppData } from "../../context/AppContext";
import { UseUserContext } from "../../context/UserContext";
import { onValue, ref } from "firebase/database";
import { database } from "../../config/firebase-config";
import SingleAddonView from "./SingleAddonView";
import { db } from "../../services/database-services";

const Container = styled('div')({
  display: "grid",
  gridTemplateColumns: "repeat(auto-fill, minmax(calc(20% - 20px), 1fr))",
  gap: "20px",
  width: "80%",
  margin: "50px auto",
});


export default function MyAddons() {

  const { userData } = UseUserContext();
  const { ...appState } = UseAppData();
  const appAddons = Object.values(appState?.addons) || [];
  const array = appAddons.filter(el => el.state !== "deleted" && userData.addons.includes(el.addonID))
                         .map(el => el);

  const [allAddons, setAllAddons] = useState(appAddons);
  const [userAddons, setUserAddons] = useState(array);
  const [pending, setPending] = useState([]);
  const [approved, setApproved] = useState([]);
  const [drafts, setDrafts] = useState([]);

  
  const [tabValue, setTabValue] = useState('1');

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };
  
  
  
  useEffect(() => {

    const addons = userAddons.filter(item => item.state !== "deleted");
    const pending = addons.filter(item => item.state === "pending");
    const approved = addons.filter(item => item.state === "approved");
    const drafts = addons.filter(item => item.state === "draft");
    setUserAddons(addons);
    setPending(pending);
    setApproved(approved);
    setDrafts(drafts);

    const newResult = async () => {
      const snapshot = await db.get("addons");
      const array = Object.values(snapshot);
      let gatherAddonsResults = array.filter(el => el.state !== "deleted" && userData.addons.includes(el.addonID))
                                     .map(el => el);

      setUserAddons(gatherAddonsResults);
    }
    
    newResult();
    
  }, []);


  // const addonsRef = ref(database, `addons/`);
  // console.log("addonsRef", addonsRef);

  
  // useEffect(() => {

  //     // const handleAddonsChange = (snapshot) => {
  //     //   const addonsData = snapshot.val();
  //     //   console.log("addonsData", addonsData);
  //     //   if (addonsData) {
  //     //     const addonsArray = Object.values(addonsData);
  //     //     setAllAddons(addonsArray);
  //     //   }
  //     // };
      
  //   const addonListener = onValue(addonsRef, handleAddonsChange);

  //   // return () => {
  //   //   addonsRef.off("value", addonListener);
  //   // };
  // }, [addonsRef]); 


  // // useEffect(() => {

  // //   const updateAddon = async () => {
  // //     try {
  // //       const addonsRef = ref(database, `addons/`);
  // //       const handleAddonsChange = (snapshot) => {
  // //         const addonsData = snapshot.val();
  // //         if (addonsData) {
  // //           const addonsArray = Object.values(addonsData);
  // //           setAllAddons(addonsArray);
  // //         }
  // //       };

  // //       const addonListener = onValue(addonsRef, handleAddonsChange);

  // //       return () => {
  // //         addonsRef.off("value", addonListener);
  // //       };

  // //     } catch (error) {
  // //       console.error("Error fetching addon data:", error);
  // //     }
  // //   };

  // //   // if(userData.userID) {
  // //     updateAddon();
  // //   // }
  // // },[userData.addons]);



  /**
   * Renders a search result page.
   * @returns {JSX.Element} The JSX elements that make up the search result page.
   */
  return (
    <Box sx={{mb: "58%"}}>
      <TabContext value={tabValue}>
        <Tabs onChange={handleChange} >
          <Tab label="All Addons" value='1'/>
          <Tab label="Approved" value='2'/> 
          <Tab label="Pending" value='3'/>
          <Tab label="Drafts" value='4'/>
          <Tab label="Joint Addons" disabled/>
        </Tabs>
        <TabPanel value="1">
          <main>
            <Typography variant="h4">All Addons</Typography>
            <Grid container spacing={3}>
              <Container >
                {userAddons.map((item, index) =>
                  <SingleAddonView addon={item} key={index}/>
                  )}
              </Container>    
            </Grid>    
          </main>
        </TabPanel>
        <TabPanel value="2">
          <main>
            <Typography variant="h4">Approved</Typography>
            <Grid container spacing={3}>
              {approved.map(item =>
                  <Grid item xs={12} sm={6} md={4} lg={3} key={item.title}>
                    <SingleAddonView addon={item} />
                  </Grid>
                )}
            </Grid>    
          </main>
        </TabPanel>
        <TabPanel value="3">
          <main>
            <Typography variant="h4">Pending</Typography>
            <Grid container spacing={3}>
              {pending.map((item, index) =>
                  <Grid item xs={12} sm={6} md={4} lg={3} key={item.title}>
                    <SingleAddonView addon={item} key={index}/>
                  </Grid>
                )}
            </Grid>    
          </main>
        </TabPanel>
        <TabPanel value="4">
          <main>
            <Typography variant="h4">Drafts</Typography>
            <Grid container spacing={3}>
              {drafts.map((item, index) =>
                  <Grid item xs={12} sm={6} md={4} lg={3} key={item.title}>
                    <SingleAddonView addon={item} key={index}/>
                  </Grid>
                )}
            </Grid>    
          </main>
        </TabPanel> 
      </TabContext>
    </Box>
  )
}