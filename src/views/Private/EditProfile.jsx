import {  FormControl, FormHelperText, Stack, TextField } from '@mui/material';
import { db } from "./../../services/database-services";
import { UseUserContext } from './../../context/UserContext';
import { CustomModal } from '../../components/Modal';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { changeProfileInfoValidation } from '../../services/validation-services';
import { UseAppData } from '../../context/AppContext';


export default function EditProfile({userData}) { 

  const { role } = UseUserContext();
  const { ...data } = UseAppData();
  
  const [openModal, setOpenModal] = useState(true);
  const [warning, setWarning] = useState(false);
  const [success, setSuccess] = useState(false);


  const handleToggle = () => {
    setOpenModal(!openModal);
  };
  const phoneBook = data?.phoneNumbers;
  const [form, setForm] = useState({
    firstName: userData?.firstName,
    lastName: userData?.lastName,
    phoneNumber: userData?.phoneNumber,
    gitHubURL: userData?.gitHubURL || "",
  });

  if(!role) {
    return;
  }


  const handleInput = (e) => {
    const { id, value } = e.target;
      setForm({
        ...form,
        [id]: value,
      });
  };


  const save = async (e) => {
    e.preventDefault();
    await updateProfile(form);

  }

  const updateProfile = async (form) => {
    const { firstName, lastName, phoneNumber, gitHubURL } = form;
    if ( userData.firstName === firstName && userData.lastName === lastName && userData.phoneNumber === phoneNumber && userData.gitHubURL === gitHubURL) {
      setSuccess("No changes were made!");
      setTimeout(() => { handleToggle(); }, 2000);
      return;
    }

    const validationResult = changeProfileInfoValidation(firstName, lastName, gitHubURL, phoneNumber, phoneBook);
    setWarning(validationResult);
    console.log(form, validationResult, phoneNumber.length)

    if (validationResult) {
      return; // Prevent form submission if there are validation errors
    }
    try {
      db.set(`/users/${userData.userID}/firstName`, form.firstName);
      db.set(`/users/${userData.userID}/lastName`, form.lastName);
      db.set(`/users/${userData.userID}/phoneNumber`, form.phoneNumber);
      db.set(`/users/${userData.userID}/gitHubURL`, form.gitHubURL);
      console.log("Profile updated successfully", form);
      setSuccess("Profile updated successfully");
      setTimeout(() => { handleToggle(); }, 2000);
    } catch (error) {
      console.error("Error updating profile:", error.message);
      }
   }


  return (
    <CustomModal 
    openByDefault={openModal}
    onClose={save}
    title="Change profile info"
    modalHeight={500}
    modalWidth={700}
    >
      <form>
        <Stack className="use-container">
          <FormControl name="firstName">
          {warning && <FormHelperText id="my-helper-text" sx={{color: "red"}}>{warning}</FormHelperText>}
          {success && <FormHelperText id="my-helper-text"sx={{color: "green"}}>{success}</FormHelperText>}
            <TextField id="firstName" label="First Name" onChange={handleInput} value={form.firstName} />
          </FormControl>
          <FormControl name="lastName">
            <TextField id="lastName" label="Last Name" onChange={handleInput} value={form.lastName} />
          </FormControl>
          <FormControl name="gitHubURL">
            <TextField id="gitHubURL" label="GitHub URL" onChange={handleInput} value={form.gitHubURL} />
          </FormControl>
          <FormControl name="phoneNumber">  
            <TextField id="phoneNumber" label="Phone Number" onChange={handleInput} value={form.phoneNumber} />
          </FormControl>
        </Stack>
      </form>
    </CustomModal>
);

}

EditProfile.propTypes = {
  userData: PropTypes.object
};