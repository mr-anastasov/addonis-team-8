import { Box, Button, TextField, Typography, DialogContentText, Input, ButtonGroup, Grid,  InputLabel, FormControl, Select, MenuItem, ListItemText} from '@mui/material';
import { useState } from 'react';
import { UseUserContext } from '../../context/UserContext';
import { createAddon } from '../../services/addon-services';
import { uploadRepoData, fetchRepoFileContent } from '../../services/git-hub';
import { createAddonValidation } from '../../services/validation-services';
import { db } from '../../services/database-services';
import { store } from '../../services/storage-services';
import { GITHUB_REPO } from '../../common/constants';
import NavigationHelper from '../../common/helper';
import { EditModal } from '../../components/ModalEdit';

export default function CreateAddon() {
  const { role, userData } = UseUserContext();
  const { goHome } = NavigationHelper();
  const [file, setFile] = useState(null);
  const [image, setImage] = useState(null);
  const [preview, setPreview] = useState(false);
  const [previewData, setPreviewData] = useState();
  const [redirect, setRedirect] = useState(null);
  const [warning, setWarning] = useState(false);
  const [form, setForm] = useState({
    name: "",
    IDE: "",
    creator: userData?.username,
    creatorID: userData?.userID ,
    description: "",
    tags: "",
    originUrl: "",
    downloadURL: "",
    addonImg:"",
    created: new Date().toLocaleDateString('en-US', {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric',
    }),
    state: "pending",
    downloads: 0,
    rating: 0,
  });

  if (role === "blocked") {
    return (
      <>
        <Typography variant="h3" textAlign="center" sx={{ m: 1, p: 1 }}>You are blocked, you can't create addon</Typography>
        {/* <TextField>Ask for explanation</TextField> */}
        <Button onClick={goHome}>Go Home</Button>
      </>
    );
  }

  const handleFileChange = (e) => {
    setFile(e.target.files[0]);
  }

  const handleImageChange = (e) => {
    setImage(e.target.files[0]);
    handleImagePreview(e);
  };

  const handleInput = (e) => {
    const { id, value } = e.target;
      setForm({
        ...form,
        [id]: value,
      });
  };


  const handleReset = () => {
    setForm({
      name: "",
      creator: userData?.username,
      creatorID: userData?.userID ,
      IDE: "",
      description: "",
      tags: "",
      originUrl: "",
      downloadURL: "",
      addonImg:"",
      created: new Date().toLocaleDateString('en-US', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
      }),
      state: "pending",
      downloads: 0,
      rating: 0,
    });
    const uploadImageInput = document.getElementById('upload-image');
    const uploadFileInput = document.getElementById('upload-file');
    
    if (uploadImageInput && uploadFileInput) {
      const newUploadImageInput = document.createElement('input');
      newUploadImageInput.type = 'file';
      newUploadImageInput.id = 'upload-image';
      newUploadImageInput.name = 'upload-image';
      newUploadImageInput.accept = 'image/*';
      newUploadImageInput.required = true;
      newUploadImageInput.addEventListener('change', handleImageChange);
      
      const newUploadFileInput = document.createElement('input');
      newUploadFileInput.type = 'file';
      newUploadFileInput.id = 'upload-file';
      newUploadFileInput.name = 'upload-file';
      newUploadFileInput.required = true;
      newUploadFileInput.addEventListener('change', handleFileChange);
      
      uploadImageInput.parentNode.replaceChild(newUploadImageInput, uploadImageInput);
      uploadFileInput.parentNode.replaceChild(newUploadFileInput, uploadFileInput);
    }
    setWarning(false);
  };


  const handleFileUpload = async (event) => {
    if (event) {
      try {
        await uploadRepoData(GITHUB_REPO, userData.username, 'adding file', event)
        .then(`File ${event.name} uploaded successfully to GitHub!`)
      } catch (error) {
        console.error('Error uploading repository file:', error.message);
      }
    }
  };

  const handleDownload = async (name) => {
    try {
      return fetchRepoFileContent(GITHUB_REPO, userData.username, name);
    } catch (error) {
      console.error('Error downloading repository file:', error);
    }
  }

  const handleDraft = async (e) => {
    setForm({
      ...form,
      state: "draft",
    });
    await handleSubmit(e);
  }

  

  const handlePreview = async (e) => {
    console.log("I got here", form, preview, image)
    setPreviewData({ ...form, addonImg: image });
    setPreview(true);
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const { name, tags, originUrl } = form;
      const validationResult = createAddonValidation(name, tags, originUrl);
      setWarning(validationResult);
      // validation for originUrl
      if (!validationResult) {
        await handleFileUpload(file);
        const id = await createAddon(form);
        const imgUrl = await store.uploadAddonImage(image, id, `AddonImage`)
        const fileUrl = await handleDownload(file.name);
        db.set(`/addons/${id}/downloadURL`,fileUrl);
        db.set(`/addons/${id}/addonImg`,imgUrl);
        db.set(`/addons/${id}/addonID`,id);
        db.set(`/addons/${id}/fileName`, file.name);
        setPreviewData({ ...form, downloadURL: fileUrl, addonImg: imgUrl, addonID: id });
        
        if(form.state === "draft") {
          alert('Addon saved as draft!')
        } else {
          alert('Addon created successfully!')
        }
        if (warning === false || warning !== '') {
          setPreview(true);
        }

        setRedirect(() => goHome);

        handleReset();
      }
    } catch(error) {
      console.error('Error creating addon:', error.message);
      alert(`Error occurred while creating the addon!`);
    }
    
  };


  const handleImagePreview = (e) => {
    const imagePreview = document.getElementById('image-preview');  
    const fileInput = e.target;

    if (fileInput.files && fileInput.files[0]) {
      const reader = new FileReader();
      
      reader.onload = function (e) {
        imagePreview.src = e.target.result;
      };
      
      reader.readAsDataURL(fileInput.files[0]);
    } else {
      imagePreview.src = ''; // Clear the image if no file is selected
    }

  }


  const handleIDEChange = (event) => {
    const { target: { value },} = event;
    setForm({
      ...form,
      IDE: value,
    });
  };

  

  return (
    <form>
      <Grid container  margin={2} padding={4} marginBottom="27%">
        <Grid item xs={9}>
          <Box component="form" sx={{ '& .MuiTextField-root': { p: 1, m: 2, width: '70%' } }} noValidate autoComplete="off">
            <Typography variant="h3" textAlign="center" sx={{ m: 1, p: 1, color: "#012101" }}>Create your Addon</Typography>
            {warning && (
              <DialogContentText className={alert ? 'animated-alert' : ''} sx={{ color: 'red', textAlign: 'center', transition: 'opacity 0.3s' }}>
                { warning }
              </DialogContentText>)
            }
            <Grid container spacing={3}>
              <Grid item xs={4.9}>
                <TextField label="Addon Name" variant="outlined" id="name" value={form.name} onChange={handleInput} />
              </Grid>
              <Grid item xs={4.9}>
              {/* <div> */}
                <FormControl sx={{ m: 1, width: 200, mt: "16px", color: "red"}}>
                  <Select
                    label="IDE Name"
                    // id="demo-multiple-checkbox"
                    className='select-input-user'
                    multiple={false}
                    value={form.IDE || "VS Code" }
                    onChange={handleIDEChange}
                    sx={{ color: "#000"}}
                  >
                    <MenuItem key={'VS Code'} value={'VS Code'} >
                      <ListItemText primary={'VS Code'}  sx={{color: "red"}}/>
                    </MenuItem>
                    <MenuItem key={'JetBrains'} value={'JetBrains'} >
                      <ListItemText primary={'Jet Brains'}  sx={{color: "red"}}/>
                    </MenuItem>
                    <MenuItem key={'Eclipse'} value={'Eclipse'} >
                      <ListItemText primary={'Eclipse'} sx={{color: "red"}} />
                    </MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <TextField label="Addon Description" id="description" sx={{color: "black"}} multiline rows={6} value={form.description} onChange={handleInput} />
              </Grid>
              <Grid item xs={12}>
                <TextField id="originUrl" label="Origin link" variant="outlined" onChange={handleInput} required={true} value={form.originUrl} />
              </Grid>
              <Grid item xs={12}>
                <TextField id="tags" label="Tags" variant="outlined" onChange={handleInput} required={true} value={form.tags} />
              </Grid>
              <Grid item xs={4}>
                <InputLabel htmlFor="upload-image">Addon Image</InputLabel>
                <Input type="file" id="upload-image" name="upload-image" accept="image/*" required={true} onChange={handleImageChange}
                sx={{ marginLeft: 3, m: 1, p: 1 }}  />
              </Grid>
              <Grid item xs={4}>
                <InputLabel htmlFor="upload-file">Addon File</InputLabel>
                <Input type="file" id="upload-file" name="upload-file" required={true} onChange={handleFileChange} 
                sx={{ marginLeft: 1, m: 1, p: 1 }} />
              </Grid>
              <Grid item xs={4}>
              </Grid>
              <Grid item xs={4}>
              </Grid>
                <Box sx={{ display: 'flex', justifyContent: 'center', marginTop: '1rem' }}>
                  <ButtonGroup >
                    <Button variant="contained" onClick={handleReset} sx={{m: 1, }}>Reset</Button>
                    <Button variant="contained" onClick={handleDraft} sx={{m: 1, }}>Save draft</Button>
                    <Button variant="contained" onClick={handlePreview} sx={{m:1, }}>Preview</Button>
                    <Button variant="contained" onClick={handleSubmit} sx={{m: 1, }}>Create</Button>
                  </ButtonGroup>
                </Box>
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={3}>
          {/* <Paper sx={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center', height: '45vh' }}> */}
            <img id="image-preview" src="" alt="" style={{ maxWidth: '100%', maxHeight: '100%' }} />
          {/* </Paper> */}
        </Grid>
      </Grid >

      <EditModal addon={previewData} open={preview} setOpen={setPreview} redirectTo={redirect} />
    </form>
  );
}
