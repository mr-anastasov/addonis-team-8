import { styled } from '@mui/material/styles';
import {  Box, Paper, Grid, Typography, Button } from '@mui/material';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useState } from 'react';


const modalStyle = {
  position: 'absolute',
  maxWidth: '80%',
  margin: '0 auto',
  padding: '10px',
  backgroundColor: '#99979a',
  borderRadius: '10px',
  boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
  overflowY: 'auto',
};


const ItemImage = styled('img')({
  width: "300px",
  height: "300px",
  objectFit: "cover",
  margin: "auto",
  marginTop: '20px',
  padding: "15px",
  display: "block",
});


export default function DetailAddon(prop) {

  // const [open, setOpen] = useState(false);
  const { addon, onClose, open } = prop;

  const handleClose = () => {
    
  }

  console.log(onClose)

  return (
    open && (
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        minHeight="100vh"
        backgroundColor="rgba(0, 0, 0, 0.5)"
        position="fixed"
        top="0"
        left="0"
        right="0"
        bottom="0"
        zIndex="1000"
        sx = {{ overflowY: 'auto'}}
      >
        <Grid container spacing={2} style={{ ...modalStyle, marginTop: '20px' }} key={addon?.key}>
          <Grid item xs={12} sm={6}>
              <Paper sx={{ m: 2, p: 2, textAlign: "center"}}>
                <Typography variant="h6" component="div">{addon?.name}</Typography>
              </Paper>
              <ItemImage src={addon?.addonImg} alt={addon?.name} />
          </Grid>
          <Grid item xs={12} sm={6}>
              <Paper sx={{ m: 2, p: 2}}>
                <Typography variant="h6" component="div">IDE: {addon?.IDE}</Typography>
                <Typography variant="h6" component="div">Creator: {addon?.creator}</Typography>
                <Typography variant="h6" component="div">Tags: {addon?.tags}</Typography>
                <Typography variant="h6" component="div">Download link: {<Link to={addon?.downloadURL}>{addon?.downloadURL}</Link>} </Typography>
                <Typography variant="h6" component="div">Origin link: {<Link to={addon?.originUrl}>{addon?.originUrl}</Link>}</Typography>
                <Typography variant="h6" component="div">Number of downloads: {addon?.downloads}</Typography>
              </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper sx={{ m: 2, p: 2}}>
              <Typography variant="h6" component="div">{addon?.description}</Typography>
              </Paper> 
          </Grid>
          <Button variant="contained" onClick={handleClose}>close</Button>
        </Grid>
      </Box>
    )
  );
}


DetailAddon.propTypes = {
  addon: PropTypes.object,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};