import { useState } from 'react';
import { Link, Typography, Dialog, DialogTitle, DialogActions, DialogContentText, DialogContent, TextField, Button } from '@mui/material';
import { handleLogin } from '../../services/users-services';
import { isValidEmail } from '../../services/validation-services';
import NavigationHelper from '../../common/helper';
import { PATHS } from '../../common/constants';
import { UseUserContext } from '../../context/UserContext';
import { UseAppData } from '../../context/AppContext';
import { ParticlesBackground } from '../../components/Addons/ParticlesBackground/ParticlesBackground';
/**
 * Represents the Login component.
 * @function Login
 * @returns {JSX.Element} Login component JSX element.
 */
export default function Login() {
  const { SIGN_UP } = PATHS;
  const { goHome } = NavigationHelper();
  const { setContext } = UseUserContext();
  const { ...data } = UseAppData();
  const [warning, setWarning] = useState(false);
  const [login, setLogin] = useState({
    email: "",
    password: "",
  });
  
  /**
   * Handles input changes for email and password fields.
   * @function handleInput
   * @param {Object} e - The input change event.
   * @returns {void}
   */
  const handleInput = (e) => {
    const { id, value } = e.target;
    setLogin({
      ...login,
      [id]: value,
    });
  };

  /**
   * Handles the login button click. 
   * Validates the email and password fields
   * calls the handleLogin service.
   * Sets the context with the user data.
   * Redirects to the home page.
   * @async
   * @function handleClick
   * @returns {void}
   */
  const handleClick = async () => {

    if (data.banned.includes(login.email)) {
      setWarning('You are banned from the system!');
      return;
    }


    const { email, password } = login;
    if (!email || !password) {
      setWarning('Please fill in both email and password fields!');
    } else if (!isValidEmail(email)) {
      setWarning('Please enter a valid email address!');
    } 
    try {
      const data = await handleLogin(email, password);
      setContext(
        {
          role: data.role,
          userData: {...data},
        }
      )
      console.log(`User have been authenticated and the context is set with this data:`, data)
      goHome();
    } catch (error) {
      console.log(`Error: ${error.message}`);
      setWarning('Invalid email or password!');
    }
  };
  
  return (
    <div style={{marginBottom: "66%"}}>
      <ParticlesBackground/>
      <Dialog open={true} onClose={goHome} fullWidth maxWidth="md" sx={{ m: '10px', p: '10px' }}>
        <DialogTitle sx={{ m: 1, p: 3, textAlign: 'center' }}>Log In</DialogTitle>
        <DialogContent sx={{ minHeight: '210px', m: 1, p: 2, textAlign: 'center' }}>
          {warning && (
            <DialogContentText
              className={warning ? 'animated-alert' : ''}
              sx={{ color: 'red', textAlign: 'center', transition: 'opacity 0.3s' }}
            >
              {warning}
            </DialogContentText>
          )}
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label="Email Address"
            type="email"
            required={true}
            style={{ width: '60%' }}
            variant="standard"
            value={login.email}
            onChange={handleInput}
          />
          <TextField
            autoFocus
            margin="dense"
            id="password"
            label="Password"
            type="password"
            required={true}
            style={{ width: '60%' }}
            variant="standard"
            value={login.password}
            onChange={handleInput}
          />
          <DialogContentText sx={{ m: 1, p: 1, textAlign: 'center' }}>
            Please enter your email and password to login.
          </DialogContentText>
        </DialogContent>
        <DialogActions sx={{ justifyContent: 'center' }}>
          <Button onClick={goHome}>Cancel</Button>
          <Button onClick={handleClick}>Login</Button>
        </DialogActions>
        <DialogActions sx={{ justifyContent: 'center' }}>
          <Typography sx={{ m: 2 }}>You are a new user? Go and register here:</Typography>
          <Link href={SIGN_UP} sx={{ m: 2 }}>
            Register
          </Link>
        </DialogActions>
      </Dialog>
    </div>
  );
}