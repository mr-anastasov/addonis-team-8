import { Grid, Typography } from "@mui/material";
import PropTypes from 'prop-types';
import AddonSmall from "../../components/Addons/AddonSmall";
export default function TabPanel (props) {


  const { tabName, data } = props;
  console.log(tabName,data)
  
  return (
    <main>
      <Typography variant="h4">{tabName}</Typography>
      <Grid container spacing={3}>
         {data.map(item =>
            <Grid item xs={12} sm={6} md={4} lg={3} key={item.title}>
               <AddonSmall addon={item} />
            </Grid>
          )}
      </Grid>    
    </main>
  );
}

TabPanel.propTypes = {
  tabName: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  value: PropTypes.string.isRequired,
};