import { useState } from "react";
import { db } from "./../../services/database-services";
import { Stack,  FormControl, InputAdornment } from "@mui/material";
import UploadIcon from '@mui/icons-material/Upload';
import { store } from "./../../services/storage-services";
import { CustomModal } from "../../components/Modal";
import PropTypes from 'prop-types';
export default function ChangePicture({data}) {

  const [ image, setImage ] = useState(data?.imageUrl || null);
  const [openModal, setOpenModal] = useState(true);

  const handleToggle = () => {
    setOpenModal(!openModal);
  };

  const handleImageUpload = (event) => {
    handleImagePreview(event);
    setImage(event.target.files[0]);
  };

  const handleImagePreview = (e) => {
    const imagePreview = document.getElementById('image-preview');  
    const fileInput = e.target;

    if (fileInput.files && fileInput.files[0]) {
      const reader = new FileReader();
      
      reader.onload = function (e) {
        imagePreview.src = e.target.result;
      };
      
      reader.readAsDataURL(fileInput.files[0]);
    } else {
      imagePreview.src = ''; // Clear the image if no file is selected
    }

  }


  

  const handleUpload = async () => {
    console.log("Uploading image...");
    try {
      const uid = data.userID;
      const imgID = await store.uploadAddonImage(image, uid);
      console.log(uid);
      db.set(`/users/${uid}/imageUrl`, imgID);
      console.log("Image uploaded successfully");
      handleToggle();
    } catch(error) {
      console.log("Failed to upload image", error.message);
    }
  }



  return (
    <CustomModal 
    title="Change picture"
    modalHeight={500}
    modalWidth={700}
    submitText={"Save"}
    onClose={handleUpload}
    openByDefault={openModal}
    >
      <FormControl id="" sx={{ m: 3, p: 2 }}>
        <Stack direction={['column', 'row']} spacing={2}>
          <img id="image-preview" src={data?.imageUrl} alt="preview" style={{ width: 300, height: 300 }} />
          {/* <Avatar  id='image-preview' src={data?.imageUrl} sx={{ width: 300, height: 300 }}></Avatar> */}
          <InputAdornment position="end">
            <label htmlFor="fileInput">
              <UploadIcon sx={{ width: 50, height: 50 }} />
            </label>
            <input type="file" id="fileInput" onChange={handleImageUpload} multiple />
          </InputAdornment>
        </Stack>
      </FormControl>
    </CustomModal>
  );
}

ChangePicture.propTypes = {
  data: PropTypes.object
};