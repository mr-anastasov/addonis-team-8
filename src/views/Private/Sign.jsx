import { useState } from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContentText, DialogContent, TextField, Button } from '@mui/material';
import { registrationValidation } from '../../services/validation-services';
import { handleRegister } from '../../services/users-services';
import NavigationHelper from '../../common/helper';
import { createGitHubRepository } from '../../services/git-hub';
import { UseUserContext } from '../../context/UserContext';
import { UseAppData } from '../../context/AppContext';
import { ParticlesBackground } from '../../components/Addons/ParticlesBackground/ParticlesBackground';
/**
 * Represents the Register component.
 * @function Register
 * @returns {JSX.Element} Register component JSX element.
 */
export default function Register() {
  // const { PRIVACY, RULES } = PATHS;
  const { goHome, goToLogin } = NavigationHelper();
  const { phoneNumbers } = UseAppData();
  const { setContext } = UseUserContext();
  const [warning, setWarning] = useState(false);
  const [form, setForm] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    username: "",
    phoneNumber: "",
    mailbox:["0"],
  });

  /**
   * Handles input changes for the registration form.
   * @function handleInput
   * @param {Object} e - The input change event.
   * @returns {void}
   */
  const handleInput = (e) => {
    const { id, value } = e.target;
    setForm({
      ...form,
      [id]: value,
    });
  };
  /**
   * Handles form submission for user registration.
   * sets the warning state depending on the validation result.
   * If the form is valid the handleRegister service is called,
   * the context is set with the user data
   * the user is redirected to the home page.
   * @function handleSubmit
   * @param {Object} e - The form submission event.
   * @returns {void}
   */


  const handleSubmit = async (e) => {
    e.preventDefault();

    // if (!firstName || !lastName || !email || !password || !username || !phoneNumber) {
    //   setWarning('All fields are required.');
    //   return; 
    // }

    const { firstName, lastName, email, password, username, phoneNumber } = form;
    const validationResult = registrationValidation(firstName, lastName, email, password, username, phoneNumber, phoneNumbers);
    setWarning(validationResult);

    if (validationResult) {
      return; // Prevent form submission if there are validation errors
    }

      try {
        await handleRegister(form);
        setContext({
          role: 'user',
          userData: { ...form},
        })
  
        const repo = await createGitHubRepository(username);
        console.log(`User was created and repo with the same name as the ${username} was created for the user`, repo);
        goHome();
      } catch (error) {
        console.log(`Could not create user: ${error.message}`);
      }
  };

  /**
   * Resets the registration form and warning state.
   * @function handleReset
   * @returns {void}
   */
  const handleReset = () => {
    setForm({
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      username: "",
      phoneNumber: "",
    });
    setWarning(false);
  };


  return (
    <div style={{marginBottom: "66%"}}>
      <ParticlesBackground/>
      <Dialog open={true} onClose={goToLogin} fullWidth maxWidth="md" sx={{m: "10px", p: "10px"}}>
          <DialogTitle sx={{m: "10px", p: "20px", textAlign: "center"}}>Register</DialogTitle>
          <DialogContent sx={{minHeight: "150px", m: "10px", p: "20px", textAlign: "center"}} >
            <DialogContentText>
              All fields are required for sign up. You will receive an email to verify your account.
            </DialogContentText>
            { warning && (<DialogContentText className={ warning ? 'animated-alert' : ''} 
                        sx={{ color: 'red', textAlign: 'center', transition: 'opacity 0.3s' }}>
                        {warning}
                      </DialogContentText>)}
            <DialogContent sx={{display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <TextField
                autoFocus
                margin="dense"
                id="firstName"
                label="First Name"
                type="text"
                sx={{width: "60%", m: "4px" }}
                variant="outlined"
                value= {form.firstName}
                onChange={handleInput}
              />
              <TextField
                autoFocus
                margin="dense"
                id="lastName"
                label="Last Name"
                type="text"
                sx={{width: "60%", m: "4px" }}
                variant="outlined"
                value= {form.lastName}
                onChange={handleInput}
              />
            </DialogContent>
            <DialogContent sx={{display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <TextField
                autoFocus
                margin="dense"
                id="email"
                label="Email Address"
                type="email"
                sx={{width: "60%", m: "4px" }}
                variant="outlined"
                value= {form.email}
                onChange={handleInput}
              />
              <TextField
                autoFocus
                margin="dense"
                id="password"
                label="Password"
                type="password"
                sx={{width: "60%", m: "4px" }}
                variant="outlined"
                value= {form.password}
                onChange={handleInput}
              />
            </DialogContent>
            <DialogContent sx={{display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <TextField
                autoFocus
                margin="dense"
                id="username"
                label="Username"
                type="text"
                sx={{width: "60%", m: "4px" }}
                variant="outlined"
                value= {form.username}
                onChange={handleInput}
              />
              <TextField
                autoFocus
                margin="dense"
                id="phoneNumber"
                label="Phone number"
                type="tel"
                sx={{width: "60%", m: "4px" }}
                variant="outlined"
                value= {form.phoneNumber}
                onChange={handleInput}
              />
            </DialogContent>
            </DialogContent>  
            {/* <DialogActions sx={{justifyContent: "center"}}>
              <Typography  sx={{m: "4px"}}>Before you proceed, please verify that you agree with our privacy policy and terms of service</Typography>
            </DialogActions>
            <FormGroup sx={{justifyContent: "center", alignItems: "center", flexDirection: "column"}}>
              <FormControlLabel required control={<Checkbox />} label={
                <Link href={RULES}>Terms of service</Link>}/>
              <FormControlLabel required control={<Checkbox />} label={
                <Link href={PRIVACY}>Privacy policy</Link>} />
            </FormGroup> */}
            <DialogActions sx={{justifyContent: "center"}}>
              <Button onClick={handleReset}>Reset</Button>
              <Button onClick={handleSubmit}>Register</Button>
            </DialogActions>
          </Dialog>
      </div>
    );
  }