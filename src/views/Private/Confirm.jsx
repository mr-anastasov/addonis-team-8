import { Button, Input, Typography, Box } from "@mui/material";
import { useState } from "react"; 

export default function Confirm() {

  const [password, setPassword] = useState('');
  const handleClick = () => {
    console.log(password);
  }

  return (
    <Box
    display="flex"
    flexDirection="column"
    alignItems="center"
    justifyContent="center"
    height="20vh" // Adjust the height as needed
    marginTop="20vh" // Adjust the top margin to move it up the page
    >
      <Typography variant="h4">Confirm your changes</Typography>
      <Input
        type="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <Typography variant="body1">Please type your password and hit save to confirm</Typography>
      <Button variant="contained" color="primary" onClick={handleClick}>Save</Button>
    </Box>
  )
}