import { Typography, ButtonGroup, Button, Box, Grid, Paper, } from '@mui/material';
import { UseUserContext } from './../../context/UserContext';
import { useEffect, useState } from 'react';
import NavigationHelper from '../../common/helper';
import ChangePicture from './ChangePicture';
import EditProfile from './EditProfile';
import ChangePassword from './ChangePassword';
import { db } from '../../services/database-services';
import { ref } from 'firebase/storage';
import { database } from '../../config/firebase-config';
export default function UserProfile() {

  const { userData, role } = UseUserContext();
  const [image, setImage] = useState(userData?.imageUrl || "");
  const [openModals, setOpenModals] = useState({
    profileModal: false,
    passwordModal: false,
    pictureModal: false,
  });

  const toggleModal = (modalName) => {
    setOpenModals((prevState) => ({
      ...prevState,
      [modalName]: !prevState[modalName],
    }));
  };

  useEffect(() => {
    const getData = async () => {
      const ref = ref(database, `users/${userData?.userID}`);
      // setImage(url);

      // const data = await ref;
      // console.log(data);
    }
    getData();
  }, [userData]);


  const { goToEditUser, goToMyAddons, goHome, goToCreateAddon, goToConfirm } = NavigationHelper();
  const [warning, setWarning] = useState(false);

  if (role === "blocked") {
    setWarning('You are blocked!\nPlease contact the administrator!');
  }


  return (

    <Box marginBottom= "27%">
      <Typography variant="h4">User profile</Typography>
          { warning && <Typography>{warning}</Typography> }
      <Grid container spacing={2}>
        <Grid item xs={12} sm={4} >
          <Box>
            <ButtonGroup  orientation="vertical" sx={{m: 1, p: 1}}>            
              <Button variant="contained" sx={{m: 3, p: 1.5}} onClick={() => toggleModal("profileModal")}>Edit profile</Button>
              <Button variant="contained" sx={{m: 3, p: 1.5}} onClick={() => toggleModal("passwordModal")}>Change password</Button>
              <Button variant="contained" sx={{m: 3, p: 1.5}} onClick={() => toggleModal("pictureModal")}>Change picture</Button>
              <Button variant="contained" sx={{m: 3, p: 1.5}} onClick={goToMyAddons}>My addons</Button>
              <Button variant="contained" sx={{m: 3, p: 1.5}} disabled>Invite a friend</Button>
              <Button variant="contained" sx={{m: 3, p: 1.5}} disabled>Metrics</Button>
            </ButtonGroup>  
          </Box>
        </Grid>
        <Grid item xs={10} sm={4} >
          <Box>
            <Paper>
              <Typography sx={{m: 2, p: 1}}>User email: {userData?.email}</Typography>
              <Typography sx={{m: 2, p: 1}}>Username: {userData?.username}</Typography>
              <Typography sx={{m: 2, p: 1}}>User type: {userData?.role}</Typography>
              <Typography sx={{m: 2, p: 1}}>Number of addons: {userData?.addons?.length}</Typography>
              <Typography sx={{m: 2, p: 1}}>Phone number: {userData?.phoneNumber}</Typography>
              <Typography sx={{m: 2, p: 1}}>GitHub: {userData?.gitHubURL}`</Typography> 
            </Paper>
          </Box>
        </Grid>

        <Grid item xs={12} sm={4} sx={{ alignItems:"center", display:"flex", justifyContent:"center"}}>
          {image && <img src={image} width='350px' height="350px" />}
        </Grid>
      </Grid>
      {openModals.profileModal && (
          <EditProfile userData={userData} />)}
      {openModals.pictureModal && (
          <ChangePicture data={userData} />)}
      {openModals.passwordModal && (
          <ChangePassword email={userData.email}/>)}
    </Box>

  )

}