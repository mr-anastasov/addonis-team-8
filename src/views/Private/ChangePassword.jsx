import { useState } from 'react';
import { Link, DialogContentText, DialogContent, TextField,  Stack } from '@mui/material';
import { EmailAuthProvider, reauthenticateWithCredential, updatePassword } from "firebase/auth";
import { CustomModal } from '../../components/Modal';
import { auth } from '../../config/firebase-config.js';
import { isEmpty, isValidPassword } from '../../services/validation-services';
import { PASSWORD_MIN_LENGTH } from '../../common/constants';


export default function ChangePassword({email}) {

  const [openModal, setOpenModal] = useState(true);
  const [warning, setWarning] = useState(false);
  const [success, setSuccess] = useState(false);

  const [form, setForm] = useState({
    email: email || "",
    password: "",
    newPassword: "",
  });

  const handleToggle = () => {
    setOpenModal(!openModal);
  };


  const handleInput = (e) => {
    const { id, value } = e.target;
      setForm({
        ...form,
        [id]: value,
      });
  };
 
  console.log(warning)

  const handlePasswordChange = async (event) => {  
    event.preventDefault();
    if (isEmpty(form.newPassword) ) {
      setWarning(`Please type your new password!`);
      return;
    }
    if(!isValidPassword(form.newPassword)) {
      setWarning(`Password should be at least ${PASSWORD_MIN_LENGTH} characters long and contain at least one digit, one lowercase, one uppercase letter and one special symbol`);
      return;
    }
    console.log("I am here", isValidPassword(form.newPassword))
    setWarning(false);

    try {
      const userCur = auth.currentUser;
      if (!userCur) {
        return null;
      }
      console.log(userCur, form, userCur.uid);
      const credential = EmailAuthProvider.credential(email, form.password);
      console.log("I die here", credential)
      await reauthenticateWithCredential(userCur, credential);
      await updatePassword(userCur, form.newPassword);
      console.log("Password updated successfully");
      setSuccess("Password updated successfully");
      setTimeout(() => { handleToggle(); }, 2000);
    } catch (error) {
      if(error.message.includes("wrong-password")) {
        setWarning("Wrong password!");
      }
      console.log(`Couldn't change password, ${error.message}`);
    }
    setForm({
      email: "",
      password: "",
      newPassword: "",
    });
  }

  


  return (
    <>
      <CustomModal 
        onClose={handlePasswordChange}
        openByDefault={openModal}
        title="Change password"
      >
        <form>
          <DialogContent sx={{ minHeight: '210px', m: 2, p: 2, textAlign: 'center' }}>
            {warning && 
              (<DialogContentText sx={{ color: 'red', textAlign: 'center', transition: 'opacity 0.3s' }}>{warning}</DialogContentText>)
            }
            {success && 
              (<DialogContentText sx={{ color: 'green', textAlign: 'center', transition: 'opacity 0.3s' }}>{success}</DialogContentText>)
            }
            <Stack spacing={2}>
            <TextField type="password" id="password" label="password"onChange={handleInput} required={true} value={form.password} />
            <TextField type="password" id="newPassword" label="newPassword" onChange={handleInput} required={true} value={form.newPassword} />
            </Stack>
            <DialogContentText sx={{ m: 1, p: 1, textAlign: 'center' }}>
              Please enter your current password and than the new one than press save to confirm the changes.
            </DialogContentText>
            {/* <Link href={handleReset} sx={{ m: 2 }}>
              <Typography sx={{ m: 2 }}>You have forgotten your password? Send a reset email</Typography>
            </Link> */}
          </DialogContent>
        </form>
      </CustomModal>
    </>
  )
}