import { AppBar,CssBaseline, Grid, Button, ButtonGroup } from "@mui/material"
import AddonSmall from "../../components/Addons/AddonSmall"
import AddonSmallAdmin from "../../components/Addons/AddonSmallAdmin";
import data from "../../../data/data.json"

export default function SearchResult() {
  /**
   * Renders a search result page.
   * @returns {JSX.Element} The JSX elements that make up the search result page.
   */
  return (
    <>
      <CssBaseline />
      <AppBar position="relative">
        <ButtonGroup color="a" variant="contained" aria-label="large button group" size="large" disableFocusRipple={false}>
          <Button sx={{p: 2, m: 2}}>Name</Button>
          <Button sx={{p: 2, m: 2}}>Creator</Button>
          <Button sx={{p: 2, m: 2}}>Tag</Button>
          <Button sx={{p: 2, m: 2}}>Downloads</Button>
          <Button sx={{p: 2, m: 2}}>Upload Date</Button>
          <Button sx={{p: 2, m: 2}}>Last Commit Date</Button>
        </ButtonGroup>
      </AppBar>
      <main>
        <Grid container spacing={1}>
          {data.map(item =>
            <Grid item xs={12} sm={6} md={4} lg={3} key={item.title}>
              <AddonSmall addon={item}></AddonSmall>
            </Grid>
          )}
        </Grid>
        <Grid container spacing={4} sx={{m:2 }}>
          {data.map(item => <AddonSmallAdmin addon={item} key={item.title}/>)}
        </Grid>
      </main>
    </>
  )
}

