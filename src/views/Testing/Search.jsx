import { useState, useEffect } from 'react';
import { MenuItem, FormControl, ListItemText, Select, styled, InputBase, IconButton, Button } from '@mui/material/';
import SearchIcon from "@mui/icons-material/Search";
import { UseAppData } from '../../context/AppContext';
import NavigationHelper from '../../common/helper';
import { useNavigate } from 'react-router-dom';

const SearchBar = styled("div")(() => ({
  display: "flex",
  alignItems: "center",
  backgroundColor: "white",
  padding: "0 10px",
}));

const Spacing = styled("div")({
  marginLeft: "10px",
});


export default function Search() {

  const [searchVisible, setSearchVisible] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [selectedMethod, setSelectedMethod] = useState('');
  const { ...data } = UseAppData();
  const [result, setResult] = useState([]);
  const { goToSearchResults } = NavigationHelper();

  // const isSearchInputEmpty = searchInput.trim() === '';
  const navigate = useNavigate();
  const toggleSearch = () => {
    setSearchVisible(!searchVisible);
  };

  const handleInput = (event) => {
    setSearchInput(event.target.value);
  }
  const handleMethod = (event) => {
    setSelectedMethod(event.target.value);
  }

  // useEffect(() => {
  //   const finalResults = async () => {


  //       console.log(allAddons)
  //       let gatherAddonsResults = [];

  //       if (selectedMethod === "Name") {
  //         gatherAddonsResults = allAddons.slice().filter((addon) => addon.name.toLowerCase().includes(searchInput) && addon.state === "approved")
  //       }
  //       if (selectedMethod === "IDE") { 
  //           gatherAddonsResults = allAddons.slice().filter((addon) => addon.IDE.toLowerCase().includes(searchInput) && addon.state === "approved")
  //       }
  //       if (selectedMethod === "Creator") {
  //           gatherAddonsResults = allAddons.slice().filter((addon) => addon.creator.toLowerCase().includes(searchInput) && addon.state === "approved") 
  //       }

  //       if (gatherAddonsResults.length > 0) {
  //           setDisplay(true);
  //           setResult(gatherAddonsResults);
  //       } 
  //   };

  //   finalResults();
  // }, []);

  const handleClick = () => {

    const string = `${searchInput.toLowerCase().trim()},${selectedMethod}`;
    console.log(string);
    
    // Navigate to FilteredAddons and pass data via location state
    navigate('/filtered-addons', { state: { data: string } });

    // const finalResults = async () => {
      // const allAddons = Object.values(data?.addons);
      // let gatherAddonsResults = [];
      // const searchTerm = searchInput.toLowerCase().trim();

      // if (selectedMethod === "Name") {
      //   gatherAddonsResults = allAddons.slice().filter((addon) => addon.name.toLowerCase().includes(searchTerm) && addon.state === "approved");
      // }
      // if (selectedMethod === "IDE") { 
      //   gatherAddonsResults = allAddons.slice().filter((addon) => addon.IDE.toLowerCase().includes(searchTerm) && addon.state === "approved")
      // }
      // if (selectedMethod === "Creator") {
      //   gatherAddonsResults = allAddons.slice().filter((addon) => addon.creator.toLowerCase().includes(searchTerm) && addon.state === "approved") 
      // }
      // console.log(gatherAddonsResults)

      // if (gatherAddonsResults.length > 0) {
      //     setResult(gatherAddonsResults);
      // } 

    // const string = `${searchInput.toLowerCase().trim()},${selectedMethod}`;
    // console.log(string)
    console.log(result)
    goToSearchResults();

    // finalResults();

    // setDisplay(true);
  }

  console.log(result)


  return (
    <>
      <IconButton size="small" onClick={toggleSearch}>
        <SearchIcon />
      </IconButton>
      {searchVisible && (
        <>
          <div>
              <FormControl sx={{ m: 1, width: 200, mt: "16px", color: "red"}}>
                  <Select
                    label="IDE Name"
                    // id="demo-multiple-checkbox"
                    className='select-input-user'
                    multiple={false}
                    value={selectedMethod || "VS Code" }
                    onChange={handleMethod}
                    sx={{ color: "#000"}}
                  >
                    <MenuItem key={'Name'} value={'Name'} >
                      <ListItemText primary={'Name'}  sx={{color: "red"}}/>
                    </MenuItem>
                    <MenuItem key={'IDE'} value={'IDE'} >
                      <ListItemText primary={'IDE'}  sx={{color: "red"}}/>
                    </MenuItem>
                    <MenuItem key={'Creator'} value={'Creator'} >
                      <ListItemText primary={'Creator'} sx={{color: "red"}} />
                    </MenuItem>
                  </Select>
                </FormControl>
          </div>
          <Spacing />
          <SearchBar>
            <InputBase
              placeholder="Search..."
              // onBlur={handleBlur} 
              value={searchInput}
              onChange={handleInput}
              className='search-input-user'
            />
          </SearchBar>
          <Spacing />
          <Button onClick={handleClick}>Search</Button>
        </>
      )}
      </>
  );
}