import { Input, Button } from "@mui/material"
import { useState } from "react";
import { store } from "../../services/storage-services"
import { UseUserContext } from "../../context/UserContext";
export default function Form() {

  const { userData, role } = UseUserContext();
  console.log(userData, role);
  const [file, selectedFile] = useState(null);
  const handleFileChange = (event) => {
    selectedFile(event.target.files[0]);
  }


  const handleClick = async () =>{
    console.log(`I am uploading ${file.name}`);
    const url = await store.uploadAddonImage(file, 'user.uid', 'images');
    console.log(url);
  }



  return (
    <>
      <form>
        <Input
        type="file"
        id="upload-image"
        name="upload-image"
        accept="image/*"
        sx={{
          display: 'block',
          mb: 1,
        }}
        onChange={handleFileChange}
        />
        <Button onClick={handleClick}>Upload</Button>

      </form>
    </>
  )
}