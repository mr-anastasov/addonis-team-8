import { Avatar, Button, FormControl, FormHelperText, Stack, Typography, Input, InputLabel } from '@mui/material';
import { EmailAuthProvider, reauthenticateWithCredential, updateEmail, updatePassword,} from "firebase/auth";
import {  getDownloadURL, ref} from "firebase/storage";
import {  useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { uploadImg, removeImage } from '../../services/storage-services';
// import CloseIcon from '@mui/icons-material/Close';
import { storage } from "../../config/firebase-config";
import { db } from "../../services/database-services";
import { auth } from "../../config/firebase-config";
import "./EditUser.css";
import { changeProfileInfoValidation } from '../../services/validation-services';
import { UseUserContext } from '../../context/UserContext';
export default function Edit() { 
  const { username, uid, data: data } = UseUserContext();
  // const { user, data } = useContext(authContext);
  const [upload, setUpload] = useState(null);
  const [URL, setURL] = useState(null);


  const handleFileChange = (event) => {
    const selectedFile = event.target.files[0];
    setUpload(selectedFile);
  };

  const handleUpload = () => {
    // const uid = user.uid;
    uploadImg(upload, uid)
    .then((url) => {
      setUpload(null);
      clearInput();
      setURL(url);
    })
    .catch(error => `Failed during upload: ${error}`);
  }
  console.log(`This is the data`,data)
  const handleRemoveImage = () => {
    // const uid = user.uid;
    removeImage(uid)
    .then(() => {
      setURL(null);
      clearInput();
    })
    .catch(error => `Failed to remove image: ${error}`);
  }

  const clearInput = () => {
    const inputElement = document.getElementById("fileInput");
    if (inputElement) {
      inputElement.value = "";
    }
  };

  const navigate = useNavigate();

  useEffect(() => {
    if (username) {
      // Fetch the user's image URL and update the URL state
      const userImageRef = ref(storage, `AutImages/${uid}`);
      // const userImageRef = ref(storage, `AutImages/${user.uid}`);
      getDownloadURL(userImageRef)
        .then((downloadURL) => {
          setURL(downloadURL);
        })
        .catch((error) => {
          // Handle errors if necessary
          console.log(error);
        });
    }
  }, [uid]);

 
  const navigateBackwards = () => {
    navigate(-1);
  };
  const [warning, setWarning] = useState(false);
  const [form, setForm] = useState({
    firstName: data?.firstName,
    lastName: data?.lastName,
    email: data?.email,
    phoneNumber: data?.phoneNumber,
    newPassword: '',
    gitHubURL: data?.gitHubURL ? data.gitHubURL : '',
    }
  );
  

  useEffect(() => {
    if (data !== null) {
      setForm((prev) => ({
        ...prev,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        phoneNumber: data.phoneNumber,
        newPassword: '',
        gitHubURL: data.gitHubURL ? data.gitHubURL : '',
      }));
    }
  }, [data])


  const handleFormChange = (e) => {
    e.preventDefault();
    const { id, value } = e.target;
    setForm({
      ...form,
      [id]: value,
    });
  };

  const updateProfile = async (e) => {
    e.preventDefault();
    // const { firstName, lastName, email, number, password, newPassword, gitHubURL } = form;
    // const validationResult = changeProfileInfoValidation(firstName, lastName, email, number, password, newPassword, gitHubURL);
    // setWarning(validationResult);

    
    try {
      // Reauthenticate the user
      const credential = EmailAuthProvider.credential(
        form.email,
        form.password
        );
        const userCur = auth.currentUser;
      await reauthenticateWithCredential(userCur, credential);
      // Update user information in the database
     db.update(`/users/${userCur.uid}`, {
        firstName: form.firstName,
        lastName: form.lastName,
        email: form.email,
        phoneNumber: form.phoneNumber,
        gitHubURL: form.gitHubURL,
        imageURL: URL,
      });
  
      // Update email and password
      const promises = [];

      if (form.email !== data.email) {
        promises.push(updateEmail(userCur, form.email));
        console.log("Email updated successfully");
      }
  
      if (form.newPassword) {
        promises.push(updatePassword(userCur, form.newPassword));
        console.log("Password updated successfully");
      }
  
      await Promise.all(promises);
  
      console.log("Profile updated successfully");
      navigateBackwards();
    } catch (error) {
      if (error.message.includes("wrong-password")) {
        setWarning("Incorrect password");
        setForm((prev) => ({
          ...prev,
          newPassword: "",
        }));
      console.error("Error updating profile:", error.message);
      }
   }
}


  return (
    <div className="web-container">
    <Stack className="use-container">
      <Typography className="-header" >User Profile Edit</Typography>
      <FormControl id="">
        <Stack direction={['column', 'row']} spacing={2}>
          <Avatar src={URL} sx={{ width: 100, height: 100 }}></Avatar>
          { URL && (<Button sx={{ width: 16, height: 16 ,color:"red"}} 
                    onClick={handleRemoveImage}>Remove</Button>)
          }
          <div>
            <input type="file" id="fileInput" onChange={handleFileChange} />
            <Button onClick={handleUpload}>Upload</Button>
            </div>
        </Stack>
      </FormControl>
      <FormControl id="firstName" variant="outlined">
        <InputLabel htmlFor="firstName">Firstname</InputLabel>
        <Input id="firstName" aria-describedby="my-helper-text" value={form.firstName}
          onChange={handleFormChange}
          placeholder="Enter your first name"
          type="text" />
       { warning && <FormHelperText id="my-helper-text" >{ warning }</FormHelperText> }
      </FormControl>
      <FormControl id="lastName" variant="outlined">
        <InputLabel htmlFor="lastName">Last name</InputLabel>
        <Input id="lastName" aria-describedby="my-helper-text" value={form.lastName}
          onChange={handleFormChange}
          placeholder="Enter your last name"
          type="text" />
      { warning && <FormHelperText id="my-helper-text" >{ warning }</FormHelperText> }
      </FormControl>
      <FormControl id="email" variant="outlined">
        <InputLabel htmlFor="email">Email</InputLabel>
        <Input id="email" aria-describedby="my-helper-text" value={form.email}
          onChange={handleFormChange}
          placeholder="Enter your email"password
          type="text" />
      { warning && <FormHelperText id="my-helper-text" >{ warning }</FormHelperText> }     
      </FormControl>
      <FormControl id="newPassword" variant="outlined">
        <InputLabel htmlFor="newPassword">New Password</InputLabel>
        <Input id="newPassword" aria-describedby="my-helper-text" 
          onChange={handleFormChange}
          placeholder="Enter your new password"
          type="password" />
        { warning && <FormHelperText id="my-helper-text" >{ warning }</FormHelperText> }     
      </FormControl>
      <FormControl id="phoneNumber" variant="outlined">
        <InputLabel htmlFor="phoneNumber">Phone Number</InputLabel>
        <Input id="phoneNumber" aria-describedby="my-helper-text" value={form.phoneNumber}
          onChange={handleFormChange}
          placeholder="Enter your phone number"
          type="text" />
      { warning && <FormHelperText id="my-helper-text" >{ warning }</FormHelperText> }     
      </FormControl>
      <FormControl id="gitHubURL" variant="outlined">
        <InputLabel htmlFor="gitHubURL">Git Hub link</InputLabel>
        <Input id="gitHubURL" aria-describedby="my-helper-text" value={form.gitHubURL}
          onChange={handleFormChange}
          placeholder="Enter your git hub account link"
          type="text" />
      { warning && <FormHelperText id="my-helper-text" >{ warning }</FormHelperText> }     
      </FormControl>
      <FormControl id="password" variant="outlined">
        <InputLabel htmlFor="password">Password</InputLabel>
        <Input id="password" aria-describedby="my-helper-text" 
          onChange={handleFormChange}
          placeholder="Enter your current password"
          type="password" />
      { warning && <FormHelperText id="my-helper-text" >{ warning }</FormHelperText> }     
      </FormControl>



      <Stack>
        <Button className='submit-button'
        
          onClick={updateProfile}
          // disabled={
          //   formState.firstName ||
          //   formState.lastName ||
          //   formState.email ||
          //   formState.newPassword ||
          //   !formState.password ||
          //   formState.currentPassword
          // }
        >
          Submit
        </Button>
        <Button className='submit-button' onClick={navigateBackwards}>
          Cancel
        </Button>
      </Stack>
    </Stack>
  </div>
);

}

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  