import { Box, Button, ButtonBase, ButtonGroup, Grid, Typography } from "@mui/material";
import { useEffect } from "react";
import NavigationHelper from "../../common/helper"
import { UseAppData } from "../../context/AppContext";
import { getAllUsers } from "../../services/users-services";
import { UseUserContext } from "../../context/UserContext";
export default function TestDashboard() {

  const {  ...data } = UseAppData();
  const { role } = UseUserContext();                                   
  const { goTo404, goToAllUsers, goToUser , goToForm, goToTest } = NavigationHelper();
  if( role !== 'admin') {
    goTo404();
  }
  const getUsers = async() => {
    const users = [...getAllUsers()];
    console.log(users)
  }

  useEffect(() => {

    getUsers();
  },[])

  
  

  return (
    <>
      <Box>
        <Grid>
          <Typography  variant="h3">Admin Panel</Typography>
          <ButtonGroup  orientation="vertical">
            <Button variant="contained" sx={{}} onClick={goToUser}>Users</Button>
            <Button variant="contained" sx={{}} onClick={goToAllUsers}>Pending Users</Button>
            <Button variant="contained" sx={{}} onClick={goToForm}>Addons</Button>
            <Button variant="contained" sx={{}}>Pending Addons</Button>
            <Button variant="contained" sx={{}} onClick={goToTest}>Drafts</Button>
          </ButtonGroup>
          <Box>
          </Box>
        </Grid>
      </Box>
    </>
  )
}


