import { useEffect, useState } from "react";
import { Box, InputBase, InputAdornment, Paper, Stack } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { UseUserContext } from "../../context/UserContext";
import { getAllMember } from "../../services/users-services";
import SingleMember from "./SingleMember";
import { ParticlesBackground } from '../../components/Addons/ParticlesBackground/ParticlesBackground';
const Members = () => {
  const { userData } = UseUserContext();
  const [members, setMembers] = useState([]);
  const [blocked, setBlocked] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState("");
  const [searchResult, setSearchResult] = useState([]);

  useEffect(() => {
    getAllMember().then((usersArray) => setMembers(usersArray)).catch(console.error);
  }, [blocked]);

  const membersToDisplay = searchResult.map((member) => {

    return (
      <SingleMember
        key={member.uid}
        uid={member.uid}
        firstName={member.firstName}
        lastName={member.lastName}
        email={member.email}
        isBlock={member.isBlock}
        setBlocked={setBlocked}
        imageURL={member.imageUrl}
        role={member.role}

      />
    );
  });

  useEffect(() => {
    const timer = setTimeout(() => {
      const filtered = members.filter((member) => {
        if (
          member &&
          member.firstName &&
          member.lastName &&
          member.username &&
          member.phoneNumber &&
          member.email &&
          searchKeyword
        ) {
          return (
            member.firstName.toLowerCase().includes(searchKeyword) ||
            member.lastName.toLowerCase().includes(searchKeyword) ||
            member.username.toLowerCase().includes(searchKeyword)||
            member.phoneNumber.includes(searchKeyword)||
            member.email.toLowerCase().includes(searchKeyword) 
          );
        }
        return false; // Filter out undefined or incomplete members
      });
  
      setSearchResult(filtered);
    }, 500);
  
    return () => {
      clearTimeout(timer);
    };
  }, [members, searchKeyword]);

  const handleChangeSearch = (event) => {
    setSearchKeyword(event.target.value.toLowerCase());
  };

  return (
   
    <Box width="50%" margin="auto" marginBottom= "27%">
      {userData?.role === "admin" && (
        <Paper component="form">
          <InputBase
            placeholder="Search members by username or email"
            value={searchKeyword}
            onChange={handleChangeSearch}
            fullWidth
            startAdornment={
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            }
          />
        </Paper>
      )}
       <ParticlesBackground/>
      <Stack marginTop={3} spacing={3}>
        {membersToDisplay}
      </Stack>
    </Box>
  );
};

export default Members;