import { useEffect, useState } from "react";
import { db } from "../../services/database-services";
import { Typography, Button, styled, IconButton } from "@mui/material";
import DownloadDoneIcon from "@mui/icons-material/DownloadDone";
import StarIcon from "@mui/icons-material/Star";
import { addonUtils } from "../../services/addon-services";
import NavigationHelper from "../../common/helper";

//get ready
const Container = styled("div")({
  display: "grid",
  gridTemplateColumns: "repeat(auto-fill, minmax(calc(15% - 15px), 1fr))",
  gap: "23px",
  width: "80%",
  margin: "50px auto",
});

const Item = styled("div")({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  border: "1px solid #ccc",
  borderRadius: "8px",
  height: "350px",
  width: "100%",
  backgroundColor: "rgba(255, 255, 255, 0.1)",
  color: "black",
  margin: "0 10px",
  transition: "box-shadow 0.3s ease, border-width 0.3s ease",
  boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
  borderWidth: "1px",
  position: "relative", // Add this to make positioning easier
  "&:hover": {
    boxShadow: "0px 10px 14px  rgba(164,128,102,1)",
    borderWidth: "5px",
  },
});

const FeaturedButton = styled(IconButton)(({ featured }) => ({
  position: "absolute",
  top: "5px",
  right: "5px",
  zIndex: 1,
  color: featured ? "#b77b63" : "#55a49e",
}));
const ItemImage = styled('img')({
  width: "100px",
  height: "100px",
  objectFit: "cover",
  margin: "auto",
  padding: "15px",
  display: "block",
});
export default function Addon() {

  const [addonData, setAddonData] = useState({});
  const { goToAdminEdit } = NavigationHelper();

  useEffect(() => {
    const addon = async () => {
      const add = await db.get("addons");

      const toArray = Object.keys(add).filter((key) => add[key].state === "approved").map((key) => ({
        uid: key,
        ...add[key],
      }));

      const addonData = {};
      toArray.forEach((addon) => {
        addonData[addon.uid] = addon;
      });

      if (toArray.length > 0) {
        setAddonData(addonData);
      } else {
        setAddonData({});
      }
    };
    addon();
  }, []);


  const handleEdit = (uid) => {
    addonUtils.edit(uid,goToAdminEdit);
  }

  const deleteAddon = async (uid) => {
    await db.remove(`addons/${uid}`);
    console.log("Remove");
    setAddonData((prevAddonData) => {
      const updatedAddonData = { ...prevAddonData };
      delete updatedAddonData[uid];
      return updatedAddonData;
    });
  };

  const handleTheRightButton = async (uid,trueOrNo) => {
    try {
      await db.update(`addons/${uid}`, {
        Featured: !trueOrNo, // Toggle the Featured property
      });

      // Update the component state to reflect the new Featured state
      setAddonData((prevAddonData) => ({
        ...prevAddonData,
        [uid]: {
          ...prevAddonData[uid],
          Featured: !trueOrNo,
        },
      }));
    } catch (error) {
      console.error("Error toggling addon Featured state:", error);
    }

  };


  return (
    <>
      <Container>
        {Object.values(addonData).map((addon) => (
          <div key={addon.uid}>
            <Item>
            <FeaturedButton
                onClick={() =>handleTheRightButton(addon.uid, addon.Featured)}
                featured={addon.Featured}
              >
                <StarIcon />
              </FeaturedButton>
              <ItemImage src={addon.addonImg} alt={addon.name} />
              <Typography sx={{ fontSize: "16px", textAlign: "center", margin: "5px", fontWeight: "600" }}>{addon.name}</Typography>
              <Typography sx={{ fontSize: "13px", textAlign: "left", margin: "5px", color: "#767676" }}>{addon.IDE}</Typography>
              <Typography sx={{ fontSize: "13px", textAlign: "left", margin: "5px", color: "#767676" }}>STATE: {addon.state}</Typography>
              <Typography sx={{ fontSize: "12px", color: "#767676", display: "flex", alignItems: "center", justifyContent: "space-between", margin: "5px" }}>
                <span>{addon.creator}</span>
                <span style={{ display: "flex", alignItems: "center" }}>
                  <DownloadDoneIcon />
                  50
                </span>
              </Typography>
              <Typography sx={{ fontSize: "0.5em", textAlign: "left", margin: "5px" }}>Ranking:</Typography>
              <div style={{ display: "flex", alignItems: "center" }} >
                <Button variant="contained" sx={{ margin: "10px", width: "100px" }} onClick={() => handleEdit(addon.uid)}>Edit</Button>
                <span>&nbsp;</span>
                <Button variant="contained" sx={{ margin: "10px", width: "100px" }} onClick={() => deleteAddon(addon.uid)}>Remove</Button>
              </div>
            </Item>
          </div>
        ))}
      </Container>
    </>
  );
}

// import { useEffect, useState } from "react";
// import { db } from '../../services/database-services';
// import { Typography, Button, styled,} from '@mui/material';
// import DownloadDoneIcon from '@mui/icons-material/DownloadDone';
// import { approveAddon } from './../../services/addon-services';

// const Container = styled('div')({
//   display: "grid",
//   gridTemplateColumns: "repeat(auto-fill, minmax(calc(15% - 15px), 1fr))",
//   gap: "23px",
//   width: "80%",
//   margin: "50px auto",
// });

// const Item = styled('div')({
//   display: "flex",
//   flexDirection: "column",
//   justifyContent: "center",
//   border: "1px solid #ccc",
//   borderRadius: "8px",
//   height: "350px",
//   width: "100%",
//   backgroundColor: "white",
//   color: "black",
//   margin: "0 10px",
//   transition: "box-shadow 0.3s ease, border-width 0.3s ease",
//   boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
//   borderWidth: "1px",
//   "&:hover": {
//     boxShadow: "0px 10px 14px rgba(0, 0, 0, 0.5)",
//     borderWidth: "5px"
//   },
// });

// const ItemImage = styled('img')({
//   width: "100px",
//   height: "100px",
//   objectFit: "cover",
//   margin: "auto",
//   padding: "15px",
//   display: "block",
// });
// export default function Addon() {
//   const [addonData, setAddonData] = useState({});

//   useEffect(() => {
//     const addon = async () => {
//       const add = await db.get("addons");
      
//       const toArray = Object.keys(add).filter((key)=>add[key].state==="approved").map((key) => ({
//         uid: key,
//         ...add[key],
//       }));
      
//       const addonData = {};
//       toArray.forEach((addon) => {
//         addonData[addon.uid] = addon;
//       });
      
//       if (toArray.length > 0) {
//         setAddonData(addonData);
//       } else {
//         setAddonData({});
//       }
//     };
//     addon();
//   }, []);
//   const dAddon = async (uid) => {
//     await db.remove(`addons/${uid}`)
//     console.log("Remove")
//     setAddonData((prevAddonData) => {
//       const updatedAddonData = { ...prevAddonData };
//       delete updatedAddonData[uid];
//       return updatedAddonData;
//     });
// };

//   return (
//     <>
//      <Container>
//         {Object.values(addonData).map((addon) => (
//           <div key={addon.uid}>
//             <Item>
//               <ItemImage src={addon.addonImg} alt={addon.name} />
//               <Typography sx={{ fontSize: "16px", textAlign: "center", margin: "5px", fontWeight: "600" }}>{addon.name}</Typography>
//               <Typography sx={{ fontSize: "13px", textAlign: "left", margin: "5px", color: "#767676" }}>{addon.IDE}</Typography>
//               <Typography sx={{ fontSize: "13px", textAlign: "left", margin: "5px", color: "#767676" }}>STATE: {addon.state}</Typography>
//               <Typography sx={{ fontSize: "12px", color: "#767676", display: "flex", alignItems: "center", justifyContent: "space-between", margin: "5px" }}>
//                 <span>{addon.creator}</span>
//                 <span style={{ display: "flex", alignItems: "center" }}>
//                   <DownloadDoneIcon />
//                   50
//                 </span>
//               </Typography>
//               <Typography sx={{ fontSize: "0.5em", textAlign: "left", margin: "5px" }}>Ranking:</Typography>
//               <div style={{ display: "flex", alignItems: "center" }} >
//               <Button variant="contained" sx={{ margin: "10px", width: "100px" }}>Edit</Button>
//                 <span>&nbsp;</span>
//                 <Button variant="contained" sx={{ margin: "10px", width: "100px" }}onClick={() => dAddon(addon.uid)} >Remove</Button>
//               </div>
//             </Item>
//           </div>
//         ))}
//       </Container>

//     </>
//   );
// }