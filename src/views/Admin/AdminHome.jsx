import { useEffect, useState } from "react";
import { getAllUsers } from "../../services/users-services";
import Members from "../Admin/Member";
import { Box, Typography } from "@mui/material";
import { BsPeopleFill,BsFillGrid3X3GapFill } from 'react-icons/bs'
import {PATHS} from "./../../common/constants";
import { Link } from "react-router-dom";
import MdPendingActions from "@mui/icons-material/PendingActions";
import { db } from '../../services/database-services';
import { Stack, styled } from "@mui/material";
import { UseUserContext } from "../../context/UserContext";


const Way = styled(Link)({
  textDecoration: "none",
  color: "inherit",
});

const  MyButton  = styled("Button")({
  margin: "10px",
  width: "100px",
    display: "block",
    height: "38px",
    border: "none",
    cursor: "pointer",
    borderRadius: "30px",
    backgroundColor: 'transparent',
    color:"#6a6567",
    fontSize: "16px",
    marginTop: "8px",

    marginBottom: "16px",
    transition: "background-color 2s, color 1s",
    "&:hover": {
      color: "#2c5364",
      backgroundColor: "white",
    },
    "&:active": {
      color: "#2c5364",
      backgroundColor: "white",
    },
  
});


export default function AdminHome() {
  const { ALL_USERS, ALL_ADDONS, PENDING_ADDONS } = PATHS;


  const { role } = UseUserContext();

  if (role !== 'admin') {
    return (
      <Box>
        <h1>Only admins can access this page</h1>
      </Box>
    );
  }

  const [memberCount, setMemberCount] = useState(0);
  const [pending, setPending] = useState(0);
  const [addon, setAddon] = useState(0);

  useEffect(() => {
    getAllUsers().then((data) => {
      const dataCount = Object.keys(data).length;
      setMemberCount(dataCount);
    });
  }, []);

  useEffect(() => {
    const fetchAddonData = async () => {
      const add = await db.get("addons");
      setAddon(Object.keys(add).length)
      const toArray = Object.keys(add).filter((key) => add[key].state === "pending")
      setPending(toArray.length)
    };
    fetchAddonData();
  }, []);

  return (
<div style={{  marginBottom: "38%" }} >
  <div style={{ display: "flex", justifyContent: "flex-end", marginRight: "16px" }}>
  <Stack direction="row" spacing={1} sx={{ backgroundColor: 'transparent', padding: "16px", borderRadius: "8px", width: "230px",marginRight: "860px",  marginTop: "16px",}}>
    <Way to={ALL_USERS}>
      <MyButton>
        Member
      </MyButton>
    </Way>
    <Way to={PENDING_ADDONS}>
      <MyButton>
        Pending
      </MyButton>
    </Way>
    <Way to={ALL_ADDONS}>
      <MyButton>
        Addons
      </MyButton>
    </Way>
  </Stack>
    <Typography sx={{
      marginLeft: "16px", // Add left margin for spacing
      marginTop: "16px",
      borderRadius: "8px",
      height: "76px",
      width: "139px",
  background: 'linear-gradient(90deg, rgba(176,160,152,1) 16%, rgba(106,101,103,1) 100%, rgba(206,201,200,1) 100%)',
  color:"#cec9c8",
      transition: "box-shadow 0.3s ease, border-width 0.3s ease",
      boxShadow: "0 1rem 1.8rem rgba(0, 0, 0, 0.8)",
      borderWidth: "1px",
      textAlign: 'center',         
      alignItems: 'center', // Center horizontally
    
    }}>
      <Box sx={{ fontSize: "38px", textAlign: 'center',  justifyContent:"center" }}>
        <BsPeopleFill /> {memberCount}
      </Box>
    </Typography>

    <Typography sx={{
      marginLeft: "16px", // Add left margin for spacing
      marginTop: "16px",
      borderRadius: "8px",
      height: "76px",
      width: "139px",
      background: 'linear-gradient(90deg, rgba(176,160,152,1) 16%, rgba(106,101,103,1) 100%, rgba(206,201,200,1) 100%)',
      color:"#cec9c8",
      transition: "box-shadow 0.3s ease, border-width 0.3s ease",
      boxShadow: "0 1rem 1.8rem rgba(0, 0, 0, 0.8)",
      borderWidth: "1px",
      justifyContent: "center",
      justifyItems: "end"
    }}>
      <Box sx={{ fontSize: "38px", textAlign: 'center' }}>
        <MdPendingActions sx={{ fontSize: "38px" }} /> {pending}
      </Box>
    </Typography>

    <Typography sx={{
      marginLeft: "16px", // Add left margin for spacing
      marginTop: "16px",
      borderRadius: "8px",
      height: "76px",
      width: "138px",
      background: 'linear-gradient(90deg, rgba(176,160,152,1) 16%, rgba(106,101,103,1) 100%, rgba(206,201,200,1) 100%)',
      color:"#cec9c8",
      transition: "box-shadow 0.3s ease, border-width 0.3s ease",
      boxShadow: "0 1rem 1.8rem rgba(0, 0, 0, 0.8)",
      borderWidth: "1px",
      justifyContent: "center",
      justifyItems: "end"
    }}>
      <Box sx={{ fontSize: "38px", textAlign: 'center' ,alignItems:'center'}}>
        <BsFillGrid3X3GapFill />
        {addon}
      </Box>
    </Typography>
  </div>

  <div style={{ display: "flex", justifyContent: "center", marginTop: "36px" }}>
    <Members />
  </div>
</div>

  );
}







