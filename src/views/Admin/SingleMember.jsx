import { useNavigate } from "react-router-dom";
import { db } from "../../services/database-services";
import { Avatar, Box, Button, Typography,} from "@mui/material";
import BlockIcon from "@mui/icons-material/Block";
import UnblockIcon from "@mui/icons-material/CheckCircle";

export default function SingleMember  ({ uid, firstName, lastName, email, isBlock = false, setBlocked,imageURL, role }) {

  console.log(`this is what I want to see` ,{uid, firstName, lastName, email, role, isBlock, setBlocked, imageURL})
  const navigate = useNavigate();

  const blockUser = async () => {
    let newRole = '';
    console.log(role);    
    if(role === 'user') {
      newRole = 'blocked';
    }

    if (role === 'blocked') {
      newRole = 'user';
    }

    if (role === 'admin') {
      alert('You cannot block an admin');
    }

    try {
      await db.update(`users/${uid}`, {
        isBlock: !isBlock,
        role: newRole,
      });
      setBlocked((blocked) => [...blocked, uid]);
    } catch (error) {
      console.error("Error blocking/unblocking user:", error);
    }
  };

  const handleNavigate = () => {
    navigate(`/member/${uid}`);
  };

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        borderRadius: "16px",
        marginInline: "auto",
        width: "100%",
        padding: "0.5rem 1rem",
        border: 1,
        backgroundColor: "rgba(255, 255, 255, 0.1)",
        transition: "box-shadow 0.3s ease, border-width 0.3s ease",
        boxShadow: "0 1rem 1.8rem  rgba(0, 0, 0, 0.8)", 
        borderWidth: "1px",
        "&:hover": {
          boxShadow: "0 1rem 1.8rem  rgba(164,128,102,1),  -0 -1rem 1.8rem  rgba(164,128,102,1)",
          borderWidth: "6px"
        },
      }}
    >
      <Box sx={{ display: "flex", gap: 2 }} onClick={handleNavigate}>
      <Avatar src={imageURL} />
        <Box>
          <Typography variant="subtitle1">
            {firstName} {lastName}
          </Typography>
          <Typography variant="body2"></Typography>
        </Box>
      </Box>
      <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
        <Typography variant="body2">{email}</Typography>
        <Button sx={{ color:"#3f4c6b"}}
          onClick={blockUser}
          startIcon={isBlock ? <UnblockIcon sx={{ color:"#3f4c6b"}}/> : <BlockIcon sx={{ color:"#3f4c6b"}} />}>
          {isBlock ? "Unblock" : "Block"}
        </Button>
      </Box>
    </Box>
  );
}

