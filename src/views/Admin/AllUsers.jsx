import { useEffect, useState } from "react";
import { Box,Stack,} from "@mui/material";
import { UseUserContext } from "../../context/UserContext";
import { getAllMember } from "../../services/users-services";
import SingleMember from "./SingleMember";

export default function AllUsers() {

  const { role } = UseUserContext();
  const [members, setMembers] = useState([]);
  const [blocked, setBlocked] = useState([]);


  useEffect(() => {
    getAllMember().then((userArray) => setMembers(userArray)).catch(console.error);
  }, [blocked]);

  if (role !== 'admin') {
    return (
      <Box>
        <h1>Only admins can access this page</h1>
      </Box>
    );
  }

  const membersToDisplay = members.map((member) => {
    console.log(member.role);
    return (
      <SingleMember
        key={member.uid}
        uid={member.uid}
        firstName={member.firstName}
        lastName={member.lastName}
        email={member.email}
        role={member.role}
        isBlock={member.isBlock}
        setBlocked={setBlocked}
        imageURL={member.imageUrl}
      />
    );
  });

  return (

    <Box width="50%" margin="auto" marginBottom= "27%">
      <Stack marginTop={3} spacing={3}>
        {membersToDisplay}
      </Stack>
    </Box>
  );
}



