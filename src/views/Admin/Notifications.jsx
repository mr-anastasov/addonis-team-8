import { useEffect, useState } from "react";
import { Typography, Avatar, Box } from "@mui/material";
import { UseUserContext } from "../../context/UserContext";
import { onValue, ref } from "firebase/database";
import { database } from "../../config/firebase-config";

export default function Not() {
  const { userData } = UseUserContext();
  const [mailbox, setMailbox] = useState([]);

  useEffect(() => {
    const getPathToMailbox = async () => {
      try {
        const mailboxRef = ref(database, `users/${userData.userID}/mailbox`);
        const handleMailboxChange = (snapshot) => {
          const mailboxData = snapshot.val();

          if (mailboxData) {
            // Convert mailboxData object to an array of entries

            const mailboxEntries = Object.values(mailboxData);

            setMailbox(mailboxEntries);
          } else {
            console.log("Mailbox data is null or undefined.");
          }
        };

        // Listen for real-time updates to mailbox

        const mailboxListener = onValue(mailboxRef, handleMailboxChange);

        return () => {
          // Unsubscribe from mailbox updates when the component unmounts

          mailboxRef.off("value", mailboxListener);
        };
      } catch (error) {
        console.error("Error fetching mailbox data:", error);
      }
    };

    getPathToMailbox();
  }, [userData.userID]);

  return (
    <div style={{ marginBottom: "27%"}}>
      {mailbox.map((addon, index) => (
        <Box
          key={index}
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            borderRadius: "16px",
            margin: "10px",
            padding: "0.5rem 1rem",
            width:"40%",
            border: 1,
            backgroundColor: "rgba(255, 255, 255, 0.1)",
            transition: "box-shadow 0.3s ease, border-width 0.3s ease",
            boxShadow: "0 1rem 1.8rem rgba(0, 0, 0, 0.8)",
            borderWidth: "1px",
            "&:hover": {
              boxShadow: "0 1rem 1.8rem #55a49e,  0 -1rem 1.8rem #55a49e",
              borderWidth: "6px",
            },
          }}
        >
          <Box sx={{ display: "flex", gap: 2 }}>
            <Avatar src={addon.addonKey} />
            <Box>
              <Typography variant="subtitle1">{addon.action}</Typography>
              <Typography variant="body2"> {addon.addonName}</Typography>
              <Typography variant="body2">by {addon.approvedBy}</Typography>
              <Typography variant="body2">{addon.addonData}</Typography>
            </Box>
          </Box>
        </Box>
      ))}
    </div>
  );
}
