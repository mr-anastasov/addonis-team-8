import {Box, Grid, Typography} from "@mui/material";

export default function NotFound() {
  return (
    <>
      <Box>
        <Grid>
          <Typography variant="h3">404</Typography>
          <Typography variant="h5">Page not found</Typography>
        </Grid>
      </Box>
    </>
  )
}