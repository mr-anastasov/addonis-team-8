import { Box, Divider, Typography, FormControlLabel, Checkbox } from "@mui/material";

export default function TermsOfService() {

  const date = new Date();
  const effectiveDate = date; // Replace with the actual effective date
  const privacyPolicyLink = "[Insert Privacy Policy Link]"; // Replace with the actual link
  const contactInformation = "[Insert Contact Information]"; // Replace with the actual contact information

  return (
    <Box sx={{ display: "flex", flexDirection: "column", minHeight: "calc(100vh - 64px - 100px)" }}>
    <Box sx={{ flex: 1, m: "20px", p: "20px"}}>
      <Typography variant="h3">
        Addonis Addons Registry Terms of Service
        Effective Date: {effectiveDate}
      </Typography>
      <Typography variant="body1">
          Please read these terms of service ("Terms") carefully before using the Addonis Addons Registry web application ("Addonis" or the "Service").

          By accessing or using the Addonis web application, you agree to be bound by these Terms. If you do not agree to these Terms, please do not use the Service.
      </Typography>
    </Box>
    <Divider />
    <Box sx={{ flex: 1, m: "20px", p: "20px" }}>
      <Typography variant="h5">
        1. Acceptance of Terms
      </Typography>
      <Typography variant="body2">
        By accessing or using the Addonis web application, you agree to be bound by these Terms. If you do not agree to these Terms, please do not use the Service.
      </Typography>
      <Typography variant="h5">
        2. User Registration
      </Typography>
      <Typography variant="body2">
        Eligibility: To use the Service, you must be at least 18 years old or the legal age of majority in your jurisdiction.

        Account Information: You agree to provide accurate and complete information when registering for an Addonis account. You are responsible for maintaining the confidentiality of your account credentials.
      </Typography>
      <Typography variant="h5">
        3. User Conduct
      </Typography>
      <Typography variant="body2">
        You agree to:

        Use the Service in compliance with all applicable laws and regulations.

        Refrain from engaging in any activity that may harm, disrupt, or abuse the Service or its users.

        Respect the intellectual property rights of others, including but not limited to, copyrights and trademarks.

        Refrain from uploading or sharing content that is offensive, unlawful, or violates the rights of others.
      </Typography>
      <Typography variant="h5">
        4. Privacy Policy
      </Typography>
      <Typography variant="body2">
        Your use of the Service is also governed by our Privacy Policy, which outlines how we collect, use, and disclose your personal information. Please review our Privacy Policy <a href={privacyPolicyLink}>here</a> to understand our practices.
      </Typography>
      <Typography variant="h5">
        5. Content Ownership
      </Typography>
      <Typography variant="body2">
        User-Generated Content: You retain ownership of the content you submit to the Service. By submitting content, you grant Addonis a non-exclusive, worldwide, royalty-free license to use, reproduce, modify, and distribute the content for the purpose of providing the Service.

        Addon Ownership: Addons uploaded to Addonis remain the intellectual property of their respective creators.
      </Typography>
      <Typography variant="h5">
        6. Termination
      </Typography>
      <Typography variant="body2">
        Addonis reserves the right to terminate or suspend your account and access to the Service for any violation of these Terms or for any reason at our discretion.
      </Typography>
      <Typography variant="h5">
        7. Disclaimer of Warranties
      </Typography>
      <Typography variant="body2">
        The Service is provided "as is" and "as available" without warranties of any kind, either express or implied. Addonis does not guarantee the accuracy, reliability, or availability of the Service.
      </Typography>
      <Typography variant="h5">
        8. Limitation of Liability
      </Typography>
      <Typography variant="body2">
        Addonis shall not be liable for any indirect, incidental, special, consequential, or punitive damages, or any loss of profits or revenues, whether incurred directly or indirectly.
      </Typography>
      <Typography variant="h5">
        9. Changes to Terms
      </Typography>
      <Typography variant="body2">
        Addonis reserves the right to modify or revise these Terms at any time. You will be notified of any material changes. Continued use of the Service after such changes constitutes your acceptance of the revised Terms.
      </Typography>
      <Typography variant="h5">
        10. Governing Law
      </Typography>
      <Typography variant="body2">
        These Terms shall be governed by and construed in accordance with the laws of [Your Jurisdiction].
      </Typography>
      <Typography variant="h5">
        11. Contact Information
      </Typography>
      <Typography variant="body2">
        If you have any questions about these Terms, please contact us at {contactInformation}.
        By using Addonis, you agree to these Terms of Service.
      </Typography>
       <FormControlLabel required control={<Checkbox />} label={"I confirm"} sx={{ p: "10px" }}/>
    </Box>
    </Box>
)
}