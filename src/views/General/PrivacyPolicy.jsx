import { Box, Divider, Typography, FormControlLabel, Checkbox } from "@mui/material";

export default function Privacy() {

  const date = new Date();

  return (
      <Box sx={{ display: "flex", flexDirection: "column", minHeight: "calc(100vh - 64px - 100px)" }}>
      <Box sx={{ flex: 1, m: "20px", p: "20px"}}>
        <Typography variant="h3">
          Privacy Policy
          Effective Date: {date.toLocaleDateString()}
        </Typography>
        <Typography variant="body1">
            Thank you for using our service: Infinity AI Market. This Privacy Policy explains how we collect, use, disclose, and safeguard your information when you use our [website/application] (the "Service").

            Please read this Privacy Policy carefully. If you do not agree with the terms of this Privacy Policy, please do not access the Service.
        </Typography>
      </Box>
      <Divider />
      <Box sx={{ flex: 1, m: "20px", p: "20px" }}>
        <Typography variant="h5">
          1. Information We Collect
        </Typography>
        <Typography variant="body2">
          We collect information that you provide directly to us when using the Service. This includes:

          User Information: When you sign up for an account, we may collect personal information such as your name, email address, and profile picture.

          Addon Information: When you upload an addon, we may collect information related to the addon, including its description, category, and associated files.

          Usage Data: We automatically collect certain information when you access and use the Service. This may include your IP address, browser type, operating system, referral URLs, and other usage information.
        </Typography>
        <Typography variant="h5">
          2. How We Use Your Information
        </Typography>
        <Typography variant="body2">
          We may use the information we collect for various purposes, including:
          Providing, maintaining, and improving the Service.
          Managing your account and providing customer support.
          Personalizing your experience and delivering tailored content.
          Sending you technical notices, updates, security alerts, and administrative messages.
          Monitoring and analyzing usage patterns and trends.
          Enforcing our Terms of Service and other policies.
        </Typography>
        <Typography variant="h5">
          3. How We Share Your Information
        </Typography>
        <Typography variant="body2">
          We may share your information in the following circumstances:
          With Third-Party Service Providers: We may share your information with third-party service providers who help us in operating the Service, such as hosting, analytics, and payment processing.
          For Legal Reasons: We may disclose your information if required to do so by law or in the good faith belief that such action is necessary to comply with legal obligations or protect our rights, users, or the public.
        </Typography>
        <Typography variant="h5">
          4. Data Security
        </Typography>
        <Typography variant="body2">
          We take reasonable measures to protect your information from unauthorized access, alteration, disclosure, or destruction. However, no method of transmission over the internet or electronic storage is 100% secure, and we cannot guarantee absolute security.
        </Typography>
        <Typography variant="h5">
          5. Links to Other Websites
        </Typography>
        <Typography variant="body2">
          The Service may contain links to third-party websites or services that are not owned or controlled by us. We are not responsible for the content, privacy policies, or practices of any third-party websites.
        </Typography>
        <Typography variant="h5">
          6. Children's Privacy
        </Typography>
        <Typography variant="body2">
          The Service is not intended for users under the age of [13/16/18], and we do not knowingly collect personal information from minors. If you are a parent or guardian and believe that your child has provided us with personal information, please contact us.
        </Typography>
        <Typography variant="h5">
          7. Changes to This Privacy Policy        
        </Typography>
        <Typography variant="body2">
          We may update our Privacy Policy from time to time. You are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.\
        </Typography>
        <Typography variant="h5">
          8. Contact Us
        </Typography>
        <Typography variant="body2">
          If you have any questions or concerns about our Privacy Policy, please contact us at [contact email].
          By using the Service, you agree to the terms of this Privacy Policy.
        </Typography>
         <FormControlLabel required control={<Checkbox />} label={"I confirm"}sx={{p:"10px"}}/>
      </Box>
      </Box>
  )
}