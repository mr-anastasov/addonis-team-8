import { Box, Typography, Divider } from "@mui/material"

export default function About() {

    const date = new Date();

    return (
        <Box sx={{ display: "flex", flexDirection: "column", minHeight: "calc(100vh - 64px - 100px)" }}>
      <Box sx={{ flex: 1, m: "20px", p: "20px"}}>
        <Typography variant="h3">
        Welcome to Addonis - Your Ultimate Addons Registry!
        </Typography>
        <Typography variant="h4">
          Effective Date: {date.toLocaleDateString()}<br /><br />
        </Typography>
        <Typography variant="h4">
        At Addonis, we've created a seamless web application dedicated to enhancing your development experience with Addon AI.

        Our platform serves as a central hub for all your Addon AI needs, offering an array of features tailored to empower both developers and users. Whether you're seeking to amplify your IDE's capabilities or contribute your own innovations, Addonis is here to streamline your journey.
        </Typography>
      </Box>
      <Divider />
      <Box sx={{ flex: 1, m: "20px", p: "20px" }}>
        <Typography variant="h5">
          1. Unleash Your Creativity: Publish Your Addons
        </Typography>
        <Typography variant="body2">
        Addonis provides a space for developers like you to showcase your ingenuity. 

        Easily publish your own addons and share them with the global community.

        Whether it's a time-saving utility, a game-changing feature, or an integration that bridges the gap, Addonis is your platform to shine.<br /><br />
        </Typography>
        <Typography variant="h5">
          2. Discover, Customize, Elevate: Browse Addons for Your Preferred IDE
        </Typography>
        <Typography variant="body2">
        Navigate through a diverse collection of addons designed to augment your preferred Integrated Development Environment (IDE).
        Our intuitive search and filtering options ensure you find the perfect addon to fit your workflow. Personalize your development environment, optimize your processes, and watch your productivity soar.<br /><br />
        </Typography>
        <Typography variant="h5">
          3. Seamless Access: Download Addons with Ease
        </Typography>
        <Typography variant="body2">
        With Addonis, obtaining the tools you need is a breeze. Downloading addons is a simple, hassle-free process, empowering you to integrate new functionalities effortlessly. <br />
        We ensure that accessing cutting-edge addons aligns perfectly with your development journey.<br /><br />
        </Typography>
        <Typography variant="h5">
          4. Empower with Your Voice: Rate Existing Addons
        </Typography>
        <Typography variant="body2">
        Your opinion matters! Contribute to our community-driven ecosystem by rating and reviewing existing addons.
        Share your experiences, insights, and suggestions to help others make informed decisions. 
        Your feedback shapes the direction of Addon AI and fosters a collaborative atmosphere.<br /><br />
        </Typography>
        <Typography variant="h5">
        Join us at Addonis and experience the future of addon innovation. Inspired by platforms like JetBrains Registry, Visual Studio Marketplace, and Eclipse Marketplace, we're committed to simplifying the way you discover and utilize addons. 
        Our clean and user-friendly interface ensures you can focus on what truly matters: enhancing your development environment.<br /><br />

        Thank you for being a part of the Addonis community. Together, we're propelling the world of AI-powered addons forward, one innovation at a time.
        </Typography>
      </Box>
      </Box>
    )
}