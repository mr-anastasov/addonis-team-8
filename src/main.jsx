import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import { BrowserRouter } from  'react-router-dom';
import { ThemeProviderCustom } from './context/ThemeContext.jsx';
import { AppDataProvider } from './context/AppContext.jsx';
import { UserContextProvider } from './context/UserContext.jsx';


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <AppDataProvider>
      <BrowserRouter>
        <ThemeProviderCustom>
            <UserContextProvider>
              <App />
            </UserContextProvider>          
        </ThemeProviderCustom>
      </BrowserRouter>
    </AppDataProvider>
  </React.StrictMode>
)
