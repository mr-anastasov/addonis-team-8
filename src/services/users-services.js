import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from "firebase/auth";
import { auth, database, storage } from "./../config/firebase-config.js";
import { db} from "./database-services.js";
import { equalTo, get, orderByKey, query, ref ,push, set} from "firebase/database";

/**
 * Handles user registration.
 * uses the firebase auth function createUserWithEmailAndPassword
 * then adds the uid to the form data
 * calls the createUser function to create the user in the database with the form data
 * @async
 * @function handleRegister
 * @param {Object} form - The registration form data.
 * @returns {void}
 */
export const handleRegister = async (form) => {

  if (form === null || form === undefined) {
    console.log("Form is empty");
    return;
  }
  return createUserWithEmailAndPassword(auth, form.email, form.password)
    .then((userCredential) => {
      const uid = userCredential.user.uid;
      form.role = "user";
      form.userID = uid;
      delete form.password;
      // form.password = null;
      form.addons = ["empty"];
      createUser(form);
      console.log(`User created with uid: ${uid}`); 
    })
    .catch((error) =>
      console.log(
        `While registration\nThis error occurred: ${error}\nThis is the message: ${error.message}`
      )
    );

};

/**
 * Creates a user in the database.
 * @async
 * @function createUser
 * @param {Object} form - The user data to create.
 * @returns {void}
 */
const createUser = async (form) => {
  db.set(`users/${form.userID}`, form)
    .then(() => console.log(`User created`))
    .catch((e) => console.log(`Error occurred while creating user:\n ${e}`));

  pushPhone(form.phoneNumber);

  for (const key in form) {
    sessionStorage.setItem(key, form[key]);
  }
};

/**
 * Handles user login.
 * uses the firebase auth function signInWithEmailAndPassword
 * then gets the user data from the database
 * checks if the user is blocked
 * sets the user data in the session storage
 * returns the user data
 * @async
 * @function handleLogin
 * @param {string} email - The user's email.
 * @param {string} password - The user's password.
 * @returns {Object|null} The user data or null if login fails.
 */
export const handleLogin = async (email, password) => {
  
  return signInWithEmailAndPassword(auth, email, password)
    .then(async (userCredential) => {
      const signUser = userCredential.user.uid;
      const userRef = ref(database, `users/${signUser}`);
      const snapshot = await get(userRef);

      if (!snapshot.exists()) {
        console.log("No data available");
        return null;
      }
      const data = snapshot.val();
      if (data.role === "blocked") {
        console.log("User is blocked");
        return null;
      }

      for (const key in data) {
        sessionStorage.setItem(key, data[key]);
      }
      return data;
    })
    .catch((error) =>
      console.log(
        `While login:\nThis error occurred: ${error}\nWith this error message: ${error.message}`
      )
    );
};

export const pushPhone = async (phoneNumber) => {
  const phones = await db.get(`phonebook/`);
  phones.push(phoneNumber);
  return await db.set(`phonebook/`, phones);
};

export const phoneExists = async (phoneNumber) => {
  const phones = await db.get(`phonebook/`);
  if (phones.includes(phoneNumber)) {
    return true;
  }
  return false;
};

// /**
//  * Handles user logout. Depricated
//  * @async
//  * @function handleLogout
//  * @returns {Promise<void>} A promise that resolves after logout.
//  */
// export const handleLogout = async () => {
//   sessionStorage.clear();
//   return auth.signOut();

// }

/**
 * Retrieves all users from the database.
 * @async
 * @function getAllUsers
 * @returns {Array} An array of user objects.
 */
export const getAllUsers = async () => {
  const users = await db.get(`users/`);
  return users;
};
export const getAllMember = async () => {
  const snapshot = await get(ref(database, "users"));

  if (!snapshot.exists()) {
    return [];
  }
  const data = snapshot.val();
  return Object.keys(data).map((key) => ({
    uid: key,
    ...data[key],
  }));
};
// export const getUserById = async (uid) => {
//   const snapshot = await get(
//     query(ref(database, `users/${uid}`))
//   );
//   const value = snapshot.val();
//   return value

// };

export const getUserById = async (uid) => {
  const snapshot = await get(query(ref(database, `users/${uid}`)));
  const value = snapshot.val();
  return value;
};

export const getUserData = async (uid) => {
  const userRef = ref(db, `users/${uid}`);

  const snapshot = await get(userRef);

  const userData = snapshot.val();

  if (userData) {
    userData.uid = uid;
  }

  return userData;
};

export const not = async (message, author, addon, userUid) => {
  try {
    // Construct the mailbox object
    const box = {
      userUid: userUid,
      addonKey: addon.addonImg,
      addonNew: addon.state,
      addonName: addon.name,
      addonData: addon.created,
      approvedBy: author,
      action: message,
    };

    // Reference the mailbox path in Firebase
    const mailboxRef = ref(database, `users/${userUid}/mailbox`);

    // Push the new mailbox item (set with a unique key)
    const newMailboxItemRef = push(mailboxRef);
    console.log(`Message sent to ${addon?.creator} by ${author}`, newMailboxItemRef);
    await set(newMailboxItemRef, box);
  } catch (error) {
    console.log("Can't update mailbox", error.message);
  }
};



// export const not= async (notification,approvedBy,addonUid,userUid)=>{

// const refData=await db.get(`users/${userUid}/mailbox`);
// console.log("Check",refData);
//  try {

//    const properties=refData;

//   if (properties===null||properties===undefined) {

//     console.log("Can Nothing")
//   }
//   const box={
//     userUid: userUid,//u
//     addonKey:addonUid,
//     approvedBy:approvedBy,
//     action:notification
//   }

//   refData.push(box)
//  } catch (error) 
//  {
//   console.log("Cant updated mailbox",error.message)
//  }

// }
