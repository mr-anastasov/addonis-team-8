import { ref, push, get } from "firebase/database";
import { db } from "./database-services.js";
import { DOCS } from "../common/constants.js";

/**
 * Utility functions for managing addons in the Firebase Realtime Database.
 * @namespace addonUtils
 */
export const addonUtils = {

  /**
   * Creates a new addon in the database.
   * @param {Object} form - The addon data to be created.
   * @returns {Promise} A promise that resolves to a reference to the newly created addon.
   */
  async create(form) {
    console.log(`this is the form`, form);
    return db
      .pushByUID(`addons/`, form)
      .then(async (ref) => {
        console.log(`I have created the addon ${ref}`)
        console.log('Creator', form.creatorID);
        const arrayAddons = await db.get(`users/${form.creatorID}/addons`);
        arrayAddons.push(ref);
        db.set(`users/${form.creatorID}/addons`, arrayAddons);
        // for (const key in form) {
        //  sessionStorage.setItem(key, form[key]);
        // }
        return ref;
      })
      .catch((e) => console.log(`Error occurred while creating addon:\n ${e}`));
  },

  async update(uid,form) {
    console.log(`this is the form`, form);
    try {
      db.update(`addons/${uid}`, form);
      console.log("Addon updated successfully", form);
    } catch (error) {
      console.log('Error while updating addon', error.message);
    }
  },

  /**
   * Marks an addon as "approved" in the database.
   * @param {string} addonID - The unique identifier of the addon to approve.
   */
  async approve(addonID) {
    try {
      const path = `/${DOCS.ADDONS}/${addonID}/state/`;
      const state = "approved";
      db.set(path, state);
      alert("Addon approved successfully");
    } catch (error) {
      console.log(error.message);
    }
  },

  /**
   * Marks an addon as "deleted" in the database.
   * @param {string} addonID - The unique identifier of the addon to delete.
   */
  async delete(addonID) {
    try {
      const path = `/${DOCS.ADDONS}/${addonID}/state/`;
      const state = "deleted";
      db.set(path, state);
      alert("Addon deleted successfully");
    } catch (error) {
      console.log(error.message);
    }
  },

  /**
   * Retrieves the data of a specific addon from the database.
   * @param {string} addonID - The unique identifier of the addon to fetch.
   * @returns {Promise} A promise that resolves to the addon data.
   */
  async get(addonID) {
    try {
      const path = `/${DOCS.ADDONS}/${addonID}/`;
      const addon = await db.get(path);
      return addon;
    } catch (error) {
      console.log(`Error while trying to fetch the addon data ${error.message}`);
    }
  },

  /**
   * Allows a user to "rate" an addon.
   * @param {string} addonID - The unique identifier of the addon to like.
   * @param {string} userID - The unique identifier of the user liking the addon.
   */
  async rate(addonID, userID) {
    try {
      const userPath = `/${DOCS.USERS}/${userID}/liked/`;
      const liked = await db.get(userPath);
      if (liked.includes(addonID)) {
        alert("You have already liked this addon");
        return;
      }
      const addonPath = `/${DOCS.ADDONS}/${addonID}/rating/`;
      const rating = await db.get(addonPath);
      rating.count += 1;
      db.set(addonPath, rating);
      alert("Addon liked successfully");
    } catch (error) {
      console.log(error.message);
    }
  },

  /**
   * Allows custom editing actions for an addon.
   * @param {string} addonID - The unique identifier of the addon to edit.
   * @param {Function} func - You need to pass the goToEditAddon function from the helper.js file.
   */
  edit(addonID, func) {
    func(addonID);
  },

  /**
   * Initiates the download of a file from a given URL.
   * @param {string} url - The URL of the file to download.
   */
  download(url) {
    const link = document.createElement("a");
    link.download = url;
    link.href = url;
    alert("download started");
    link.click();
  },

  searchInObject(object, id) {
    for (const key in object) {
      if (key == id) {
        return object[key];
      }
    }
    return `Element not found`;
  },

  searchInArray(array, id) {
    for (const element of array) {
      if (element == id) {
        return element;
      }
    }
    return `Element not found`;
  }
};



export const createAddon = async (form) => {
  console.log(`this is the form`, form);
  return db
    .pushByUID(`addons/`, form)
    .then(async (ref) => {
      // console.log(`I have created the addon ${ref}`)
      // console.log('Creator', form.creatorID);
      const arrayAddons = await db.get(`users/${form.creatorID}/addons`);
      arrayAddons.push(ref);
      db.set(`users/${form.creatorID}/addons`, arrayAddons);
      // for (const key in form) {
      //  sessionStorage.setItem(key, form[key]);
      // }
      return ref;
    })
    .catch((e) => console.log(`Error occurred while creating addon:\n ${e}`));
};

export const deleteAddon = async (id) => {
  return db.update(`addons/${id}`, { state: "Deleted" });
};

export const approveAddon = async (id) => {
  return db.update(`addons/${id}`, { state: "Pending" });
};

export const getAllAddons = async () => {
  return await db.get(`addons/`);
};


// export const handleDownload = (downloadURL) => {
//   const link = document.createElement('a');
//   link.download = downloadURL;

//   link.href = downloadURL;

//   alert("download started")
//   link.click();

// }

// export const addAddon = (content, username) => {
//     return push(
//       ref(db, `addons/`),
//       {
//         author: username,
//         content,
//         createdOn: Date.now()
//       },
//     )
//       .then(result => {
//         return getAddonById(result.key);
//       });
//   };

// function del (id) {
//   deleteAddon(id)
//   return;
// }

// del('-NdYfh_EwaeL8MIIIxsF');

export const addAddonAlternative = async (addon) => {
  return await push(ref(db, `addons/`), addon);
};

export const getAddonById = async (id) => {
  return get(ref(db, `addons/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Addon with id ${id} does not exist!`);
    }

    const addon = result.val();
    addon.id = id;
    addon.createdOn = new Date(addon.createdOn);
    if (!addon.likedBy) addon.likedBy = [];

    return addon;
  });
};
