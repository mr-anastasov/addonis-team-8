import { useEffect } from "react";
import { auth } from "../config/firebase-config";
import { onAuthStateChanged } from "firebase/auth";
import  NavigationHelper  from "../common/helper";

export const AuthenticationVer = () => {

  const { goHome } = NavigationHelper();

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        logAuthenticationStatus(`User ${user.email} logged in`);
      } else {
        logAuthenticationStatus("User logged out");
        goHome();
      }
    });

    const logAuthenticationStatus = (message) => {
      console.log(message);
    };

    return () => unsubscribe();
  }, []);

  return null;
};