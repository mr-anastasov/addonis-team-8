import { storage } from "./../config/firebase-config";
import { deleteObject, getDownloadURL, ref, uploadBytes,} from "firebase/storage";


import { db } from "./database-services";

export const store = {

  async uploadAddonImage(file, uid) {
    if (file === null) return;
    try {
      const refImg = ref(storage, `AddonImage/${uid}`);
      return uploadBytes(refImg, file)
      .then(() => getDownloadURL(refImg));
    } catch (error) {
      console.log(`Error occurred while uploading image: ${error.message}`);
    }
  },

  async remove(uid, pathStorage) {
    try {
      const refImg = ref(storage, `${pathStorage}/${uid}`);
      await deleteObject(refImg);
      console.log(`Image have been removed successfully`);
    } catch (error) {
      console.log(`Error occurred while removing image: ${error.message}`);
    }
  },

  /**
   * Updates data at a specified path in the database.

   */
  async update(path, newData) {
    console.log(`Needs to be implemented: ${path} ${newData}`);
  },
};


export const uploadImg = (upload, uid) => {
  if (upload === null) return;

  const userImageRef = ref(storage, `AutImages/${uid}`);

  uploadBytes(userImageRef, upload)
    .then(() => {
      getDownloadURL(userImageRef)
        .then((downloadURL) => {
          console.log(uid);
          // Save the image URL to the database under the user's node
          db.set(`/users/${uid}/imageURL`, downloadURL);
          console.log(downloadURL)
          return downloadURL;
        })
        .catch((error) => {
          alert(error.message, "Error");
        });
    })
    .catch((error) => {
      alert(error.message);
    });
};






export const removeImage = (uid) => {
  // Remove the image from Firebase Storage
  const ref = ref(storage, `AutImages/${uid}`);
  deleteObject(ref)
    .then(() => {
      // Remove the image URL from Firebase Database
      db.set(`/users/${uid}/imageURL`, null);
    })
    .catch((error) => {
      console.error("Error removing image:", error);
    });
};
