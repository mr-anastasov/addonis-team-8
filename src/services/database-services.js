import { set, ref, get, push, remove, update,} from "firebase/database";
import { database } from "../config/firebase-config.js";



/**
 * Represents a set of database operations.
 * @namespace db
 */
export const db = {

  async set(path, data) {
    try {
      const reference = ref(database, path);
      await set(reference, data);
      console.log("Data set successfully.");
    } catch (error) {
      console.log(error.message);
    }
  },



  /**
   * Sets data at a specified path in the database.
   * @async
   * @function set
   * @param {string} path - The path to the database location.
   * @param {*} data - The data to be set.
   * @returns {void}
   */
  async setМultiple(path, ...data) {
    try {
      if (data.length > 0) {
        const reference = ref(database, path);
        await set(reference, data);
        console.log("Data set successfully.");
      } else {
        await set(ref(database, path), null);
        console.log("Document created successfully.");
      }

    } catch (error) {
      console.log(error.message);
    }
  },





  // Zlati този get е добър пример за overloading на функция, защото може да боти само с първия параметър, а може и с всички
  /**
   * Retrieves data from a specified path in the database.
   * @async
   * @function get
   * @param {string} path - The path to the database location.
   * @param {...string} constraints - Constraints for data retrieval (optional).
   * @returns {*} Retrieved data, or null if no data is available.
   */
  async get(path, ...constraints) {
    try {
      const reference = ref(database, path);
      let snapshot;

      if (constraints.length > 0) {
        snapshot = await get(reference, ...constraints);
      } else {
        snapshot = await get(reference);
      }

      if (snapshot.exists()) {
        return snapshot.val();
      } else {
        console.log("No data available.");
        return null;
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  /**
   * Pushes data to a specified path in the database.
   * @async
   * @function push
   * @param {string} path - The path to the database location.
   * @param {*} data - The data to be pushed.
   * @returns {string} The key of the pushed data.
   */
  async pushByUID(path, data) {
    try {
      const reference = ref(database, path);
      const key = (await push(reference, data)).key;

      console.log("Data pushed successfully.", key);
      return key;
    } catch (error) {
      console.log(error.message);
    }
  },

  async pushByValue(path, data) {
    try {
      const reference = ref(database, path);
      // Set the data at the generated key
      const phone = {[data]: data}
      await set(reference, phone);

      // const key = newChildRef.key;

      console.log("Data pushed successfully with key:", data);
      // return key;
    } catch (error) {
      console.log(error.message);
    }
  },

  /**
   * Removes data from a specified path in the database.
   * @async
   * @function remove
   * @param {string} path - The path to the database location.
   * @returns {void}
   */
  async remove(path) {
    try {
      const reference = ref(database, path);

      await remove(reference);
      console.log("Data removed successfully.");
    } catch (error) {
      console.log(error.message);
    }
  },

  /**
   * Updates data at a specified path in the database.
   * @async
   * @function update
   * @param {string} path - The path to the database location.
   * @param {*} newData - The new data to update with.
   * @returns {void}
   */
  async update(path, newData) {
    try {
      const reference = ref(database, path);
      await update(reference, newData);

      console.log("Data updated successfully.");
    } catch (error) {
      console.log(error.message);
    }
  },

  async link(path, uid, field, newData) {
    try {
      const reference = ref(database, `${path}/${uid}/${field}`);
      await update(reference, newData);
      console.log("Data linked successfully.");
    } catch (error) {
      console.log(error.message);
    }
  },


  async listener(path) {
    try {
        const ref = await ref(database, `${path}`)
        .then(snapshot =>  snapshot.val())
        .then(data => data);

        console.log('data',await ref);
      //   await ref.on("value", (snapshot) => {
      //   const data = snapshot.val();
      //   console.log('data',data);
      //   return data;
      // });
    } catch (error) {
      console.log(`Error occurred while fetching the main data need for the application to run:${error.message}\n
                   For further info check the error message.`);
    }
  }
};
