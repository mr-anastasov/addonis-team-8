// next stages when the app is deployed: https://docs.github.com/en/apps/creating-github-apps/registering-a-github-app/registering-a-github-app


import { Octokit } from "octokit";
// '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'

const token = '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';
// Create a personal access token at https://github.com/settings/tokens/new?scopes=repo
export const octokit = new Octokit({ auth: token });

// Compare: https://docs.github.com/en/rest/reference/users#get-the-authenticated-user

// const { data: { login, public_repos, url }} = await octokit.rest.users.getAuthenticated();
// const { data: {name , repos_url, public_repos} } = await octokit.rest.users.getAuthenticated();

export async function listUserRepositories(username) {
  try {
    const response = await octokit.rest.repos.listForUser({
      username: username,
    });

    return response.data;
  } catch (error) {
    console.error('Error listing user repositories:', error);
    throw error;
  }
}

const user = "addon-market";

const reduceReposInfo = async (user) => {
  return await listUserRepositories(user)
    .then(repositories => {
      return repositories.reduce((data, repo) => {
        let object = {};
        for (const key in repo) {
          if (Object.hasOwnProperty.call(repo, key)) {
            if(key === 'html_url' || key === 'name' ||
               key === 'description' || key === 'stargazers_count' ||
               key === 'forks_count' || key === 'language') {
              object[key] = repo[key];
            }
          }
        }
        data.push(object);
        return data
      }, []);
    })
    .catch(error => {
      console.error('Error:', error);
    });
}

export async function fetchRepoFileContent(owner, repo, filePath) {
  try {
    const response = await octokit.request(`GET /repos/${owner}/${repo}/contents/${filePath}`, {
      owner: owner,
      repo: repo,
      path: filePath
    });
    const url = await response.data.download_url

    return url;
  } catch (error) {
    console.error('Error fetching repository file link:', error.message);
    throw error;
  }
}

export async function fetchRepoFileContentNested(owner, repo, filePath) {
  try {
    const response = await octokit.request(`GET /repos/${owner}/${repo}/contents/${filePath}`, {
      owner: owner,
      repo: repo,
      path: filePath
    });
    const data = response.data.content;
    const url = response.data.download_url
;
    // Convert base64 content to a readable string
    const content = Buffer.from(data, 'base64').toString('utf-8');

    return {url, content};
  } catch (error) {
    console.error('Error fetching repository file content:', error);
    throw error;
  }
}

export const getDownloadUrl = async (user, repo, file) => {
  return fetchRepoFileContent(user, repo , file,)
    .then(data =>  data.url)
    .catch(error => {console.error('Error:', error);});
}

/**
 * Creates a new GitHub repository using the Octokit library.
 * @param {string} username - The name of the repository to be created.
 * @param {string} description - The description of the repository to be created.
 * @returns {object} - The data of the created GitHub repository.
 * @throws {Error} - If an error occurs during the repository creation.
 */
export async function createGitHubRepository(username, description) {
  try {
    const response = await octokit.request('POST /user/repos', {
      name: username,
      description: description,
      private: false, // Adjust as needed
    });

    return response.data;
  } catch (error) {
    console.error('Error creating repository:', error);
    throw error;
  }
}


export async function checkFilePath (owner, repo) {
  const url = `https://api.github.com/repos/${owner}/${repo}`;
  try {
    const response = await fetch(url);
    return  response.status === 200;
  } catch (error) {
    console.error('Error fetching repository file content:', error);
    throw error;
  }
}


export async function checkNestedPath (owner, repo, path = '') {
  const url = `https://api.github.com/repos/${owner}/${repo}/contents/${path}`;
  try {
    const response = await fetch(url);
    return  response.status === 200;
  } catch (error) {
    console.error('Error fetching repository file content:', error);
    throw error;
  }
}

export async function getSHA(owner, repo, path, file = '') {
  try {
    const response = await octokit.request(`GET /repos/${owner}/${repo}/contents/${path}/${file}`);
    if (response.status === 200 && response.data[0].sha) {
      return response.data[0].sha;
    } else {
      throw new Error('Failed to fetch SHA value.');
    }
  } catch (error) {
    console.error('Error fetching SHA value:', error);
    throw error;
  }
}



export async function uploadRepoData(owner, repo, message, file) {

  const name = file.name;
  const fileContent = await readFileContent(file);
  const binContent   = arrayBufferToBase64(fileContent);

  try {
        const response = await octokit.request(`PUT /repos/${owner}/${repo}/contents/${name}`, 
        {
          owner: owner,
          repo: repo,
          path: name,
          message: message,
          content: binContent,
        }) 
      console.log('File uploaded successfully!:', response.status);
    } catch (error) {
      console.error('Error fetching repository file content:', error);
    }
}



function readFileContent(file) {
  return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = (event) => {
          resolve(event.target.result);
      };

      reader.onerror = (event) => {
          reject(event.error);
      };

      reader.readAsArrayBuffer(file);
  });
}

function arrayBufferToBase64(arrayBuffer) {
  const binary = new Uint8Array(arrayBuffer);
  const bytes = [];

  for (const byte of binary) {
      bytes.push(String.fromCharCode(byte));
  }

  return btoa(bytes.join(""));
}


// export async function uploadToGitHub(repoName, description, file) {
//   const reader = new FileReader();

//   reader.onload = async () => {
//     const arrayBuffer = reader.result;
//     const base64Content = btoa(new Uint8Array(arrayBuffer).reduce((data, byte) => data + String.fromCharCode(byte), ''));
//     const dataFile = Buffer.from(arrayBuffer);
//     try {
//       const response = await octokit.request('POST /user/repos', {
//         name: repoName,
//         description: description,
//       });

//       const { owner, name } = response.data;

//       await octokit.request('PUT /repos/{owner}/{repo}/contents/{path}', {
//         owner,
//         repo: name,
//         path: 'src/services/git-hub.js',
//         message: 'my commit message',
//         content: base64Content,
//       });

//       console.log('File uploaded successfully!');
//     } catch (error) {
//       console.error('Error uploading file:', error);
//       throw error;
//     }
//   };

//   reader.readAsArrayBuffer(file);
// }


export const getAddonInformation = async (name, owner, repository) => {
  const addons = await octokit.request(`GET /repos/${owner}/${repository}/contents/${name}`, {
      owner: owner,
      repo: repository
  });
  return addons.data.download_url;
}


export async function fetchRepoData(owner, repo) {
  try {
    const { data: repoData } = await octokit.rest.repos.get({
      owner: owner,
      repo: repo,
    });
    console.log("Repository:", repoData.name); 
    console.log("Description:", repoData.description);
    console.log("Creator:", repoData.owner.login);
    console.log("Tags:", repoData.topics);
    console.log("Number of Downloads:", repoData.subscribers_count);
    console.log("Rating:", repoData.stargazers_count);
    console.log("Download Link:", repoData.html_url); //from GitHub 
    console.log("Origin Link:", repoData.html_url);
    console.log("Open Issues Count:", repoData.open_issues_count); //from GitHub 
    console.log("Pull Requests Count:", repoData.open_issues_count); //from GitHub
    console.log("Last Commit Date:", repoData.pushed_at); //from GitHub
    console.log("Content:", repoData.contents_url[1]); //from GitHub
    console.log("Repo data:", repoData); //from GitHub
    return repoData.trees_url;
  } catch (error) {
    console.log("Error fetching repository data:", error);
  }
}


export const contentsResponse = async(owner, repo, path) => {
  return await octokit.rest.repos.getContent({
    owner: owner,
    repo: repo,
    path: path, // You can specify a specific path if needed
  });

} 




export async function fetchRepoInsights(owner, repo) {
  try {
    // Get the repository traffic insights
    const trafficResponse = await octokit.rest.repos.getViews({
      owner: owner,
      repo: repo,
    });

    console.log("Traffic Insights:", trafficResponse.data);

    // Get the repository community insights
    const communityResponse = await octokit.rest.repos.getCommunityProfileMetrics({
      owner: owner,
      repo: repo,
    });

    console.log("Community Insights:", communityResponse.data);

    // Get the repository code frequency insights
    const codeFrequencyResponse = await octokit.rest.repos.getCodeFrequencyStats({
      owner: owner,
      repo: repo,
    });

    console.log("Code Frequency Insights:", codeFrequencyResponse.data);

    // Add more insights endpoints as needed

  } catch (error) {
    console.log("Error fetching repository insights:", error);
  }
}

export async function getGitHubRepoInfoFromLink(link) {
  try {
    const parts = link.split('/').slice(3);

    const owner = parts[0];
    const repo = parts[1];

    const { data: repoData } = await octokit.rest.repos.get({ owner, repo });
    const { data: commits } = await octokit.rest.repos.listCommits({ owner, repo });
    console.log(commits);
    const { data: pullRequests } = await octokit.rest.pulls.list({ owner, repo });
    const { data: issues } = await octokit.rest.issues.list({ owner, repo, state: 'open' });

    const lastCommit = commits[0];
    const lastCommitDate = lastCommit.commit.author.date;
    const lastCommitTitle = lastCommit.commit.message;
    const pullRequestCount = pullRequests.length;
    const issueCount = issues.length;

    return {
      owner,
      repo,
      lastCommitDate,
      lastCommitTitle,
      pullRequestCount,
      issueCount,
    };
  } catch (error) {
    console.error(`Error fetching GitHub repo info: ${error.message}`);
    return null;
  }
}

// const githubLink = 'https://github.com/dracula/visual-studio-code';

// (async () => {
//   try {
//     await octokit.request('GET /user');
//     const repoInfo = await getGitHubRepoInfoFromLink(githubLink);

//     if (repoInfo) {
//       console.log(`Project Details`);
//       console.log(`${repoInfo.owner}/${repoInfo.repo}`);
//       console.log(`Last Commit: ${new Date(repoInfo.lastCommitDate).toDateString()}`);
//       console.log(`Last Commit Title: ${repoInfo.lastCommitTitle}`);
//       console.log(`${repoInfo.pullRequestCount} Pull Requests`);
//       console.log(`${repoInfo.issueCount} Open Issues`);
//     }
//   } catch (err) {
//     console.error(err);
//   }
// })();