import { db } from "./database-services.js";
import { DOCS, GENERIC_PHONES } from "../common/constants.js";
import { pushPhone } from "./users-services.js";

const { USERS, PHONEBOOK, PENDING_USERS, INVITED_USERS, BLOCKED, BANNED, ADDONS, DRAFT_ADDONS, PENDING_ADDONS, CATEGORIES, } = DOCS;


const setDocument = async (doc) => {
  return db.set(`${doc}`, "generic");
};

const getDocument = async (doc) => {
  return db.get(`${doc}`);
};

/**
 * Initializes the database documents.
 * @async
 * @function initializeDatabaseDocs
 * @returns {void} This function does not return any value.
 *
 * @throws {Error} If an error occurs while creating the main documents in the database.
 */
export const initializeDatabaseDocs = async () => {
  try {
    setDocument(USERS);
    setDocument(PHONEBOOK);
    setDocument(PENDING_USERS);
    setDocument(INVITED_USERS);
    setDocument(BLOCKED);
    setDocument(BANNED);
    setDocument(ADDONS);
    setDocument(PENDING_ADDONS);
    setDocument(DRAFT_ADDONS);
    setDocument(CATEGORIES);
  } catch (error) {
    console.log(`Error occurred while creating the main documents in the database:${error.message}\n
                 Please check your database rules settings.`);
  }
};


const getObjects = (obj, array) => {
  const arr = [];
  try {
    if(obj && array) {
      for (const key in obj) {
        array.map(item => {
          if(item == key) {
            array.push(obj[key])
          }
        })
      }
    }
    console.log(`Success new array: ${arr}`)
  } catch(error) {
    console.log(`Failed to get objects: ${error.message}`)
  }
  return arr;
}





export const getData = async () => {
  try {
    const addons = await getDocument(ADDONS);
    const phoneBook = await getDocument(PHONEBOOK);
    const bannedUsers = await getDocument(BANNED);
    const blockedUsers = await getDocument(BLOCKED);
    const categories = await getDocument(CATEGORIES);
    const data = await Promise.all([
      addons,
      phoneBook,
      bannedUsers,
      blockedUsers,
      categories,
    ]);
    return data;
  } catch (error) {
    console.log(`Error occurred while fetching the main data need for the application to run:${error.message}\n
                 For further info check the error message.`);
  }
};

const getAllAddons = async () => {
  return await db.get(`addons/`);
};

const getBanned = async () => {
  return await db.get(`banned/`);
};

const getBlocked = async () => {
  return await db.get(`blocked/`);
};

const getPhonebook = async () => {
  return await db.get(`phonebook/`);
};

const setPhonebok = () => {
  return db.set(`phonebook/`, "0000000000");
};

export const blockGenericPhones = async () => {
  pushPhone(GENERIC_PHONES);
};

// const phonebook = await getPhoneNumbers();

// const unique = phonebook.map( el => {
//   if (!unique.includes(el)) {
//     return el;
//   }
// })
