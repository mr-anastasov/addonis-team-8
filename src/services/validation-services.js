import { ADDON_MAX_LENGTH, ADDON_MIN_LENGTH, NUMBER_MIN_LENGTH } from '../common/constants';
import { USERNAME_MAX_LENGTH, PASSWORD_MIN_LENGTH, USERNAME_MIN_LENGTH, FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH, LAST_NAME_MIN_LENGTH, LAST_NAME_MAX_LENGTH,} from "../common/constants";

/**
 * Validates registration form data.
 * @function registrationValidation
 * @param {string} firstName - The first name.
 * @param {string} lastName - The last name.
 * @param {string} email - The email address.
 * @param {string} password - The password.
 * @param {string} username - The username.
 * @param {string} number - The number.
 * @returns {string|null} An error message or null if data is valid.
 */ 
export const registrationValidation = (firstName, lastName, email, password, username, number, phonesArray) => {

  if (typeof firstName !== 'string' || typeof lastName !== 'string' || typeof email !== 'string' || typeof password !== 'string' || typeof username !== 'string' || typeof number !== 'string') {
    return 'Invalid data type';
  }

  if (isEmpty(firstName) || !isValidFirstName(firstName)) {
    return `First name should be between ${FIRST_NAME_MIN_LENGTH} and ${FIRST_NAME_MAX_LENGTH} characters long`;
  }

  if (isEmpty(lastName) || !isValidLastName(lastName)) {
    return `Last name should be between ${LAST_NAME_MIN_LENGTH} and ${LAST_NAME_MAX_LENGTH} characters long`;
  }

  if (isEmpty(email) || !isValidEmail(email)) {
    return `Email should be valid`;
  }

  if (isEmpty(password) || !isValidPassword(password)) {
    return `Password should be at least ${PASSWORD_MIN_LENGTH} characters long and contain at least one digit, one lowercase, one uppercase letter and one special symbol`;
  }

  if (isEmpty(username) || !isValidUsername(username)) {
    return `Username should be between ${USERNAME_MIN_LENGTH} and ${USERNAME_MAX_LENGTH} characters long`;
  }

  if (isEmpty(number) || !isValidNumber(number)) {
    return `Number should be at least ${NUMBER_MIN_LENGTH} characters long`;
  }

  if (phonesArray.includes(number)) {
    return `This number is already registered!`;
  }


  return false;
}


export const changeProfileInfoValidation = (firstName, lastName, gitHubRepo, number, phonesArray) => {

 // if (typeof firstName !== 'string' || typeof lastName !== 'string' || typeof number !== 'string' || gitHubRepo !== 'string' ) {
  //   return 'Invalid data type';
  // }
  
  if (!isValidFirstName(firstName)) {
    return `First name should be between ${FIRST_NAME_MIN_LENGTH} and ${FIRST_NAME_MAX_LENGTH} characters long`;
  }

  if (!isValidLastName(lastName)) {
    return `Last name should be between ${LAST_NAME_MIN_LENGTH} and ${LAST_NAME_MAX_LENGTH} characters long`;
  }
  
  if (!isValidUrl(gitHubRepo)) {
    return `GitHub repo should be valid url address`;
  }

  if (phonesArray.includes(number)) {
    return `This number is already registered!`;
  }

  if (!isValidNumber(number)) {
    return `Number should be at least ${NUMBER_MIN_LENGTH} characters long`;
  }


  return false  ;
}

export const createAddonValidation = (name, tag, url) => {
  if (!isValidAddon(name)){
    return `The addon name should be between ${ADDON_MIN_LENGTH} and ${ADDON_MAX_LENGTH} character long`;
  }
  if (url === ''){
    return `The original link field cannot be empty!`;
  }
  if (!isValidTagInput(tag)){
    return `The tag field shoud include at least one tag and cannot contain special characters!`;
  }

}




/**
 * Checks if a first name is valid.
 * @function isValidFirstName
 * @param {string} name - The first name to validate.
 * @returns {boolean} True if the first name is valid, otherwise false.
 */
const isValidFirstName = (name) => {
  return name.length >= FIRST_NAME_MIN_LENGTH && name.length <= FIRST_NAME_MAX_LENGTH;
}

/**
 * Checks if a last name is valid.
 * @function isValidLastName
 * @param {string} name - The last name to validate.
 * @returns {boolean} True if the last name is valid, otherwise false.
 */
const isValidLastName = (name) => {
  return name.length >= LAST_NAME_MIN_LENGTH && name.length <= LAST_NAME_MAX_LENGTH;
}

/**
 * Checks if an email is valid.
 * @function isValidEmail
 * @param {string} email - The email to validate.
 * @returns {boolean} True if the email is valid, otherwise false.
 */
export const isValidEmail = (email) => {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);  
};

/**
 * Checks if a password is valid.
 * @function isValidPassword
 * @param {string} password - The password to validate.
 * @returns {boolean} True if the password is valid, otherwise false.
 */
export const isValidPassword = (password) => {
  const passwordRegex = new RegExp(
    `(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*_=\\-+]).{${PASSWORD_MIN_LENGTH},}`
  );
  // const passwordRegex = /^(?=.*d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
  // // const length = password.length >= PASSWORD_MIN_LENGTH;   
  // const symbols = passwordRegex.test(password)
  // console.log(symbols, length)
  return passwordRegex.test(password);  
};

/**
 * Checks if a number is valid.
 * @function isValidNumber
 * @param {string} number - The number to validate.
 * @returns {boolean} True if the number is valid, otherwise false.
 */
export const isValidNumber = (number) => {
  return number.length >= NUMBER_MIN_LENGTH
};

/**
 * Checks if a username is valid.
 * @function isValidUsername
 * @param {string} username - The username to validate.
 * @returns {boolean} True if the username is valid, otherwise false.
 */
const isValidUsername = (username) => {
  return (
    username.length >= USERNAME_MIN_LENGTH &&
    username.length <= USERNAME_MAX_LENGTH
  );
};


export const isEmpty = (value) => value.trim() === '';

/**
 * Checks if an addon name is valid.
 * @function isValidAddon
 * @param {string} addonName - The addon name to validate.
 * @returns {boolean} True if the addon name is valid, otherwise false.
 */
export const isValidAddon = (addonName) => {
  return (
    addonName.length >= ADDON_MIN_LENGTH &&
    addonName.length <= ADDON_MAX_LENGTH
  );
}

/**
 * Checks if tag input is valid.
 * @function isValidTagInput
 * @param {string} input - The tag input to validate.
 * @returns {boolean} True if the tag input is valid, otherwise false.
 */
export const isValidTagInput = (input) => {
  const tagRegex = /^[^\s!@#$%^&*_=+\-].*$/;
  const tags = input.split(" ");

  for (const tag of tags) {
    if (!tagRegex.test(tag)) {
      return false;
    }
  }

  return true;
};


const isValidUrl = (url) => {
  const urlPattern = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i;
  return urlPattern.test(url);
}



