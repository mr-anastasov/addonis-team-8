import Router from "./components/Router";
import Footer from "./components/Footer";
import Nav from "./components/NavBar";
import { neumorphism } from "./styles/theme"
import { ThemeProvider } from "@mui/material";
import { UseAppData } from "./context/AppContext";
import { UseUserContext } from "./context/UserContext";
import { UseThemeContext } from "./context/ThemeContext";
import image from "./assets/Load photo.jpg"
import { CssBaseline, } from "@mui/material";
export default function App() {

  const { ...data } = UseAppData();
  const  { userData, role } = UseUserContext();
  const { theme, toggleTheme } = UseThemeContext();
  // console.log(theme)     
  // console.log(toggleTheme)   

  if (userData !== null) {
    console.log('User data is loaded', role);
  }

  console.log('App data is loaded', data);
  
  if (data.isLoading) {
    // You can return a loading indicator here
    return <div>
            <img src={image} />
            Loading...
          </div>;
  }

  return (
    <>
      <ThemeProvider theme={neumorphism(theme)}>
        <CssBaseline enableColorScheme/>
        <Nav /> 
        <Router /> 
        <Footer />
      </ThemeProvider>
    </>
  )
}
