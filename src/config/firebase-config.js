import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from "firebase/storage";
/**
 * Firebase configuration object containing authentication and database details.
 * @typedef {Object} FirebaseConfig
 * @property {string} apiKey - The API key for Firebase authentication.
 * @property {string} authDomain - The domain for Firebase authentication.
 * @property {string} databaseURL - The URL for the Firebase Realtime Database.
 * @property {string} projectId - The ID of the Firebase project.
 * @property {string} storageBucket - The storage bucket associated with the Firebase project.
 * @property {string} messagingSenderId - The messaging sender ID for Firebase.
 * @property {string} appId - The Firebase application ID.
 */

/**
 * The Firebase configuration object.
 * @type {FirebaseConfig}
 */
const firebaseConfig = {
    apiKey: "AIzaSyAumUVvAg0b50XqC78P5rfNalPjYHitke4",
    authDomain: "addon-market-1bad5.firebaseapp.com",
    databaseURL: "https://addon-market-1bad5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "addon-market-1bad5",
    storageBucket: "addon-market-1bad5.appspot.com",
    messagingSenderId: "1060185541974",
    appId: "1:1060185541974:web:01ff5179b88b57efc65af7"

      // apiKey: "AIzaSyCTl7x1W-Pz20anvMSq6glKb5Afi5cj_qs",
      // authDomain: "ai---marketplace.firebaseapp.com",
      // databaseURL: "https://ai---marketplace-default-rtdb.europe-west1.firebasedatabase.app",
      // projectId: "ai---marketplace",
      // storageBucket: "ai---marketplace.appspot.com",
      // messagingSenderId: "192044470711",
      // appId: "1:192044470711:web:9aa3d65c5322697c819832"
  };

/**
 * Initialized Firebase application.
 * @type {import('firebase/app').App}
 */
export const app = initializeApp(firebaseConfig);

/**
 * The Firebase authentication handler.
 * @type {import('firebase/auth').Auth}
 */
export const auth = getAuth(app);

/**
 * The Firebase Realtime Database handler.
 * @type {import('firebase/database').Database}
 */
export const database = getDatabase(app);
export const storage = getStorage(app);