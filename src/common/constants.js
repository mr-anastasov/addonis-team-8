export const FIRST_NAME_MIN_LENGTH = 4;
export const FIRST_NAME_MAX_LENGTH = 32;

export const LAST_NAME_MIN_LENGTH = 4;
export const LAST_NAME_MAX_LENGTH = 32;

export const USERNAME_MIN_LENGTH = 2;
export const USERNAME_MAX_LENGTH = 21;

export const NUMBER_MIN_LENGTH = 10;
export const PASSWORD_MIN_LENGTH = 8;

export const ADDON_MIN_LENGTH = 3;
export const ADDON_MAX_LENGTH = 30;

export const GITHUB_REPO = "addon-market";

export const PATHS = {
  // public
  HOME: "/",
  SEARCH: "/search",
  SEARCH_RESULTS: "/search-results",
  LOGIN: "/login",
  SIGN_UP: "/signup",

  // private
  MY_PROFILE: "/my-profile",
  MY_ADDONS: "/my-addons",
  CREATE_ADDON: "/create-addon",
  EDIT_ADDON: "/edit-addon",
  EDIT_PROFILE: "/edit-profile-info",
  EDIT_PASSWORD: "/edit-password",
  NOTIFICATION: "/notifications",
  FORGOT_PASSWORD: "/forgot-password",
  RESET_PASSWORD: "/reset-password",
  CONFIRM: "/confirm",

  // admin
  ADMIN_DASHBOARD: "/admin-dashboard",
  ALL_USERS: "/all-users",
  ALL_ADDONS: "/all-addons",
  PENDING_ADDONS: "/pending-addons",
  EDIT_ADMIN: "/edit-admin",

  ADMIN_ADDON: "/admin-addon",
  ADMIN_USER: "/admin-user",
  USER: "/user",
  ADMIN_EDIT_ADDON: "/admin-edit-addon",
  ADMIN_EDIT_USER: "/admin-edit-user",

  // general
  ERROR: "/error",
  ABOUT: "/about",
  PRIVACY: "/privacy",
  RULES: "/terms-of-service",

  // test
  TEST_DASHBOARD: "/test-dashboard",
  TEST: "/test",
  FORM: "/form",
  ADDON: "/addon",
};

export const DOCS = {
  USERS: "users",
  PHONEBOOK: "phonebook",
  PENDING_USERS: "pendingUsers",
  INVITED_USERS: "invitedUsers",
  BLOCKED: "blocked",
  BANNED: "banned",
  ADDONS: "addons",
  PENDING_ADDONS: "pendingAddons",
  DRAFT_ADDONS: "draftAddons",
  CATEGORIES: "categories",
};

export const GENERIC_PHONES = [
  "0000000000",
  "1111111111",
  "2222222222",
  "3333333333",
  "4444444444",
  "5555555555",
  "6666666666",
  "7777777777",
  "8888888888",
  "9999999999",
];
