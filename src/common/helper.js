import { useNavigate } from "react-router-dom";
import { PATHS } from "./constants";

// Create a function component that uses the navigate hook

const {
  // public
  HOME,
  SEARCH,
  SEARCH_RESULTS,
  // private
  SIGN_UP,
  LOGIN,
  CREATE_ADDON,
  MY_PROFILE,
  EDIT_PROFILE,
  EDIT_ADDON,
  MY_ADDONS,
  CONFIRM,
  NOTIFICATION,
  // admin
  TEST_DASHBOARD,
  USER,
  ALL_USERS,
  ALL_ADDONS,
  ADMIN_DASHBOARD,
  EDIT_ADMIN,
  // general
  ERROR,
  ABOUT,
  PRIVACY,
  TERMS_OF_SERVICE,
  // test
  TEST,
  FORM,
} = PATHS;
export default function NavigationHelper() {
  const navigate = useNavigate();

  // public
  const goHome = () => {
    navigate(HOME);
  };

  const goToLogin = () => {
    navigate(LOGIN);
  };

  const goToSignUp = () => {
    navigate(SIGN_UP);
  };

  const goToSearchResults = () => {
    navigate(SEARCH_RESULTS);
  };


  // private
  const goToCreateAddon = () => {
    navigate(CREATE_ADDON);
  };

  const goToMyProfile = () => {
    navigate(MY_PROFILE);
  };

  const goToEditUser = () => {
    navigate(EDIT_PROFILE);
  };

  const goToMyAddons = () => {
    navigate(MY_ADDONS);
  };

  const goToEditAddon = (addonID) => {
    navigate(EDIT_ADDON, { state: { addonID } });
  };

  const goToConfirm = () => {
    navigate(CONFIRM);
  };

  const goToNotifcation = () => {
    navigate(NOTIFICATION);
  };

  // admin
  const goToAdmin = () => {
    navigate(TEST_DASHBOARD);
  };

  const goToUser = () => {
    navigate(USER);
  };

  const goToAllUsers = () => {
    navigate(ALL_USERS);
  };


  const goToAdminDashboard = () => {
    navigate(ADMIN_DASHBOARD);
  };

  const goToAdminEdit = (addonID) => {
    navigate(EDIT_ADMIN, { state: { addonID } });
  };
  const goToAllAddons = () => {
    navigate(ALL_ADDONS);
  };

  // general
  const goTo404 = () => {
    navigate(ERROR);
  };

  const goToAbout = () => {
    navigate(ABOUT);
  };

  const goToPrivacy = () => {
    navigate(PRIVACY);
  };

  const goToTermsOfService = () => {
    navigate(TERMS_OF_SERVICE);
  };

  // test
  const goToForm = () => {
    navigate(FORM);
  };
  const goToTest = () => {
    navigate(TEST);
  };
  const goBack = () => {
    navigate(-1);
  };

  // Return null, as this component doesn't render anything
  return {
    // public
    goHome,
    goToLogin,
    goToSignUp,
    goToSearchResults,
    // private
    goToMyProfile,
    goToEditUser,
    goToMyAddons,
    goToCreateAddon,
    goToEditAddon,
    goToConfirm,
    goToNotifcation,
    // admin
    goToAdmin,
    goToUser,
    goToAllUsers,
    goToAdminDashboard,
    goToAllAddons,
    goToAdminEdit,
    // general
    goTo404,
    goToAbout,
    goToPrivacy,
    goToTermsOfService,
    // test
    goToTest,
    goToForm,
    goBack,
  };
}
