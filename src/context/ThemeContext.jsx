  import { createContext, useState, useContext } from "react";
  import PropTypes from 'prop-types';

  /**
   * Creates a React context called `themeContext` and a corresponding provider component called `ThemeProvider`.
   * Also provides a custom hook called `useTheme` to access the theme value from the context.
   *
   * @module themeContext
   */

  const ThemeContext = createContext({
    mode: 'light',
    changeTheme: () => {},
  });

  /**
   * Provides the theme context to the wrapped components.
   * @param {Object} props - The component props.
   * @param {ReactNode} props.children - The children components that will be wrapped by the `ThemeProvider`.
   * @returns {ReactNode} The `themeContext.Provider` component with the `value` prop set to the `value` object and the `children` as its children.
   */
  export const ThemeProviderCustom = ({ children }) => {
    const [theme, setTheme] = useState('light');

    /**
     * Toggles the theme state between `dark and light mode` and its opposite value.
     */
    const toggleTheme = () => {
      setTheme((mode) => (mode === 'light' ? 'dark' : 'light'));
    };

    const value = {
      mode: theme,    
      changeTheme: toggleTheme,
    };  

    return (<ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>)
  };

  /**
   * A custom hook to access the theme value from the `themeContext`.
   * @returns {string} The current theme value from the `themeContext`.
   */
  export const UseThemeContext = () => {
    return useContext(ThemeContext);
  };


  ThemeProviderCustom.propTypes = {
    children: PropTypes.node.isRequired,
  };