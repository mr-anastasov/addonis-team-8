import { createContext, useState, useContext, useEffect } from "react";
import { getData } from "./../services/app-services"


const AppDataContext = createContext({
  addons: null,
  blocked: null,
  banned: null,
  phoneNumbers: null,
  categories: null,
  searchInput: null,
  setContext: () => {},
});

export const AppDataProvider = ({children}) => {
  const [isLoading, setLoading] = useState(true);
  const [addons, setAddons] = useState('');
  const [blocked, setBlocked] = useState('');
  const [banned, setBanned] = useState('');
  const [phoneNumbers, setNumber] = useState('');
  const [categories, setCategories] = useState('');
  const [searchInput, setSearchInput] = useState('');

  useEffect(() => {

    async function fetchData() {
      try {
        const data = await getData();
        setAddons(data[0]);
        setNumber(data[1]);
        setBanned(data[2]);
        setBlocked(data[3]);
        setCategories(data[4]);
        setLoading(false);
      } catch (error) {
        console.log(`Error: ${error.message}`);
        setLoading(false);
      }
    } 

    fetchData();
  },[]);


  const setContext = (val) => {
    // Loop through the keys in the updateObj and update the corresponding state variables
    for (const key in val) {
      if (val.hasOwnProperty(key)) {
        switch (key) {
          case 'addons':
            setAddons(val[key]);
            break;
          case 'blocked':
            setBlocked(val[key]);
            break;
          case 'banned':
            setBanned(val[key]);
            break;
          case 'phoneNumbers':
            setNumber(val[key]);
            break;
          case 'categories':
            setCategories(val[key]);
            break;
          case 'searchInput':
            setSearchInput(val[key]);
            break;
          // Add cases for other context properties as needed
          default:
            // Handle other cases or provide an error message
            break;
        }
      }
    }
  }

  const value = {
    addons,
    blocked,
    banned,
    phoneNumbers,
    categories,
    searchInput,
    setContext,
  };
  
  if(isLoading) {
    return <div>Loading...</div>
  }

  return (<AppDataContext.Provider value={value}>{children}</AppDataContext.Provider>)
}

export const UseAppData = () => {
  return useContext(AppDataContext);
}