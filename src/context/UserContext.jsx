  import { createContext, useState, useContext, useEffect } from "react";
  import { auth } from "./../config/firebase-config.js"
  import { useAuthState } from "react-firebase-hooks/auth";
  import { getUserById } from "./../services/users-services"; 


  const UserContext = createContext({
    role: null,
    userData: null,
    setContext: () => {},
  });


  export const UserContextProvider = ({ children }) => {

    const [ user ] = useAuthState(auth);

    const currentUser = auth.currentUser;

    // let currentUser = null;
    // if (user) {
    //   currentUser = auth.currentUser
    // } 
    
    // const [uid, setUid] = useState(null);
    // const [username, setUsername] = useState(null);
    const [role, setRole] = useState(null);
    const [userData, setData] = useState(null);
    const [loading, setLoading] = useState(true);

 
    useEffect(() => {

      async function fetchData() {
        if(currentUser) {
          try {
              const userData = await getUserById(currentUser.uid);
              setRole(userData.role);
              setData(userData);
              setLoading(false);
            } catch (error) {
              console.log(`Error: ${error.message}`);
              setLoading(false);
            }
        } else {
          setRole(null);
          setData(null);
          setLoading(false);
        }
      } 

      fetchData();

    },[currentUser]);


    const setContext = (data) => {
      setRole(data.role);
      setData(data);
    }

    const value = {
      role,
      userData: userData,
      setContext
    }

    // if(loading) {
    //   return <div>User is being authenticated...</div>
    // }


    return (<UserContext.Provider value={value}>{children}</UserContext.Provider>);
  }

  export const UseUserContext = () => {
    return useContext(UserContext);
  }