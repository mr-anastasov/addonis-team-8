import { Button, styled } from "@mui/material"
import NavigationHelper from '../common/helper';




// const AnimatedButton = styled(Button)({
//     transition: 'background-color 0.2s, color 0.2s',
//     '&:hover': {
//         backgroundColor: '#ff5722',
//         color: '#fff',
//     },
// });


export default function Footer() {

    const CenteredButtons = styled("div")({
        flexGrow: "1",
        display: "flex",
        justifyContent: "center",
        gap: "50px",
        marginTop: "auto",
        border: "3px solid #ccc",
        borderRadius: "8px",
        padding: "20px",
        backgroundColor: "#e2e2e2"
      });

 
  
    const { goToPrivacy, goToAbout } = NavigationHelper();

    return(
        <CenteredButtons>
            <Button variant="contained" onClick={goToAbout}>About</Button>
            <Button variant="contained" onClick={goToPrivacy}>Privacy Policy</Button>
        </CenteredButtons>
    )
}