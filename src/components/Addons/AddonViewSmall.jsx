import DownloadDoneIcon from '@mui/icons-material/DownloadDone';
import StarIcon from "@mui/icons-material/Star";

import { Box, Button, Typography, styled, IconButton } from "@mui/material";
import  PropTypes  from "prop-types";

const Item = styled('div')({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  border: "1px solid #ccc",
  borderRadius: "8px",
  height: "350px",
  width: "100%",
  backgroundColor: "white",
  color: "black",
  margin: "0 10px",
  transition: "box-shadow 0.3s ease, border-width 0.3s ease",
  boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
  borderWidth: "1px",
  "&:hover": {
    boxShadow: "0px 10px 14px rgba(0, 0, 0, 0.5)",
    borderWidth: "5px"
  },
});

const ItemImage = styled('img')({
  width: "100px",
  height: "100px",
  objectFit: "cover",
  margin: "auto",
  padding: "15px",
  display: "block",
});

const FeaturedButton = styled(IconButton)({
  position: "absolute",
  top: "5px", // Adjust the top position as needed
  right: "5px", 
  zIndex: 1, 
});

export function AddonViewSmall({ addon, approveAddon, deleteAddon, featureAddon }) {

  
  const { addonImg, name, IDE, state, creator, uid, featured } = addon;
  const bool = featured ? false : true;
  
  return (
    <Box key={uid}>
    <Item>

      <FeaturedButton onClick={() => featureAddon(uid, bool)}>
                <StarIcon />
      </FeaturedButton>
      <ItemImage src={addonImg} alt={name} />
      <Typography sx={{ fontSize: "16px", textAlign: "center", margin: "5px", fontWeight: "600" }}>{name}</Typography>
      <Typography sx={{ fontSize: "13px", textAlign: "left", margin: "5px", color: "#767676" }}>{IDE}</Typography>
      <Typography sx={{ fontSize: "13px", textAlign: "left", margin: "5px", color: "#767676" }}>State: {state}</Typography>
      <Typography sx={{ fontSize: "12px", color: "#767676", display: "flex", alignItems: "center", justifyContent: "space-between", margin: "5px" }}>
        <span>{creator}</span>
        <span style={{ display: "flex", alignItems: "center" }}>
          <DownloadDoneIcon />
          50
        </span>
      </Typography>
      <Typography sx={{ fontSize: "0.5em", textAlign: "left", margin: "5px" }}>Ranking:</Typography>
      <Box sx={{display: "flex", alignItems: "center" }}>

      </Box>
      <div style={{ display: "flex", alignItems: "center" }} >
        <Button variant="contained" sx={{ margin: "10px", width: "100px" }} onClick={() => approveAddon(uid)}>Approve</Button>
        <span>&nbsp;</span>
        <Button variant="contained" sx={{ margin: "10px", width: "100px" }}onClick={() => deleteAddon(uid)} >Remove</Button>
      </div>
    </Item>
  </Box>
  )
}

AddonViewSmall.propTypes = {
  addon: PropTypes.object.isRequired,
  approveAddon: PropTypes.func.isRequired,
  deleteAddon: PropTypes.func.isRequired,
  featureAddon: PropTypes.func.isRequired,
}