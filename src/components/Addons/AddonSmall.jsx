import {  Grid, IconButton, ImageListItemBar, ListItem, Typography, Box } from "@mui/material";
import ModeEditOutlineTwoToneIcon from '@mui/icons-material/ModeEditOutlineTwoTone';
import DeleteForeverTwoToneIcon from '@mui/icons-material/DeleteForeverTwoTone';
import PropTypes from 'prop-types'
import { addonUtils } from "../../services/addon-services";
import NavigationHelper from "./../../common/helper"

export default function AddonSmall({addon}) {

  const { goToEditAddon } = NavigationHelper();
  const { addonImg, name, addonID } = addon;

  // import { addonUtils } from "../../services/addon-services";
  // import NavigationHelper from "./../../common/helper"
  
  // const handleDelete = () => {
  //   const path = `/${DOCS.ADDONS}/${addonID}/state/`;
  //   const state = 'deleted';
  //   db.set(path, state)
  //   alert("Addon deleted successfully");
  // }
  
  const handleEdit = () => {
    addonUtils.edit(addonID,goToEditAddon);
  }


  return (
      <>
        <Grid >
          <ListItem >
            <img
              src={`${addonImg}?w=248&fit=crop&auto=format`}
              // srcSet={`${addonImage}?w=248&fit=crop&auto=format&dpr=2 2x`}
              alt={name}
              loading="lazy"
              width={300}
              height={300}
            />
              <ImageListItemBar
                sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                width: "100%",
                }}
                // title={name}
                // subtitle={creator}
                actionIcon={
                  <Box sx={{justifyContent: "space-between", alignItems: "center",}}>
                    <IconButton
                      sx={{ color: 'rgba(255, 255, 255, 0.54)', position:"sticky", right: "5"}}
                      aria-label={`info about ${name}`}
                      onClick={handleEdit}
                      ><ModeEditOutlineTwoToneIcon />
                    </IconButton>
                    <Typography variant="p" sx={{m: 6, position: "relative"}}>{name}</Typography>
                    <IconButton
                      sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
                      aria-label={`info about ${name}`}
                      onClick={() => {}} 
                      ><DeleteForeverTwoToneIcon />
                    </IconButton>
                  </Box>
                } 
             />
          </ListItem>
        </Grid>
      </>
  )
}


AddonSmall.propTypes = {
  addon: PropTypes.object
}