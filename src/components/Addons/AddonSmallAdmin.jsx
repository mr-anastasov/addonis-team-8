import { Card, CardActions, CardContent, CardMedia, Button, Typography} from '@mui/material';
import { PropTypes } from 'prop-types'
export default function AddonSmallAdmin({addon}) {
  
  const {img, title, description} = addon;
  return (
    <Card sx={{ maxWidth: 350, m:1, p:2, }}>
        <Typography gutterBottom variant="h5" component="div" sx={{m: 1}}>
          Lizard
        </Typography>
      <CardMedia
        sx={{ height: 300, width: 300, m: 1}}
        image={img}
        title={title}
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          { "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis reiciendis rerum maxime fugiat iusto assumenda expedita amet nihil nulla." || description }
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Edit</Button>
        <Button size="small">Delete</Button>
      </CardActions>
    </Card>
  );
}

AddonSmallAdmin.propTypes = {
  addon: PropTypes.object
}