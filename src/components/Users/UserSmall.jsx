import { Card, CardMedia, CardActions, Typography, Button } from "@mui/material";
import { db } from "../../services/database-services";
import { DOCS } from "../../common/constants";
export default function UserSmall() {


  const username = 'DtOuMnr0hxP62u52uygLkzVbpfA2';
  const image= 'test';
  const path = `/${DOCS.USERS}/${username}/role/`;
  const data = 'banned';
  const handleBlock = async () => {
    db.set(path, data)
  }



  return (
    <>
      <Card sx={{ maxWidth: 350, m:1, p:2, }}>
        <Typography gutterBottom variant="h5" component="div" sx={{m: 1}}>
          {username}
        </Typography>
      <CardMedia
        sx={{ height: 300, width: 300, m: 1}}
        image={image}
        title={username}
      />
      <CardActions>
        <Button size="small" onClick={handleBlock}>Block</Button>
        <Button size="small">View</Button>
      </CardActions>
    </Card>
    </>
  );
}
