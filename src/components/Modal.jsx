import { Typography, Button, Modal, ButtonGroup,Box } from "@mui/material";
import { useState, useEffect } from "react";
import PropTypes from 'prop-types';


export function CustomModal({ openByDefault = true, onClose: onSave, title, children, modalHeight, modalWidth }) {
  const [openModal, setOpenModal] = useState(openByDefault);
  
  useEffect(() => {
    setOpenModal(openByDefault);
  }, [openByDefault]);

  const handleToggle = () => {
    setOpenModal(false);
  };


  return (
    <>
      <Modal open={openModal} onClose={handleToggle}>
      <Box sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            background: "white",
            padding: "20px",
            boxShadow: "0px 2px 10px rgba(0, 0, 0, 0.2)",
            width: modalWidth || "auto",
            height: modalHeight || "auto",
          }}
        >
          <Typography variant="h6" sx={{alignItems: "center"}}>{title}</Typography>
          {children}
          <Box> 
            <ButtonGroup orientation='horizontal' >
              <Button onClick={handleToggle} variant="contained" sx={{m: 1, p: 1}}>Cancel</Button>
              <Button onClick={onSave} variant="contained" sx={{m: 1, p: 1}}>Save</Button>
            </ButtonGroup>
          </Box>
        </Box>
      </Modal>
    </>
  );
}


CustomModal.propTypes = {
  openByDefault: PropTypes.bool,
  onClose: PropTypes.func,
  title: PropTypes.string,
  children: PropTypes.node,
  submitText: PropTypes.string,
  modalHeight: PropTypes.string,
  modalWidth: PropTypes.string,
};