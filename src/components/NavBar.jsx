import { useState } from "react";
import { AppBar, styled, Toolbar, Box, Button } from "@mui/material";
import { Tooltip, IconButton, Menu, MenuItem, Divider, ListItemIcon, Avatar, } from "@mui/material";
import Logout from "@mui/icons-material/Logout";
import TeamLogo from "../assets/TeamLogo.svg";
import NavigationHelper from "../common/helper";
import { auth } from "../config/firebase-config";
import { UseUserContext } from "../context/UserContext";
import { Link } from "react-router-dom";
import SearchBar from "../views/Public/SearchBar";
import { UseThemeContext } from "../context/ThemeContext";
export default function Nav() {
  
  const { goToLogin, goToCreateAddon, goToMyProfile, goHome, goToAdmin, goToMyAddons,goToAdminDashboard, goToNotifcation} = NavigationHelper();
  const { setContext, role, userData} = UseUserContext();
  const { changeTheme, theme } = UseThemeContext();
  const [ anchorEl, setAnchorEl] = useState(null);

  const handleLogout = () => {
    sessionStorage.clear();
    auth.signOut();
    setContext({ role: null, userData: null,});
    goHome();
  };
    
  const adminDetails = role === "admin" ? true : role === "blocked" ? false : null;
  const userDetails = role === "blocked" ? false : 
  role === "admin" ? false : 
  role !== null ? true : false;
  
  const open = Boolean(anchorEl);
  
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  
  const handleClose = () => {
    setAnchorEl(null);
  };
  

  const TeamLogoCustom = styled("img")({
    width: "7%",
  });

  const Spacing = styled("div")({
    marginLeft: "10px",
  }); 

  // const handleToggle = () => {
  //   console.log("toggle", theme);
  //   changeTheme();
  // };

  return (
    <Box sx={{ flexGrow: 1}}>
      <AppBar position="sticky">
        <Toolbar sx={{ display: "flex", justifyContent: "space-between" }}>
          <TeamLogoCustom src={TeamLogo} onClick={goHome}></TeamLogoCustom>
          <Toolbar>
         <SearchBar />
            <Spacing />
            { role === null && (
              <Button variant="contained" size="small" onClick={goToLogin}>Login</Button>)
            }
            { adminDetails && (
              <>
                {/* <Button variant="contained" size="small" sx={{ borderRadius: "8px", }} onClick={goToAdmin}>Test</Button> */}
                <Button variant="contained" size="small" onClick={goToAdminDashboard}>Admin</Button>
              </> )
              }
            { userDetails && (
              <>
                <Button variant="contained" size="small" sx={{ borderRadius: "8px", }} onClick={goToCreateAddon}>Create</Button>
                <Button onClick={goToNotifcation}>Notifications</Button>
              </>
            )}
            {(userDetails || adminDetails) && (
              <>
                <Box sx={{ display: "flex", alignItems: "center", textAlign: "center", }}>
                  <Tooltip title="Account settings">
                    <IconButton onClick={handleClick} size="small" sx={{ ml: 2 }} aria-controls={open ? "account-menu" : undefined} aria-haspopup="true" aria-expanded={open ? "true" : undefined} >
                      {userData.imageUrl !== null ? 
                        ( <Avatar src={`${userData.imageUrl}`} sx={{ width: 32, height: 32 }}></Avatar> ) : 
                        ( <Avatar sx={{ width: 32, height: 32 }}> Default </Avatar> )
                      }
                    </IconButton>
                  </Tooltip>
                </Box>
                <Menu anchorEl={anchorEl} id="account-menu" open={open} onClose={handleClose} onClick={handleClose} transformOrigin={{ horizontal: "right", vertical: "top" }} anchorOrigin={{ horizontal: "right", vertical: "bottom" }} className="men-container" >
                  <MenuItem onClick={goToMyProfile}>My Profile</MenuItem>
                  <MenuItem onClick={goToMyAddons}>My Addons</MenuItem>
                  <Divider />
                  <MenuItem onClick={handleLogout}>
                    <ListItemIcon>
                      <Logout fontSize="small" />
                    </ListItemIcon>Logout
                  </MenuItem>
                </Menu>
              </> )
            }
            {/* <Button onClick={handleToggle}>
              change
            </Button> */}
            {/* <IconButton onClick={changeTheme}>
              <Brightness1Rounded />
            </IconButton> */}
          </Toolbar>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
