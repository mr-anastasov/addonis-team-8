import { Routes, Route } from "react-router";
import { PATHS } from "../common/constants";

// Public Views  
import Main from "./../components/Main";
import SearchResult from "../views/Testing/SearchResult";
import FilteredAddons from "../views/Public/FilteredView";
// Private Views 
import Login from "../views/Private/Login";
import Sign from "../views/Private/Sign";
import CreateAddon from "../views/Private/CreateAddon";
import EditProfile from "../views/Private/EditProfile";
import MyAddons from "../views/Private/UserAddons";
import Confirm from "../views/Private/Confirm";
import UserProfile from "../views/Private/UserProfile";
import EditAddon from "../views/Private/EditAddon";

// Admin Views
import AdminDashboard from './../views/Admin/AdminDashboard';
import AllUsers from '../views/Admin/AllUsers';
import PendingAddon from '../views/Admin/PendingAddons';
import Addon from '../views/Admin/Addon';
import UserSmall from "./Users/UserSmall";
import Not from '../views/Admin/Notifications';
// General Views
import Privacy from "../views/General/PrivacyPolicy";
import About from "../views/General/About";
import NotFound from "../views/General/NotFound";
// Testing Views
import TestDashboard from "../views/Testing/TestDashboard";
import TestMap from "../views/Testing/TestMap";
import Form from "../views/Testing/Form";
import AdminEdit from "../views/Admin/AdminEdit";

export default function Router() {

  const { 
    HOME, LOGIN, SIGN_UP, SEARCH, SEARCH_RESULTS,
    CREATE_ADDON, MY_ADDONS, MY_PROFILE, EDIT_PROFILE, CONFIRM, EDIT_ADDON, NOTIFICATION,
    ADMIN_DASHBOARD, EDIT_ADMIN, ALL_ADDONS, PENDING_ADDONS, ALL_USERS,
    PRIVACY, ABOUT, ERROR, 
    TEST_DASHBOARD, TEST ,USER, FORM, 
  } = PATHS;

  return (
    <>
        <Routes>
        <Route path={HOME} element={<Main />} />
        <Route path={LOGIN} element={<Login />} />
        <Route path={SIGN_UP} element={<Sign />} />
        <Route path={SEARCH_RESULTS} element={<FilteredAddons />} />
        <Route path={SEARCH} element={<SearchResult />} />

        <Route path={MY_PROFILE} element={<UserProfile/>} />
        <Route path={MY_ADDONS} element={<MyAddons />} />
        <Route path={EDIT_PROFILE} element={<EditProfile/>} />
        <Route path={CREATE_ADDON} element={<CreateAddon />} />
        <Route path={EDIT_ADDON} element={<EditAddon />} />
        <Route path={CONFIRM} element={<Confirm/>} />
        <Route path={NOTIFICATION} element={<Not/>} />

        <Route path={ADMIN_DASHBOARD} element={<AdminDashboard/>} />
        <Route path={ALL_USERS} element={<AllUsers />} />
        <Route path={ALL_ADDONS} element={<Addon />} />
        <Route path={PENDING_ADDONS} element={<PendingAddon />} />
        <Route path={EDIT_ADMIN} element={<AdminEdit/>} />

        <Route path={PRIVACY} element={<Privacy />} />
        <Route path={ABOUT} element={<About />} />
        <Route path={ERROR  } element={<NotFound />} />

        <Route path={TEST_DASHBOARD} element={<TestDashboard />} />
        <Route path={USER} element={<UserSmall />} />
        <Route path={TEST} element={<TestMap />} />
        <Route path={FORM} element={<Form />} />
      </Routes>
    </>
  )
}
