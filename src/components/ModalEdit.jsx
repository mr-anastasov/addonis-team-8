import { Typography, Button, Modal, ButtonGroup,Box, Grid, Paper, Link } from "@mui/material";
import { styled } from '@mui/material/styles';
import { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { red } from "@mui/material/colors";

const ItemImage = styled('img')({
  width: "300px",
  height: "300px",
  objectFit: "cover",
  margin: "auto",
  marginTop: '20px',
  padding: "15px",
  display: "block",
});

export function EditModal({ addon, modalHeight, modalWidth, open, setOpen, redirectTo }) {
  

  useEffect(() => {
    setOpen(open);
  }, [open]);

  const handleToggle = () => {
    setOpen(!open);
    if (redirectTo) {
      redirectTo();
    }
  };


  


  return (
    <>
      {/* <Button onClick={handleOpenModal}>Open Modal</Button> */}
      <Modal open={open} onClose={handleToggle}>
      <Box sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            background: "white",
            padding: "20px",
            boxShadow: "0px 2px 10px rgba(0, 0, 0, 0.2)",
            width: modalWidth || "auto",
            height: modalHeight || "auto",
          }}
        >
          <Typography variant="h6" sx={{alignItems: "center"}}>Preview</Typography>
          <Grid item xs={12} sm={6}>
              <Paper sx={{ m: 2, p: 2, textAlign: "center"}}>
                <Typography variant="h6" component="div">{addon?.name}</Typography>
              </Paper>
              <ItemImage src={addon?.addonImg} alt={addon?.name} />
          </Grid>
          <Grid item xs={12} sm={6}>
              <Paper sx={{ m: 2, p: 2}}>
                <Typography variant="h6" component="div">IDE: {addon?.IDE}</Typography>
                <Typography variant="h6" component="div">Creator: {addon?.creator}</Typography>
                <Typography variant="h6" component="div">Tags: {addon?.tags}</Typography>
                <Typography variant="h6" component="div">Download link: {<Link to={addon?.downloadURL}>{addon?.downloadURL}</Link>} </Typography>
                <Typography variant="h6" component="div">Origin link: {<Link to={addon?.originUrl}>{addon?.originUrl}</Link>}</Typography>
                <Typography variant="h6" component="div">Number of downloads: {addon?.downloads}</Typography>
              </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper sx={{ m: 2, p: 2}}>
              <Typography variant="h6" component="div">{addon?.description}</Typography>
              </Paper> 
          </Grid>
          <Box> 
            <ButtonGroup orientation='horizontal' >
              <Button onClick={handleToggle} variant="contained" sx={{m: 1, p: 1}}>Close</Button>
            </ButtonGroup>
          </Box>
        </Box>
      </Modal>
    </>
  );
}


EditModal.propTypes = {
  addon: PropTypes.object,
  modalHeight: PropTypes.string,
  modalWidth: PropTypes.string,
  open: PropTypes.bool,
  setOpen: PropTypes.func,
  redirectTo: PropTypes.func,
};