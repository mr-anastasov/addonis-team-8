# Set of rules: how to work on the project. 

## We are going to aim for 4 full iterations: iteration will be 6 days long.
## Meetings when, where and how?
  * Everyday 15 min: **Decide when?**
    * What I did yesterday
    * What I am doing today
    * Do I have any roadblocks
  * **Plannings**: beginning of every iteration up to 2 hours - **adhoc time**
    * Discuss what we need to do in the next iteration
    * Review things that we couldn't get to on the delivery
    * Remind ourselves of good practices and of what we have found in the retrospective
  * **Delivery & Retrospective**: ending every iteration  up to 2 hours - **adhoc**
      * Delivery:
        * discussing and review functionality
        * making a short demo of the current state of the app
        * adding screenshots and pictures of the progress to the README file of the project.
      * Retrospective discussion:
        * what went good?
        * what went bad?
        * what can we improve?
        * is there somethings we don't know?
  * Peer programming / Refactoring sessions - **adhoc**

Good practices for meetings:
  * Schedule meetings and make sure you follow a timeline/agenda 
  * Take notes at meetings to send to everyone afterwards 
  * Research topics for the project   
  * Compile and design presentation for partners
  * Present your project work


## GitLab as tracking system
## Branching system: two project branches: 
  * main(deployment - stable)
  * development( developers are branching off to work on the features their developing or bugs they are fixing)

## What kind of code convention we are going to fallow when writing code? When are we going to refactor existing code?

  
## Expectations of the project, of my team members?
  * Dimitar - to work consistently, to deliver on time, and to be direct in their communication
  * Alex - to do their job, to look for help, to work in a team, to have clear view how the project will look like
  * Zlati - to have a nice looking project, to deliver on time
## My strong sides, my weak sides? max 3?
  * Dimitar:
      * strong sides: can take care of organizing, will refactor, focus on structure
      * weak sides: sometimes I miss details, stuck on something I would like to work
  * Alex
      * strong sides: to deliver on time and with best possible quality, focus on simplicity and composition, rudimentary knowledge of html/css 
      * weak sides: tends to get stuck, sometimes is busy with work, organization of time
  * Zlati
      * strong sides: collaborative,  
      * weak sides: writes big function, get's stuck sometimes 
## Am I lacking any technical skills, what kind?
  * Dimitar - I am yet not the best at React, but I feel way more confident than two weeks ago, no really good with css, but willing to learn will address that in my spear time.
  * Alex - structuring the application, name of variables, and functions, promises
  * Zlati - learning React


## Teamwork guidelines
### Step 1 - Create your repository
    • Create a new group repository on GitLab.
    • Add your teammate as a group members with role owner.
    • Create a new project in the group and mark it as public. Add your trainers as members (with Reporter role). Adding the trainers as members will send them email notification with url of the repo.
    • Create a README.md file in your repository with details about your project. 

### Step 2 – Plan your tasks
How you organize you work is crucial to the success of the team. Create a GitLab Issues board with the following data, fill it and keep it updated:
#### You can have several columns (lists) in the board:
  * Open – the backlog of your project, containing all open issues
  * To do – issues that have been assigned and scheduled with a deadline
  * Doing – what issues are currently in progress (no more than 2 per person)
  * For review – the issues that need to be reviewed by your teammates
  * Closed – all issues that are done
#### You can use different issues for different purposes:
  * Features:
    * Name - the name of the card would be the given feature/task that needs to be done.
    * Size - what is the size of the feature in terms of programming effort i.e., Large/Medium/Small.
    * Priority - what is the importance of the feature i.e., Must/Should/Could.
    * Owner - who is responsible for the successful completion of the given feature/task.
  * Bugs:
    * Name - the name of the card would be the given issue/problem of the software that need to be fixed.
    * Size - what is the size of the feature in terms of programming effort i.e., Large/Medium/Small.
    * Priority - what is the importance of the feature i.e., Must/Should/Could.
    * Owner - who is responsible for the successful completion of the given feature/task.
    * Ideas - any additional ideas for new features or improvements in the project.
    * Name - the name of the card would be a short description of the idea that occurred to you.

backlog / in progress / for review / done /
develop / test / deploy
plan / build / test / review / deploy

backlog
userstories
burndown chart

Info videos: 
https://www.youtube.com/watch?v=J2u7OqBA_aQ
https://www.youtube.com/watch?v=9W4oxjdAwUs

Short summary:
* Issue is something that needs to be done, a deliverable, piece of code, component function, etc. 
* Labels are a way to categorize your issues
  * scoped: can have only one per issue
  * unscoped: can have multiple
* Epic is a bucket(group) of issues, it grows and shrinks related to the milestones we assign to our issues.Epics can be part of other epics(as child parent relationship)	
* Milestones – is a strategic point of time, where certain amount of tasks(issues) needs to be delivered

### Step 3 – Coding!
Try to adhere to this project specification and make your project as close to it as possible. Also, do not go crazy on features, implement a few but implement them amazingly!
Always remember, quality over quantity!

### Step 4 – Validation. 
Review the code and test the behavior of the features of your team members. All team members should be aware how a feature works, even if they were not the ones to implement it. - исками се да създадем тестове, това би помогнало за тази част от проекта, също така ще ускори процеса developmt


### Step 5 – Teamwork create towards a common goal:
    • prepare a common plan that you agree to follow.
    • take responsibility for your tasks.
    • communicate and ask for details with regards to the project implementation.
    • be able to explain how you have contributed to the project.
    • be able to explain the source code of your team members.


### Step 6 – Give Feedback about Your Teammates. - мисля че поне веднъж седмично е хубаво да го правим проактивно, иначе вярвам, че ако момента е подходящ винаги трябва да го правим. 
You will be asked to provide feedback about your teammates, their attitude to this project, their technical skills, their team working skills, their contribution to the project, etc. The feedback is important part of the project evaluation so take it seriously and be honest.

## Working Effectively in Teams
Project Scope and Ground Rules

### Establish project objectives, team norms and guidelines for meetings
To work effectively as a team discussing project objectives and assigning team roles, guidelines and division of labor is critical. Think through who will: - помислете, в коя част сте най-силни и как може да помогнете за най-доброто възможно представяне на проекта. 
  * Schedule meetings and make sure you follow a timeline/agenda
    * in person or virtual - will allow you keep yourselves on track and ensure you have an opportunity to share ideas, notes and research
  * establish communication ground rules
    * meetings / email / phone / chat
    * how long till you reply
  * set mutually agreed upon ground rules for contribution.
  * Take notes at meetings to send to everyone afterwards 
  * Research topics for the project   
  * Compile and design presentation for partners
  * Present your project work

### Create a timeline and split work into smaller tasks
A timeline is important to make sure the project isn’t left until the last minute. This will also help estimate work effort and identify dependencies between activities (one cannot start if the other hasn’t finished, etc.) Set aside buffer time in case there are delays in delivery. 
Discuss and assign tasks according to each person’s preferences to make it less overwhelming. This also makes it easier to complete the project work because team members can work independently on their sections. Write off the tasks and time estimates in the project management system in GitLab.

## Peer-to-Peer Feedback

### Keep a Positive Mindset: The first thing to remember when giving or receiving feedback is that it’s intended to help. It shouldn’t be viewed as a personal attack. Feedback should be written/delivered with positive intentions: To help your peers grow as they progress along their career journey.
### Be Specific and Actionable: When possible, try to provide actionable steps for improvement. For example, rather than saying, “You need to be better about finishing projects on schedule”, you could say, “It would be helpful if you provided progress updates so that we can offer additional support as deadlines approach.”
### Focus on Problems, Not People: Again, constructive criticism shouldn’t feel like a personal attack. To avoid defensiveness, you should focus on your peers’ work, not their personality. Writing comments in the passive voice can actually help with this. For example, rather than saying, “You didn’t provide enough data in the presentation,” you could say, “The presentation would be more convincing if it included more data.”


## Challenges in a team work setting - кое сте изпитали в последните си два проекта, и нека направим кратка дискусия по въпроса

## Scheduling problems - този го изпитахме
This can create roadblocks to getting started/continuing with your project. It can be frustrating for you as a learner to feel that others aren’t compromising and don’t take their situation into consideration.
    • Try and be understanding of your team member’s schedules and responsibilities which may be different from your own. 
    • Consider using online channels to communicate.
    • Take turns picking when and where you will be meeting next time.

## Group conflict
Group conflict is natural and often necessary for effective projects. Sometimes though, it escalates and makes it even more difficult to focus.
    • Don’t let personal feelings impact your work in the team. Focus on the task. 
    • Address issues openly directly and respectfully.
    • Try and find common ground between two ideas to reach compromise.

## Uneven contribution - това мисля, че също го срещнахме
Some team members don’t contribute to the team project or aren’t perceived to be contributing to the team. This creates tension in the team and is unfair to the team members
    • Discuss and set clear guidelines and work expectations at the beginning of the team project. 
    • Assign roles and responsibilities so that each person will be making an equal contribution. 
    • Speak directly, but respectfully to the person who is not completing their work.


## Different expectations - може би с това може да имаме проблем
Some members strive for perfection, while others simply want to pass. Some begin projects in advance, while others procrastinate. This can create tension because the team is not working towards the same goal.
    • Early communication is key to make sure everyone is focused on common goals.
    • Keep goals realistic and understand that your actions affect others in the team. 
    • Make a timeline so that your team can stay on an agreed plan for getting the project done.

## Getting stuck
At some point teams may get ‘stuck’ and hit a mental roadblock. This is discouraging and can lead to procrastination and avoidance.
    • Reread project requirements and goals.
    • Have a brainstorming session where ideas are discussed. 
    • Seek help from trainers and your peers and/or mentors, if you remain stuck.

## Groupthink
‘Groupthink’ occurs when members of a group agree with other group members to avoid conflict. This kills creativity and constructive evaluation of alternative ideas.
    • Thinking critically about ideas presented, offering and assessing alternatives, and embracing diverse opinions from team members. 
    • Work through projects analytically using the teams’ combined knowledge and experience.


























