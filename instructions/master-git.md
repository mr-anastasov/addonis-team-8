Git commit guide- in short:

The perfect commit:
* add the right changes
* write a meaningfully commit message

Write a meaningfully title: (use the tags specified beneath to create context)
* feat: a new feature
* fix: a bug fix
* docs: changes to documentation
* style: formatting, missing semi colons, etc; no code change
* refactor: refactoring production code
* test: adding tests, refactoring test; no production code change
* chore: updating build tasks, package manager configs, etc; no production code change

Write a body that is descriptive: 
* short, concise and imperative. 
* If you can't do that probably you are adding to many changes to the commit


Good git practices: 
* Always pull from remote/other related branches before you start any new logical work on your code. It will keep your branch up-to-date as much as possible and reduce the chances of conflicts.
* Always pull before a push to make sure you will not face any rejections from Git.
Talk to your peers/co-developers when you are unable to make a call on what to keep vs. what to remove. Pair up to resolve any difficult merge conflicts.

Additional reads: 
https://cbea.ms/git-commit/
https://dev.to/pavlosisaris/git-commits-an-effective-style-guide-2kkn



Most useful commands:
* git diff 'the_file_name'- to see what changes have been made to the file
* git add -p - to add just some of the changes but not all
* git add 'the_file_name' - to add the entire file
* git commit -m 'here you write a short text, max 80 chars'
* git commit - opens a text editor where you can add longer text
* git branch - to see on what branch you are on
* git checkout 'name of the branch' - to switch between branches

workflow= git add -> git commit -> git pull -> git push


Merge conflics:
* get merge --abort - to undo the merge
* git rebase --abort - 

https://www.freecodecamp.org/news/resolve-merge-conflicts-in-git-a-practical-guide/




* git checkout -b 'name of the branch' - to create a new branch (we are not going to need it for now)
* git push --set -upstream origin 'the same name as the branch you on to push changes to it / or the parent branch if you want to push changes to that branch'  (Used when you are on a newly created branch, first time you push you need to specify where to push it to)



Git branching strategies:
- mainline development - always integrating (few branches, relative small commits, high quality testing & QA standards)
- State, Release and Feature Branches: different type of branch for different purposes

Type of branches:
* long running 
    * (main / master)
    * (develop/production/staging - used for different states of the project, usually specified in your release and deployment strategy, commits are never directly committed to that branches, through integration or merge, you don't want to add untested code to your main branch, commit on specific times, batches, so it's better kept track of what is happening).
* short lived Branches (based on long branch, for new features, bug fixes, refactoring, experiments, will be deleted after integration) 

**GitHub Flow** - simple, lean, only one long-running branch + feature branches that merge back to the main
**GitFlow** - two long branches
* main(production - deployment)
* develop(all the feature, bug-fixes and so are happing on it, and merge back to it, when is thoroughly tested and ready for deployment it's gets merged with the main)

https://www.youtube.com/watch?v=Uszj_k0DGsg


In depth begginer friendly guide (https://www.freecodecamp.org/news/create-and-sync-git-and-github-repositories/)


Other:
git reset --hard 'number of the commit' - if you have added and pushed to main accidentally and your main branch stays ahead you can use this command to return to the last state you main branch was up to date with the origin/main

https://typicode.github.io/husky/ - linter for git