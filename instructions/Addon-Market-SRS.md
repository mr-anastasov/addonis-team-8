# Project Description 

Addonis is an Addons Registry web application. 
It enables its users to:  
  * Publish their own addons  
  * Browse addons for their preferred IDE
  * Download addons
  * Rate existing addons  

You may look for inspiration at the JetBrains Registry, the Visual Studio Marketplace, or the Eclipse Marketplace.The goal is to have a place that makes it easy for a user to find the right tool for their need.Build a clean, simple application that gets the job done!  

# Functional Requirements 

## Entities 
Authentication is handled by Firebase, there is no need for auth entity 

## Users must have a username, email, phone number and a photo. 
  * [x] Username must be unique and between 2 and 20 symbols. 
  * [x] Email must be valid email and unique in the system. 
  * [x] Phone number must be 10 digits and unique in the system. 

## Addon must have name, target IDE, creator, description, tags, binary content, origin(link)
  * [x] name: unique and between 3 and 30 symbols. 
  * [x] target IDE exactly one target IDE. 
  * [x] creator
  * [x] description
  * [x] tags at least one tag (but can have more than one) 
  * [x] binary content (the addon itself)
  * [x] origin (this is the location of the GitHub repo hosting the extension)
  * [] other:  think of what additional entities you might relate to the Addon entity

## Public part accessible without authentication i.e., for anonymous users.
  * [x] must be able to register and login, required fields for registration are username, email, and phone number.
      * [] could: Email Verification – In order for the registration to be completed, the user should verify their email by clicking on a link send to their email by the application. Before verifying their email, users cannot make transactions.(CRUD) 
      * [] could: Identity Verification – In order for the user registration to be completed, the user should submit a photo of their id card and a selfie. Before they are approved by administrator user are treated as anonymous who have pending registration -> users cannot create addons. /to check how to create a relation between file and the user who uploaded it/
        * Note: DO NOT upload actual photos of id cards! 
  * [x] must see a landing page that has a list with three categories: 
      * [x] Popular (the addons with the most downloads)
      * [x] New (the newest addons)  
      * [x] Featured (addons selected by the admins)
  * [x] must be able to still see details for each addon:
      * [x] The addon is visible in the Public Part only if it is approved. /state: 'approved', all other states are forbidden(pending, draft, deleted)/ 
      * [x] Name  
      * [x] Description  
      * [x] Creator  
      * [x] Tags  
      * [x] Number of downloads  
      * [] Rating  
      * [x] Download link  
      * [x] Origin link – this is the location of the GitHub repo hosting the extension  
      * [x] Open issues count (retrieved from origin repository)  
      * [x] Pull requests count (retrieved from origin repository)  
      * [x] Last commit (date + title) (retrieved from origin repository)
  * [x] must be able to filter addons 
      * [x] by name
      * [x] by IDE
        * [x]could: by Creator
  * [x] must be able to sort by the following fields:
      * [x] Name  
      * [x] Creator 
      * [x] Tags 
      * [x] Number of downloads  
      * [x] Upload date  
      * [х] Last commit date
  * [x] must be able to download a plugin, directly from the landing page



 
## Private part: accessible only if the user is authenticated(registered and logged in)
implement stepper for sign-up and create addon
instead of button group for the user addons can make tabs page, same thing when sorting
put container in most views
popover can be implemented for notifications
<TextareaAutosize /> for 


  * [x] must be able to login/logout
  * [x] must be able to view and update their profile info except their username, which is selected on registration and cannot be changed afterwards.
  * [x] must be able to manage their addons (Create Read Update Delete). - if blocked this functionality is suspended
    * [x]create
      * [x] pending state
      * [x] draft mode 
      * [x] administrator approval of pending addons. 
    * [x] read
    * [x]update
    * [x]delete - soft delete
      * []could: upon account closing to erase data
  * [] must be able to rate addons.  
      * [] could: Addon Creation Verification – In order to create an addon, the user is prompted to enter a verification code, sent to their email. The code should be unique for the addon and could have an expiration date. 
      * [] could: Joint Addons – User can create joint addons. They function as regular addons; however, multiple users can manage them. The owner of the addon has an administration panel for the addon, where they can grant or revoke other user’s access to the addon. Joint addon should show its owner and all its maintainers. Ownership could be transferred to a maintainer.
  
  * [x] Could: Drafts – A user can create a draft of an addon. A draft addon is visible only for the creator. All data in a draft is optional but only a valid and complete addon can be switched to pending phase. Admins must not see drafts in the approve list but could have a separate list for all drafts in the system. Admins must only be able to delete drafts not to update them. 
  * [] Could: Invite a Friend – A user can enter email of people,  not yet registered for the application, and invite them to register. The application sends to that email a registration link. 
  * [] Could: Any user should be able to follow other users for easy access to the list of their addons. User could be notified when a user from the following list creates new addon (after the verification). The user has a following list administration page, where they can remove users from the list. 
  * [] Could: Recurring Metrics Reports – Users can set up recurring metrics reports. User has the option to select an interval of time on which the report is generated automatically. Reports should include all user’s addons (names) and their current number of downloads and rating. Users have a page, where they can view all their reports and cancel them. Users should be able to choose which addons to be included in the report. Users could be notified if a metric passes certain threshold (i.e., over 1000 downloads or rating falling below 3.5). 


## Administrative part accessible to users with administrative privileges. 
  * [x] must be able to approve new addons. 
  * [x] must be able to see list of all users.  
      * [] User list should support pagination.
      * [] should: Users with administrator rights should have a page where they can view all users waiting for verification, review the photos they submitted and approve or reject them. 
  * [x] search users by
    * [x] phone number
    * [x] name
    * [x] username
    * [x] email
  * [x] be able to block or unblock them. A blocked user must be able to do everything as normal user, except to create new and update current addons. 
  * [] Admin users must be able to edit/delete addons. 
  * [x] Addons list should support pagination. 


## Firebase Realtime Database: all data should be stored in the document (NoSQL) database hosted by Google Firebase. You must think of a way to organize your documents to achieve the functionalities described above. 


# Good practice to fallow: 

## General 
Follow KISS, SOLID, DRY principles when coding 
Follow the principles of functional programming wherever applicable 
Use tiered project structure (separate the application in layers, if applicable)
Write clean code: https://www.freecodecamp.org/news/how-to-write-clean-code/#naming 
You should implement proper exception handling and propagation 
Try to think ahead. When developing something, think – “How hard would it be to change/modify this later?”

## Git 
Commits in the GitLab repository should give a good overview of how the project was developed, which features were created first and the people who contributed. Contributions from all team members must be evident through the git commit history! 
* The repository must contain the complete application source code and any scripts (database scripts, for example). 
* Use a branching while working with Git. 
* Provide a link to a GitLab repository with the following information in the README.md file: 
* Project description 
* Link to the hosted project (if hosted online) 
* Instructions how to setup and run the project locally
* architecture of the project: https://www.freecodecamp.org/news/information-architecture-userflow-sitemap/


## Host you application with Firebase Hosting or any other hosting service 


## Legend 
* Must – Implement these first.
* Should – if you have time left, try to implement these.
* Could – only if you are ready with everything else give these a go. 

 
JavaProjects of previous groups: 
* https://gitlab.com/dilqna9/addonis
* https://gitlab.com/lambova/addonis_of_dari_and_gali 
* https://gitlab.com/SvetlioNikolov/addonis 
* https://gitlab.com/yoto741/addonis/-/tree/master	 



AUTHORS: credits to those participating in writing the code.
BUGS: known issues and instructions on reporting newly found bugs.
CONTRIBUTING/HACKING: guide for prospective contributors, especially useful for open-source projects.
FAQ: you already know what that is. ;)
INSTALL: instructions on how to compile or install the software on different systems.
NEWS: similar to the CHANGELOG file, but intended for end users, not developers.
THANKS: acknowledgments.
TODO/ROADMAP: a listing for planned upcoming features.
VERSION/RELEASE: a one-liner describing the current version number or
TECHNOLOGIES: 
DATABASE SCHEME: 
APP SCREEN SHOTS: 
