# Ways ot testing with vitest

to execute tests run npm run tests

to write tests
  * separate file

the name of the file is the same as the of the component/view you are testing + .test.js extension
```js
import { describe, expect, it } from "vitest"

describe("sum", () => {
  it("returns 0 with no numbers", () => {
    expect(sum().toBe(0));
  })
})

```
  * in the same file where the component is

```js

if (import.meta.vitest) {

  const import { describe, expect, it } = import.meta.vitest;


}
```